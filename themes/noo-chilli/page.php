<?php get_header(); ?>
<?php echo noo_custom_page_heading(); ?>

<div class="container default-page">
    <!-- Begin The loop -->
    <?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post(); ?>
            <?php the_content(); ?>
        <?php endwhile; ?>
    <?php endif; ?>
    <!-- End The loop -->
</div>

<?php get_footer(); ?>