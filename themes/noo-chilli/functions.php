<?php
/**
 * Theme functions for NOO Framework.
 * This file include the framework functions, it should remain intact between themes.
 * For theme specified functions, see file functions-<theme name>.php
 *
 * @package    NOO Framework
 * @version    1.0.0
 * @author     Kan Nguyen <khanhnq@nootheme.com>
 * @copyright  Copyright (c) 2014, NooTheme
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       http://nootheme.com
 */

// Set global constance
if( !defined('NOO_FRAMEWORK') ) define( 'NOO_FRAMEWORK', get_template_directory() . '/framework' );
if( !defined('NOO_FRAMEWORK_ADMIN') ) define( 'NOO_FRAMEWORK_ADMIN', NOO_FRAMEWORK . '/admin' );
if( !defined('NOO_FRAMEWORK_FUNCTION') ) define( 'NOO_FRAMEWORK_FUNCTION', NOO_FRAMEWORK . '/functions' );

if( !defined('NOO_FRAMEWORK_URI') ) define( 'NOO_FRAMEWORK_URI', get_template_directory_uri() . '/framework' );
if( !defined('NOO_FRAMEWORK_ADMIN_URI') ) define( 'NOO_FRAMEWORK_ADMIN_URI', NOO_FRAMEWORK_URI . '/admin' );

if ( !defined( 'NOO_ASSETS' ) ) {
	define( 'NOO_ASSETS', get_template_directory() . '/assets' );
}

if ( !defined( 'NOO_ASSETS_URI' ) ) {
	define( 'NOO_ASSETS_URI', get_template_directory_uri() . '/assets' );
}

if ( !defined( 'NOO_ADMIN_ASSETS_URI' ) ) {
    define( 'NOO_ADMIN_ASSETS_URI', NOO_FRAMEWORK_ADMIN_URI . '/assets' );
}

if( !defined('NOO_WOOCOMMERCE_EXIST') ) define( 'NOO_WOOCOMMERCE_EXIST', class_exists( 'WC_API' ) );
if( !defined('NOO_SUPPORT_PORTFOLIO') ) define( 'NOO_SUPPORT_PORTFOLIO', false );

// Functions for specific theme
$theme_name = 'custom';
if ( file_exists( get_template_directory() . '/functions_' . $theme_name . '.php' ) ) {
	require_once get_template_directory() . '/functions_' . $theme_name . '.php';
}

//
// Helper functions.
//
require_once NOO_FRAMEWORK_FUNCTION . '/noo-theme.php';
require_once NOO_FRAMEWORK_FUNCTION . '/noo-utilities.php';
require_once NOO_FRAMEWORK_FUNCTION . '/noo-html.php';
require_once NOO_FRAMEWORK_FUNCTION . '/noo-style.php';
require_once NOO_FRAMEWORK_FUNCTION . '/noo-wp-style.php';
require_once NOO_FRAMEWORK_FUNCTION . '/noo-mailchimp.php';
require_once NOO_FRAMEWORK_FUNCTION . '/noo-woocommerce.php';
require_once NOO_FRAMEWORK_FUNCTION . '/noo-css.php';

//
// Enqueue assets
//
require_once NOO_FRAMEWORK_FUNCTION . '/noo-enqueue-css.php';
require_once NOO_FRAMEWORK_FUNCTION . '/noo-enqueue-js.php';

//
// Admin panel
//

// Initialize theme
require_once NOO_FRAMEWORK_ADMIN . '/_init.php';

// Initialize NOO Customizer
require_once NOO_FRAMEWORK_ADMIN . '/customizer/_init.php';


// Meta Boxes
require_once NOO_FRAMEWORK_ADMIN . '/meta-boxes/_init.php';

// Taxonomy Meta Fields
require_once NOO_FRAMEWORK_ADMIN . '/taxonomy-meta.php';

// Add Menu more
require_once NOO_FRAMEWORK_ADMIN . '/add_menu_more.php';

// Mega Menu
require_once NOO_FRAMEWORK . '/mega-menu/noo_mega_menu.php';

//
// Widgets
//
$widget_path = get_template_directory() . '/widgets';

if ( file_exists( $widget_path . '/widgets_init.php' ) ) {
	require_once $widget_path . '/widgets_init.php';
	require_once $widget_path . '/widgets.php';
}

//
// Plugins
// First we'll check if there's any plugins inluded
//
$plugin_path = get_template_directory() . '/plugins';
if ( file_exists( $plugin_path . '/tgmpa_register.php' ) ) {
	require_once NOO_FRAMEWORK_ADMIN . '/class-tgm-plugin-activation.php';
	require_once $plugin_path . '/tgmpa_register.php';
}

function noo_custom_archive_title($title){
    if ( is_category() ) {
        $title = single_cat_title( '', false );
    }
    return $title;
}
add_filter('get_the_archive_title','noo_custom_archive_title');

function noo_comment_form( $args = array(), $post_id = null ) {
    global $id;
    $user = wp_get_current_user();
    $user_identity = $user->exists() ? $user->display_name : '';

    if ( null === $post_id ) {
        $post_id = $id;
    }
    else {
        $id = $post_id;
    }

    if ( comments_open( $post_id ) ) :
        ?>
    <div id="respond-wrap">
        <?php
        $commenter = wp_get_current_commenter();
        $req = get_option( 'require_name_email' );
        $aria_req = ( $req ? " aria-required='true'" : '' );
        $fields =  array(
            'author' => '<div class="row"><p class="comment-form-author  col-sm-6"><input id="author" name="author" type="text" placeholder="' . esc_html__( 'Full Name*', 'noo-chilli' ) . '" class="form-control" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' /></p>',
            'email' => '<p class="comment-form-email col-sm-6"><input id="email" name="email" type="text" placeholder="' . esc_html__( 'Email*', 'noo-chilli' ) . '" class="form-control" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' /></p>',
            'comment_field'        => '<div class="col-sm-12"><p class="comment-form-comment"><textarea class="form-control" placeholder="' . esc_html__( 'Enter Your Comment', 'noo-chilli' ) . '" id="comment" name="comment" cols="40" rows="6" aria-required="true"></textarea></p></div></div>'
        );
        $comments_args = array(
            'fields'               => apply_filters( 'comment_form_default_fields', $fields ),
            'logged_in_as'         => '<p class="logged-in-as">' . sprintf( wp_kses( __( 'Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>', 'noo-chilli' ), noo_allowed_html() ), admin_url( 'profile.php' ), $user_identity, wp_logout_url( apply_filters( 'the_permalink', get_permalink( ) ) ) ) . '</p>',
            'title_reply'          => sprintf('<span>%s</span>', esc_html__( 'Leave a reply', 'noo-chilli' )),
            'title_reply_to'       => sprintf('<span>%s</span>', esc_html__( 'Leave a reply to %s', 'noo-chilli' )),
            'cancel_reply_link'    => esc_html__( 'Click here to cancel the reply', 'noo-chilli' ),
            'comment_notes_before' => '',
            'comment_notes_after'  => '',
            'label_submit'         => esc_html__( 'Send Now', 'noo-chilli' ),
            'comment_field'        =>'',
            'must_log_in'          => ''
        );
        if(is_user_logged_in()){
            $comments_args['comment_field'] = '<p class="comment-form-comment"><textarea class="form-control" placeholder="' . esc_html__( 'Enter Your Comment', 'noo-chilli' ) . '" id="comment" name="comment" cols="40" rows="6" aria-required="true"></textarea></p>';
        }
        comment_form($comments_args);
        ?>
    </div>

    <?php
    endif;
}


function noo_contact_methods($profile_fields) {

    // Add new fields
    $profile_fields['twitter'] = 'Twitter URL';
    $profile_fields['facebook'] = 'Facebook URL';
    $profile_fields['gplus'] = 'Google+ URL';
    $profile_fields['pinterest'] = 'Pinterest URL';

    return $profile_fields;
}
add_filter('user_contactmethods', 'noo_contact_methods');

function noo_add_span_cat_count($links) {
    $links = str_replace('</a> (', '</a> <span>', $links);
    $links = str_replace(')', '</span>', $links);
    return $links;
}
add_filter('wp_list_categories', 'noo_add_span_cat_count');

if ( ! function_exists( 'noo_custom_paging_nav' ) ) :
    function noo_custom_paging_nav($query_total) {
        global $wp_query, $wp_rewrite;
        // Don't print empty markup if there's only one page.
        if ( $query_total < 2 ) {
            return;
        }

        $paged        = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;
        $pagenum_link = html_entity_decode( get_pagenum_link() );
        $query_args   = array();
        $url_parts    = explode( '?', $pagenum_link );

        if ( isset( $url_parts[1] ) ) {
            wp_parse_str( $url_parts[1], $query_args );
        }

        $pagenum_link = esc_url( remove_query_arg( array_keys( $query_args ), $pagenum_link ) );
        $pagenum_link = trailingslashit( $pagenum_link ) . '%_%';

        $format  = $wp_rewrite->using_index_permalinks() && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
        $format .= $wp_rewrite->using_permalinks() ? user_trailingslashit( $wp_rewrite->pagination_base . '/%#%', 'paged' ) : '?paged=%#%';
        // Set up paginated links.
        $links = paginate_links( array(
            'base'     => $pagenum_link,
            'format'   => $format,
            'total'    => $query_total,
            'current'  => $paged,
            'mid_size' => 1,
            'add_args' => array_map( 'urlencode', $query_args ),
            'prev_text' => esc_html__( 'Previous', 'noo-chilli' ),
            'next_text' => esc_html__( 'Next', 'noo-chilli' ),
        ) );

        if ( $links ) :

            ?>
            <nav class="navigation paging-navigation noo-hide" role="navigation">
                <div class="pagination loop-pagination">
                    <?php echo $links; ?>
                </div><!-- .pagination -->
            </nav><!-- .navigation -->
        <?php
        endif;
    }
endif;

if ( ! function_exists( 'noo_search_form' ) ) {
    function noo_search_form( $form ) {
        $form = '<form method="GET" class="form-horizontal noo-form-horizontal" action="' . home_url( '/' ) . '">
        <label class="sr-only"></label>
        <input type="search" name="s" class="formsearch-input noo-formsearch-input" value="' . get_search_query() . '" placeholder="' . esc_html__( 'Search something...', 'noo-chilli' ) . '">
        <button type="submit" class="fa fa-search-plus noo-form-button"></button>
        </form>';

        return $form;
    }
    add_filter( 'get_search_form', 'noo_search_form' );
}

// Mobile icon
function noo_minicart_mobile() {

    global $woocommerce;
    
    $cart_output = "";
    $cart_total = $woocommerce->cart->get_cart_total();
    $cart_count = $woocommerce->cart->cart_contents_count;
    $cart_output = '<a class="cart-button" href="' . $woocommerce->cart->get_cart_url() . '" title="' . esc_html__( 'View Cart', 'noo-chilli' ) .
         '"><span><i class="fa fa-shopping-cart"></i><em>' . $cart_count . '</em></span></a>';
    return $cart_output;
}
function noo_navbar_shop_icons_mini_simple( $items, $args ) {
    if( ! NOO_WOOCOMMERCE_EXIST ) return $items;
    if( ! noo_get_option('noo_header_minicart', true ) ) {
        return $items;
    }
    if ( ( $args->theme_location == 'primary' || $args->theme_location == 'onepage' ) ) {
        $minicart = noo_minicart_mobile();
        $items .= '<li id="nav-menu-item-simple-cart" class="menu-item noo-menu-item-simple-cart">'.$minicart.'</li>';
    }
   return $items;
}

add_filter( 'wp_nav_menu_items', 'noo_navbar_shop_icons_mini_simple', 10, 2 );
// Toggle admin bar
// show_admin_bar( false );