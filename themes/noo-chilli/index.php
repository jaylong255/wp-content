<?php


get_header(); ?>
<?php

$noo_blog_layout = noo_get_option('noo_blog_layout');
$class_slider = 'col-md-8 col-sm-12 pull-left reser-padding-right';
if( $noo_blog_layout == 'left_sidebar' ){
    $class_slider = 'col-md-8 col-sm-12 reser-padding-right pull-right';
}elseif($noo_blog_layout == 'fullwidth'){
    $class_slider = 'col-md-12 pull-right';
}
?>
<?php echo noo_custom_page_heading(); ?>
<div id="primary-archive" class="container">
    <div class="row">
        <div class="<?php echo esc_attr($class_slider); ?>">
            <?php if ( have_posts() ) : ?>


                <?php
                // Start the Loop.
                while ( have_posts() ) : the_post();
                    /*
                     * Include the Post-Format-specific template for the content.
                     * If you want to override this in a child theme, then include a file
                     * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                     */
                    get_template_part( 'content');

                    // End the loop.
                endwhile;

                noo_pagination_normal();
            endif;
            ?>
        </div>

        <?php if( $noo_blog_layout !='fullwidth' ): get_sidebar(); endif; ?>

    </div>
</div><!-- .content-area -->

<?php get_footer(); ?>
