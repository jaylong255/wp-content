<form method="GET" class="form-horizontal noo-form-horizontal" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="form">
    <label class="sr-only"><?php esc_html__( 'Search', 'noo-chilli' ); ?></label>
    <input type="search" name="s" class="formsearch-input noo-formsearch-input" value="<?php echo esc_attr(get_search_query()); ?>" placeholder="<?php echo esc_attr__( 'Search...', 'noo-chilli' ); ?>" />
    <button type="submit" class="fa fa-search-plus noo-form-button"></button>
</form>