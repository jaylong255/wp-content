<!doctype html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
<?php 
	if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) :
		$favicon = noo_get_image_option('noo_custom_favicon', '');
		if ($favicon != ''): ?>
			<!-- Favicon-->
			<link rel="shortcut icon" href="<?php echo esc_url($favicon); ?>" />
		<?php
		endif;
	endif;
?>
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<div class="site">
        <?php noo_get_layout('navbar'); ?>

