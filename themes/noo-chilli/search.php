<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
<?php $bk_img = NOO_ASSETS_URI.'/images/bk_default.jpg' ;
$headerimg = noo_get_option('noo_blog_heading_image');
if ( isset($headerimg) && $headerimg !='' ){
    $bk_img = $headerimg;
}
?>

<?php echo noo_custom_page_heading(); ?>
<section id="primary-archive" class="container">
    <div class="row">
        <div class="col-md-12">
            <?php if ( have_posts() ) : ?>


                <?php
                // Start the Loop.
                while ( have_posts() ) : the_post();
                    get_template_part( 'content');
                    // End the loop.
                endwhile;

                // Previous/next page navigation.
                noo_pagination_normal();

            // If no content, include the "No posts found" template.
            else :
                echo "<p>" . esc_html__('No results found', 'noo-chilli' ) . "</p>";
            endif;
            ?>
        </div>
    </div>
</section><!-- .content-area -->

<?php get_footer(); ?>
