<?php

get_header(); ?>

<?php


$layour = noo_get_page_layout();
$noo_single_author_bio   = noo_get_option('noo_blog_post_author_bio');
$class_slider = 'col-md-8 reser-padding-right pull-left';
if( $layour == 'left_sidebar' ){
    $class_slider = 'col-md-8 reser-padding-right pull-right';
}elseif($layour == 'fullwidth'){
    $class_slider = 'col-md-12 pull-right';
}
?>
    <?php echo noo_custom_page_heading(); ?>
    <div id="primary-archive" class="container">
        <div class="row">
            <div class="<?php echo esc_attr($class_slider); ?>">
                <?php if ( have_posts() ) : ?>


                    <?php
                    // Start the Loop.
                    while ( have_posts() ) : the_post();


                        /*
                         * Include the Post-Format-specific template for the content.
                         * If you want to override this in a child theme, then include a file
                         * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                         */
                        get_template_part( 'content');

                        ?>
                        
                        <?php

                        // End the loop.
                    endwhile;

                ?>
                    <?php if( $noo_single_author_bio == 1 ): ?>
                    <div id="author-bio">
                        <div class="author-avatar">
                            <?php echo get_avatar( get_the_author_meta( 'user_email' ),150); ?>
                        </div>
                        <div class="author-info">
                            <h5>
                                <a title="<?php printf( esc_html__( 'Post by %s', 'noo-chilli' ), get_the_author() ); ?>" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author">
                                    <?php echo get_the_author() ?>
                                </a>
                            </h5>
                            <p>
                                <?php the_author_meta( 'description' ) ?>
                            </p>
                            <?php
                            $facebook     = get_the_author_meta('facebook');
                            $twitter      = get_the_author_meta('twitter');
                            $google		  = get_the_author_meta('gplus');
                            $pinterest    = get_the_author_meta('pinterest');
                            if($facebook || $twitter || $google || $pinterest) {
                                ?>
                                <div class="noo-social">
                                    <?php if($facebook) {?>
                                        <a href="<?php echo esc_url( $facebook ); ?>"><i class="fa fa-facebook">&nbsp;</i></a>
                                    <?php } ?>
                                    <?php if($twitter) {?>
                                        <a href="<?php echo esc_url( $twitter ); ?>"><i class="fa fa-twitter">&nbsp;</i></a>
                                    <?php } ?>
                                    <?php if($google) {?>
                                        <a href="<?php echo esc_url( $google ); ?>"><i class="fa fa-google">&nbsp;</i></a>
                                    <?php } ?>
                                    <?php if($pinterest) {?>
                                        <a href="<?php echo esc_url( $pinterest ); ?>"><i class="fa fa-pinterest">&nbsp;</i></a>
                                    <?php } ?>

                                </div>
                            <?php } ?>
                        </div>

                    </div>
                    <?php endif; ?>

                    <div class="single-comment">
                        <?php if ( comments_open() ) : ?>
                            <?php comments_template( '', true ); ?>
                        <?php endif; ?>
                    </div>
                <?php
                endif;
                ?>
            </div>

            <?php if( $layour !='fullwidth' ): get_sidebar(); endif; ?>

        </div>
    </div><!-- .content-area -->


<?php
get_footer();
?>