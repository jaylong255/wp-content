<?php


remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);

remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);

remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

add_action('woocommerce_before_main_content', 'noo_theme_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'noo_theme_wrapper_end', 10);

// change position rating

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 25);

function noo_theme_wrapper_start() {
    echo '<div id="main">';
}

function noo_theme_wrapper_end() {
    echo '</div>';
}
remove_action('woocommerce_sidebar','woocommerce_get_sidebar',10);
add_action('woocommerce_sidebar','noo_woo_sidebar',10);
function noo_woo_sidebar(){


    $sidebar = noo_get_option('noo_shop_sidebar', 'shop-main');
    $single_pro = noo_get_option('noo_woocommerce_product_sidebar', 'shop-main');
    if( is_product() ){
        $sidebar = $single_pro;
    }
    ?>
        <div class="noo-shop-sidebar">
            <?php if( function_exists('dynamic_sidebar') && dynamic_sidebar($sidebar) ): endif; ?>
        </div>
    <?php
}
if(!function_exists(('noo_woo_layout'))):
    function noo_woo_layout(){
        $noo_shop_layout = noo_get_option('noo_shop_layout');
        $sidebarLayout = noo_get_option('noo_woocommerce_product_layout', '');
        $class_slider = 'col-md-9 col-sm-12 col-xs-12 pull-right';
        if( $noo_shop_layout == 'sidebar' ){
            $class_slider = 'col-md-9 col-sm-12 col-xs-12 pull-left';
        }elseif($noo_shop_layout == 'fullwidth'){
            $class_slider = 'col-md-12 pull-right';
        }
        if( isset($sidebarLayout) && $sidebarLayout != '' ){
            if( $sidebarLayout == 'sidebar' ){
                $class_slider = 'col-md-9 col-sm-12 col-xs-12 pull-left';
            }elseif($sidebarLayout == 'fullwidth'){
                $class_slider = 'col-md-12 pull-right';
            }
        }
        return $class_slider;
    }
endif;




// Number of products per page
function noo_woocommerce_loop_shop_per_page() {
    return noo_get_option( 'noo_shop_num', 12 );
}
add_filter( 'loop_shop_per_page', 'noo_woocommerce_loop_shop_per_page' );

// Related products
add_filter( 'woocommerce_output_related_products_args', 'noo_woocommerce_output_related_products_args' );

function noo_woocommerce_output_related_products_args() {

    $args = array( 'posts_per_page' => noo_get_option('noo_woocommerce_product_related',6), 'columns' => 4 );
    return $args;
}

// Wishlist
if ( ! function_exists( 'noo_woocommerce_wishlist_is_active' ) ) {

    /**
     * Check yith-woocommerce-wishlist plugin is active
     *
     * @return boolean .TRUE is active
     */
    function noo_woocommerce_wishlist_is_active() {
        $active_plugins = (array) get_option( 'active_plugins', array() );

        if ( is_multisite() )
            $active_plugins = array_merge( $active_plugins, get_site_option( 'active_sitewide_plugins', array() ) );

        return in_array( 'yith-woocommerce-wishlist/init.php', $active_plugins ) ||
        array_key_exists( 'yith-woocommerce-wishlist/init.php', $active_plugins );
    }
}
function noo_template_loop_wishlist() {
    if ( noo_woocommerce_wishlist_is_active() ) {
        echo do_shortcode( '[yith_wcwl_add_to_wishlist]' );
    }
}

add_action('woocommerce_share','noo_share_product',1);
function noo_share_product(){
    if( is_product() ):
        ?>
        <div class="product_share">
            <span><?php echo esc_html_e('Share:', 'noo-chilli' ); ?></span>
            <!-- Facebook Button -->
            <a href="javascript: void(0)" onclick="window.open('http://www.facebook.com/sharer.php?s=100&amp;p[title]=<?php the_title(); ?>&amp;p[url]=<?php the_permalink() ; ?>','sharer','toolbar=0,status=0,width=580,height=325');" id="fb-share" class="noo_social"><i class="fa fa-facebook"></i></a>

            <!-- Twitter Button -->
            <a href="javascript: void(0)" onclick="window.open('http://twitter.com/share?text=<?php the_title(); ?>&amp;url=<?php the_permalink() ; ?>','sharer','toolbar=0,status=0,width=580,height=325');" class="noo_social" id="tw-share"><i class="fa fa-twitter"></i></a>

            <!-- Google +1 Button -->
            <a href="javascript: void(0)" onclick="window.open('https://plus.google.com/share?url=<?php the_permalink() ; ?>','sharer','toolbar=0,status=0,width=580,height=325');" class="noo_social" id="g-share"><i class="fa fa-google-plus"></i></a>

            <!-- Pinterest Button -->
            <a href="javascript: void(0)" onclick="window.open('http://pinterest.com/pin/create/button/?url=<?php the_permalink() ; ?>&amp;description=<?php the_title(); ?>','sharer','toolbar=0,status=0,width=580,height=325');" class="noo_social" id="p-share"><i class="fa fa-pinterest"></i></a>
        </div>
    <?php
    endif;
}
add_filter('woocommerce_product_thumbnails_columns','noo_product_thumbnails_columns');
function noo_product_thumbnails_columns(){
    return 4;
}
?>