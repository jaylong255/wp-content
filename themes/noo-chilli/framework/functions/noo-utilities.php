<?php
/**
 * Utilities Functions for NOO Framework.
 * This file contains various functions for getting and preparing data.
 *
 * @package    NOO Framework
 * @version    1.0.0
 * @author     Kan Nguyen <khanhnq@nootheme.com>
 * @copyright  Copyright (c) 2014, NooTheme
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       http://nootheme.com
 */

if (!function_exists('noo_smk_get_all_sidebars')):
	function noo_smk_get_all_sidebars() {
		global $wp_registered_sidebars;
		$sidebars = array();
		$none_sidebars = array();
		for ($i = 1;$i <= 4;$i++) {
			$none_sidebars[] = "noo-top-{$i}";
			$none_sidebars[] = "noo-footer-{$i}";
		}
		if ($wp_registered_sidebars && !is_wp_error($wp_registered_sidebars)) {
			
			foreach ($wp_registered_sidebars as $sidebar) {
				// Don't include Top Bar & Footer Widget Area
				if (in_array($sidebar['id'], $none_sidebars)) continue;
				
				$sidebars[$sidebar['id']] = $sidebar['name'];
			}
		}
		return $sidebars;
	}
endif;

if (!function_exists('noo_get_sidebar_name')):
	function noo_get_sidebar_name($id = '') {
		if (empty($id)) return '';
		
		global $wp_registered_sidebars;
		if ($wp_registered_sidebars && !is_wp_error($wp_registered_sidebars)) {
			foreach ($wp_registered_sidebars as $sidebar) {
				if ($sidebar['id'] == $id) return $sidebar['name'];
			}
		}
		
		return '';
	}
endif;

if (!function_exists('noo_get_sidebar_id')):
	function noo_get_sidebar_id() {
		// Normal Page or Static Front Page
		if ( is_page() || (is_front_page() && get_option('show_on_front') == 'page') ) {
			// Get the sidebar setting from
			$sidebar = noo_get_post_meta(get_the_ID(), '_noo_wp_page_sidebar', '');
			
			return $sidebar;
		}


		// NOO Portfolio
		if( is_post_type_archive( 'portfolio_project' )
			|| is_tax( 'portfolio_category' )
			|| is_tax( 'portfolio_tag' )
			|| is_singular( 'portfolio_project' ) ) {

			$portfolio_layout = noo_get_option('noo_portfolio_layout', 'fullwidth');
			if ($portfolio_layout != 'fullwidth') {
				return noo_get_option('noo_portfolio_sidebar', '');
			}

			return '';
		}



		// Single post page
		if (is_single()) {
			// Check if there's overrode setting in this post.
			$post_id = get_the_ID();
			$override_setting = noo_get_post_meta($post_id, '_noo_wp_post_override_layout', false);
			if ($override_setting) {
				// overrode
				$overrode_layout = noo_get_post_meta($post_id, '_noo_wp_post_layout', 'fullwidth');
				if ($overrode_layout != 'fullwidth') {
					return noo_get_post_meta($post_id, '_noo_wp_post_sidebar', 'sidebar-main');
				}
			} else{

				$post_layout = noo_get_option('noo_blog_post_layout', 'same_as_blog');
				$sidebar = '';
				if ($post_layout == 'same_as_blog') {
					$post_layout = noo_get_option('noo_blog_layout', 'sidebar');
					$sidebar = noo_get_option('noo_blog_sidebar', 'sidebar-main');
				} else {
					$sidebar = noo_get_option('noo_blog_post_sidebar', 'sidebar-main');
				}

				if($post_layout == 'fullwidth'){
					return '';
				}

				return $sidebar;
			}

			return '';
		}

		// Archive page
		if( is_archive() ) {
			$archive_layout = noo_get_option('noo_blog_archive_layout', 'same_as_blog');
			$sidebar = '';
			if ($archive_layout == 'same_as_blog') {
				$archive_layout = noo_get_option('noo_blog_layout', 'sidebar');
				$sidebar = noo_get_option('noo_blog_sidebar', 'sidebar-main');
			} else {
				$sidebar = noo_get_option('noo_blog_archive_sidebar', 'sidebar-main');
			}
			
			if($archive_layout == 'fullwidth'){
				return '';
			}
			return $sidebar;
		}

		// Archive, Index or Home
		if (is_home() || is_archive() || (is_front_page() && get_option('show_on_front') == 'posts')) {
			
			$blog_layout = noo_get_option('noo_blog_layout', 'sidebar');
			if ($blog_layout != 'fullwidth') {
				return noo_get_option('noo_blog_sidebar', 'sidebar-main');
			}
			
			return '';
		}
		
		return '';
	}
endif;

if ( !function_exists('noo_default_primary_color') ) :
	function noo_default_primary_color() {
		return '#ff514a';
	}
endif;
if ( !function_exists('noo_default_font_family') ) :
	function noo_default_font_family() {
		return 'Source Sans Pro';
	}
endif;
if ( !function_exists('noo_default_text_color') ) :
	function noo_default_text_color() {
		return '#000000';
	}
endif;
if ( !function_exists('noo_default_headings_font_family') ) {
	function noo_default_headings_font_family() {
		return 'Source Sans Pro';
	}
}
if ( !function_exists('noo_default_headings_color') ) {
	function noo_default_headings_color() {
		return noo_default_text_color();
	}
}
if ( !function_exists('noo_default_header_bg') ) {
	function noo_default_header_bg() {
		return '#FFFFFF';
	}
}
if ( !function_exists('noo_default_nav_font_family') ) {
	function noo_default_nav_font_family() {
		return noo_default_headings_font_family();
	}
}
if ( !function_exists('noo_default_logo_font_family') ) {
	function noo_default_logo_font_family() {
		return noo_default_headings_font_family();
	}
}
if ( !function_exists('noo_default_logo_color') ) {
	function noo_default_logo_color() {
		return '#ffffff';
	}
}
if ( !function_exists('noo_default_font_size') ) {
	function noo_default_font_size() {
		return '16';
	}
}
if ( !function_exists('noo_default_font_weight') ) {
	function noo_default_font_weight() {
		return '400';
	}
}

//
// This function help to create the dynamic thumbnail width,
// but we don't use it at the moment.
// 
if (!function_exists('noo_thumbnail_width')) :
	function noo_thumbnail_width() {
		$site_layout	= noo_get_option('noo_site_layout', 'fullwidth');
		$page_layout	= noo_get_page_layout();
		$width			= 1200; // max width

		if($site_layout == 'boxed') {
			$site_width = (int) noo_get_option('noo_layout_site_width', '90');
			$site_max_width = (int) noo_get_option('noo_layout_site_max_width', '1200');
			$width = min($width * $site_width / 100, $site_max_width);
		}

		if($page_layout != 'fullwidth') {
			$width = $width * 75 / 100; // 75% of col-9
		}

		return $width;
	}
endif;

if (!function_exists('noo_get_thumbnail_width')) :
	function noo_get_thumbnail_width() {

		// if( is_admin()) {
		// 	return 'admin-thumb';
		// }

		// NOO Portfolio
		if( is_post_type_archive( 'portfolio_project' ) ) {
			// if it's portfolio page, check if the masonry size is fixed or original
			if(noo_get_option('noo_portfolio_masonry_item_size', 'original' ) == 'fixed') {
				$masonry_size = noo_get_post_meta($post_id, '_noo_portfolio_image_masonry_size', 'regular');
				return "masonry-fixed-{$masonry_size}";
			}
		}

		$site_layout	= noo_get_option('noo_site_layout', 'fullwidth');
		$page_layout	= noo_get_page_layout();

		if($site_layout == 'boxed') {
			if($page_layout == 'fullwidth') {
				return 'boxed-fullwidth';
			} else {
				return 'boxed-sidebar';
			}
		} else {
			if($page_layout == 'fullwidth') {
				return 'fullwidth-fullwidth';
			} else {
				return 'fullwidth-sidebar';
			}
		}

		return 'fullwidth-fullwidth';
	}
endif;

if (!function_exists('noo_get_page_layout')):
	function noo_get_page_layout() {
		
		// Normal Page or Static Front Page
		if (is_page() || (is_front_page() && get_option('show_on_front') == 'page')) {
			// WP page,
			// get the page template setting
			$page_id = get_the_ID();
			$page_template = noo_get_post_meta($page_id, '_wp_page_template', 'default');
			
			if (strpos($page_template, 'sidebar') !== false) {
				if (strpos($page_template, 'left') !== false) {
					return 'left_sidebar';
				}
				
				return 'sidebar';
			}
			
			return 'fullwidth';
		}
		


		// WooCommerce
		if( NOO_WOOCOMMERCE_EXIST ) {
			if( is_shop() || is_product_category() || is_product_tag() ){
				return noo_get_option('noo_shop_layout', 'fullwidth');
			}

			if( is_product() ) {
				$product_layout = noo_get_option('noo_woocommerce_product_layout', 'same_as_shop');
				if ($product_layout == 'same_as_shop') {
					$product_layout = noo_get_option('noo_shop_layout', 'fullwidth');
				}
				
				return $product_layout;
			}
		}
		
		// Single post page
		if (is_single()) {

			// WP post,
			// check if there's overrode setting in this post.
			$post_id = get_the_ID();
			$override_setting = noo_get_post_meta($post_id, '_noo_wp_post_override_layout', false);
			if ( !$override_setting ) {
				$post_layout = noo_get_option('noo_blog_post_layout', 'same_as_blog');
				if ($post_layout == 'same_as_blog') {
					$post_layout = noo_get_option('noo_blog_layout', 'sidebar');
				}

				return $post_layout;
			}

			// overrode
			return noo_get_post_meta($post_id, '_noo_wp_post_layout', 'sidebar-main');
		}

		// Archive
		if (is_archive()) {
			$archive_layout = noo_get_option('noo_blog_archive_layout', 'same_as_blog');
			if ($archive_layout == 'same_as_blog') {
				$archive_layout = noo_get_option('noo_blog_layout', 'sidebar');
			}
			
			return $archive_layout;
		}

		// Index or Home
		if (is_home() || (is_front_page() && get_option('show_on_front') == 'posts')) {
			
			return noo_get_option('noo_blog_layout', 'sidebar');
		}
		
		return '';
	}
endif;

if(!function_exists('noo_is_fullwidth')){
	function noo_is_fullwidth(){
		return noo_get_page_layout() == 'fullwidth';
	}
}

if (!function_exists('noo_is_one_page_enabled')):
	function noo_is_one_page_enabled() {
		if( (is_front_page() && get_option('show_on_front' == 'page')) || is_page()) {
			$page_id = get_the_ID();
			return ( noo_get_post_meta( $page_id, '_noo_wp_page_enable_one_page', false ) );
		}

		return false;
	}
endif;

if (!function_exists('noo_get_one_page_menu')):
	function noo_get_one_page_menu() {
		if( noo_is_one_page_enabled() ) {
			if( (is_front_page() && get_option('show_on_front' == 'page')) || is_page()) {
				$page_id = get_the_ID();
				return noo_get_post_meta( $page_id, '_noo_wp_page_one_page_menu', '' );
			}
		}

		return '';
	}
endif;

if (!function_exists('noo_has_home_slider')):
	function noo_has_home_slider() {
		if (class_exists( 'RevSlider' )) {
			if( (is_front_page() && get_option('show_on_front' == 'page')) || is_page()) {
				$page_id = get_the_ID();
				return ( noo_get_post_meta( $page_id, '_noo_wp_page_enable_home_slider', false ) )
					&& ( noo_get_post_meta( $page_id, '_noo_wp_page_slider_rev', '' ) != '' );
			}
		}

		return false;
	}
endif;

if (!function_exists('noo_home_slider_position')):
	function noo_home_slider_position() {
		if (noo_has_home_slider()) {
			return noo_get_post_meta( get_the_ID(), '_noo_wp_page_slider_position', 'below' );
		}

		return '';
	}
endif;

if (!function_exists('noo_is_masonry_style')):
	function noo_is_masonry_style() {
		if( is_post_type_archive( 'portfolio_project' ) || is_tax('portfolio_category') || is_tax('portfolio_tag')  ) {
			return true;
		}

		if(is_home()) {
			return (noo_get_option( 'noo_blog_style' ) == 'masonry');
		}
		
		if(is_archive()) {
			$archive_style = noo_get_option( 'noo_blog_archive_style', 'same_as_blog' );
			if ($archive_style == 'same_as_blog') {
				return (noo_get_option( 'noo_blog_style', 'standard' ) == 'masonry');
			} else {
				return ($archive_style == 'masonry');
			}
		}

		return false;
	}
endif;

if (!function_exists('noo_get_page_heading')):
	function noo_get_page_heading() {
		$heading = '';
		$archive_title = '';
		$archive_desc = '';
		if( ! noo_get_option( 'noo_page_heading', true ) ) {
			return array($heading, $archive_title, $archive_desc);
		}
		if ( is_home() ) {
			$heading = noo_get_option( 'noo_blog_heading_title', esc_html__( 'Blog', 'noo-chilli' ) );
		} elseif ( NOO_WOOCOMMERCE_EXIST && is_shop() ) {
			if( is_search() ) {
				$heading = esc_html__( 'Search Results:', 'noo-chilli' ) . ' ' . esc_attr( get_search_query() );
			} else {
				$heading = noo_get_option( 'noo_shop_heading_title', esc_html__( 'Shop', 'noo-chilli' ) );
			}
		} elseif ( is_search() ) {
			$heading = esc_html__( 'Search Results', 'noo-chilli' );
			global $wp_query;
			if(!empty($wp_query->found_posts)) {
				if($wp_query->found_posts > 1) {
					$heading =  $wp_query->found_posts ." ". esc_html__('Search Results for:', 'noo-chilli' )." ".esc_attr( get_search_query() );
				} else {
					$heading =  $wp_query->found_posts ." ". esc_html__('Search Result for:', 'noo-chilli' )." ".esc_attr( get_search_query() );
				}
			} else {
				if(!empty($_GET['s'])) {
					$heading = esc_html__('Search Results for:', 'noo-chilli' )." ".esc_attr( get_search_query() );
				} else {
					$heading = esc_html__('To search the site please enter a valid term', 'noo-chilli' );
				}
			}
		} elseif ( is_author() ) {
			$curauth = (get_query_var('author_name')) ? get_user_by('slug', get_query_var('author_name')) : get_userdata(get_query_var('author'));
			$heading = esc_html__('Author Archive', 'noo-chilli' );

			if(isset($curauth->nickname)) $heading .= ' ' . esc_html__('for:', 'noo-chilli' )." ".$curauth->nickname;
		} elseif ( is_year() ) {
    		$heading = esc_html__( 'Post Archive by Year: ', 'noo-chilli' ) . get_the_date( 'Y' );
		} elseif ( is_month() ) {
    		$heading = esc_html__( 'Post Archive by Month: ', 'noo-chilli' ) . get_the_date( 'F,Y' );
		} elseif ( is_day() ) {
    		$heading = esc_html__( 'Post Archive by Day: ', 'noo-chilli' ) . get_the_date( 'F j, Y' );
		} elseif ( is_404() ) {
    		$heading = esc_html__( 'Oops! We could not find anything to show to you.', 'noo-chilli' );
    		$archive_title =  esc_html__( 'Would you like going else where to find your stuff.', 'noo-chilli' );
		} elseif ( is_archive() ) {
			$heading        = single_cat_title( '', false );
			// $archive_desc   = term_description();
		} elseif ( is_singular( 'product' ) ) {
			$heading = noo_get_option( 'noo_woocommerce_product_disable_heading', true ) ? '' : get_the_title();
		}  elseif ( is_single() ) {
			$heading = noo_get_option( 'noo_blog_post_disable_heading', true ) ? '' : get_the_title();
		} elseif( is_page() ) {
			if( ! noo_get_post_meta(get_the_ID(), '_noo_wp_page_hide_page_title', false) ) {
				$heading = get_the_title();
			}
		}

		return array($heading, $archive_title, $archive_desc);
	}
endif;

if (!function_exists('noo_get_page_heading_image')):
	function noo_get_page_heading_image() {
		$image = '';
		if( ! noo_get_option( 'noo_page_heading', true ) ) {
			return $image;
		}
		if( NOO_WOOCOMMERCE_EXIST && is_shop() ) {
			$image = noo_get_image_option( 'noo_shop_heading_image', '' );
		} elseif ( is_home() ) {
			$image = noo_get_image_option( 'noo_blog_heading_image', '' );
		} elseif( is_category() || is_tag() ) {
			$queried_object = get_queried_object();
			$image			= noo_get_term_meta( $queried_object->term_id, 'heading_image', '' );
			$image			= empty( $image ) ? noo_get_image_option( 'noo_blog_heading_image', '' ) : $image;
		} elseif( is_tax( 'portfolio_category' ) ) {
			$queried_object = get_queried_object();
			$image			= noo_get_term_meta( $queried_object->term_id, 'heading_image', '' );
			$image			= empty( $image ) ? noo_get_image_option( 'noo_portfolio_heading_image', '' ) : $image;
		} elseif( NOO_WOOCOMMERCE_EXIST && ( is_product_category() || is_product_tag() ) ) {
			$queried_object = get_queried_object();
			$image			= noo_get_term_meta( $queried_object->term_id, 'heading_image', '' );
			$image			= empty( $image ) ? noo_get_image_option( 'noo_shop_heading_image', '' ) : $image;
		} elseif ( is_singular('product' ) || is_single() || is_page() ) {
			$image = noo_get_post_meta(get_the_ID(), '_heading_image', '');
		}

		if( !empty( $image ) && is_numeric( $image ) ) $image = wp_get_attachment_url( $image );

		return $image;
	}
endif;

if( !function_exists('noo_custom_page_heading') ){
    function noo_custom_page_heading(){
        $status = noo_get_option( 'noo_page_heading', true );
        if( is_archive() || is_category() || is_tag() || is_home()){
            $bk_img = '' ;
            $headerimg = noo_get_option('noo_blog_heading_image');
            if ( isset($headerimg) && $headerimg !='' ){
                $bk_img = $headerimg;
            }

        }
        if( is_single() ){
            $bk_img = '';
            $headerimg = noo_get_option('noo_blog_single_heading_image');
            $post_header = noo_get_post_meta(get_the_ID(),'_heading_image','');
            if( isset($post_header) && $post_header != '' ){
                $bk_img = wp_get_attachment_url(esc_attr($post_header),'full');
            }elseif( isset($headerimg) && $headerimg !='' ){
                $bk_img = $headerimg;
            }

        }
        if(is_singular('portfolio_project')){
            $bk_img = '';
            $background_header = noo_get_option('noo_portfolio_single_heading_image');
            $post_header       = get_post_meta(get_the_ID(),'_noo_wp_portfolio_header_background',true);
            if( isset($post_header) && $post_header != '' ){
                $bk_img = wp_get_attachment_url(esc_attr($post_header),'full');
            }else{
                $bk_img = $background_header;
            }
        }
        if (  class_exists( 'WooCommerce' ) ) :
            $title_product = '';
            if(is_product()){
                $bk_img = '';
                $background_header = noo_get_option('noo_product_heading_image');
                $post_header       = get_post_meta(get_the_ID(),'_noo_wp_product_header_product_background',true);
                if( isset($post_header) && $post_header != '' ){
                    $bk_img = wp_get_attachment_url(esc_attr($post_header),'full');
                }else{
                    $bk_img = $background_header;
                }

            }
            if( is_shop() ){
                $bk_img = '' ;
                $headerimg = noo_get_option('noo_shop_heading_image');
                if ( isset($headerimg) && $headerimg !='' ){
                    $bk_img = $headerimg;
                }
            }
        endif;

        if( is_search() ){
            global $wp_query;
            $heading='';
            if(!empty($wp_query->found_posts)) {
                if($wp_query->found_posts > 1) {
                    $heading =  $wp_query->found_posts ." ". esc_html__('Search Results for:', 'noo-chilli' ).esc_attr( get_search_query());
                } else {
                    $heading =  $wp_query->found_posts ." ". esc_html__('Search Result for:', 'noo-chilli' ).esc_attr( get_search_query() );
                }
            } else {
                if(!empty($_GET['s'])) {
                    $heading = esc_html__('No Result for:', 'noo-chilli' ).esc_attr( get_search_query() );
                } else {
                    $heading = esc_html__('To search the site please enter a valid term', 'noo-chilli' );
                }
            }
            $bk_img = '';
            $headerimg = noo_get_option('noo_blog_heading_image');
            if ( isset($headerimg) && $headerimg !='' ){
                $bk_img = $headerimg;
            }
        }
        if(is_page()){
            $bk_img = '';
            $headerimg = noo_get_post_meta(get_the_ID(),'_noo_wp_page_heading_image','');
            $titlepage = noo_get_post_meta(get_the_ID(),'_noo_wp_page_hide_page_title',1);
            $custom_title = noo_get_post_meta(get_the_ID(),'_noo_wp_page_custom_page_title','');
            $page_title = ( isset($custom_title) && $custom_title != ''  ) ? $custom_title : get_the_title();
            if ( isset($headerimg) && $headerimg !='' ){
                $bk_img = wp_get_attachment_url($headerimg);
            }
        }


        if( $status == true && isset($bk_img) && $bk_img !='' ) {
        	$nav_class = noo_get_option( 'noo_header_nav_style', 'relative' ) == 'absolute' ? ' noo_header_has_absolute' : '';
            ?>
            <div class="noo_header_bk <?php echo esc_attr( $nav_class ); ?>" style="background-image: url(<?php echo esc_url($bk_img); ?>)">
                <?php
                if( is_single() ){
                	?>
                	<div class="noo-title-wrap invert">
						<div class="noo-title-block">
                	<?php
                    if( is_singular('portfolio_project') ):
                    ?>
                    	<h1 class="noo-title header-title noo-title-medium"><?php the_title() ?></h1>
                	<?php elseif(  class_exists( 'WooCommerce' ) && is_product() ): ?>
                    	<h2 class="noo-title header-title noo-title-medium"><?php the_terms(get_the_ID(),'product_cat') ?></h2>
                	<?php else: ?>
                    	<h2 class="noo-title header-title noo-title-medium"><?php the_category(','); ?></h2>
                	<?php endif;  ?>
	                    </div>
	                    <p class="noo-breadcrumb"><?php if ( function_exists( 'bcn_display' ) ) bcn_display(); ?></p>
					</div>
                    <?php
                }elseif( is_search() ){
                    ?>
                        <div class="noo-title-wrap invert">
							<div class="noo-title-block">
		                        <h1 class="noo-title header-title noo-title-medium"><?php echo esc_html($heading); ?></h1>
		                    </div>
						</div>
                    <?php
                }elseif( is_page() ){
                    ?>
                        <?php if( $titlepage != 1 ): ?>
                        
						<div class="noo-title-wrap invert">
							<div class="noo-title-block">
		                        <h1 class="noo-title header-title noo-title-medium"><?php echo esc_html($page_title); ?></h1>
		                    </div>
		                    <p class="noo-breadcrumb"><?php if ( function_exists( 'bcn_display' ) ) bcn_display(); ?></p>
						</div>
						
						<?php endif; ?>
                <?php }elseif(  NOO_WOOCOMMERCE_EXIST && is_shop()  ){ ?>
                		<div class="noo-title-wrap invert">
							<div class="noo-title-block">
                    			<h1 class="noo-title header-title noo-title-medium"><?php echo esc_html(noo_get_option('noo_shop_heading_title','Shop')); ?></h1>
                    		</div>
		                    <p class="noo-breadcrumb"><?php if ( function_exists( 'bcn_display' ) ) bcn_display(); ?></p>
						</div>
                    <?php
                }else{
                    ?>
                    <div class="noo-title-wrap invert">
						<div class="noo-title-block">
	                        <h1 class="noo-title header-title noo-title-medium"><?php
	                        if ( is_category() ) {
	                            $title = sprintf( esc_html__( 'Category: %s', 'noo-chilli' ), single_cat_title( '', false ) );
	                        } elseif ( is_tag() ) {
	                            $title = sprintf( esc_html__( 'Tag: %s', 'noo-chilli' ), single_tag_title( '', false ) );
	                        } elseif ( is_author() ) {
	                            $title = sprintf( esc_html__( 'Author: %s', 'noo-chilli' ), get_the_author() );
	                        } elseif ( is_year() ) {
	                            $title = sprintf( esc_html__( 'Year: %s', 'noo-chilli' ), get_the_date('Y') );
	                        } elseif ( is_month() ) {
	                            $title = sprintf( esc_html__( 'Month: %s', 'noo-chilli' ), get_the_date('F Y') );
	                        } elseif ( is_day() ) {
	                            $title = sprintf( esc_html__( 'Day: %s', 'noo-chilli' ), get_the_date('F j, Y') );
	                        } elseif ( is_post_type_archive() ) {
	                            $title = sprintf( esc_html__( 'Archives: %s', 'noo-chilli' ), post_type_archive_title( '', false ) );
	                        } elseif ( is_tax() ) {
	                            $tax = get_taxonomy( get_queried_object()->taxonomy );
	                            /* translators: 1: Taxonomy singular name, 2: Current taxonomy term */
	                            $title = sprintf( esc_html__( '%1$s: %2$s', 'noo-chilli' ), $tax->labels->singular_name, single_term_title( '', false ) );
	                        } elseif( is_home() ){
	                            $title = noo_get_option('noo_blog_heading_title','Blog');
	                        }else {
	                            $title = esc_html__( 'Archives' , 'noo-chilli' );
	                        }
	                        echo esc_html($title)
	                        ?></h1>
	                    </div>
	                    <p class="noo-breadcrumb"><?php if ( function_exists( 'bcn_display' ) ) bcn_display(); ?></p>
					</div>
                    
                    <?php
                }
                ?>
            </div>
        <?php
        }
    }
}

if (!function_exists('noo_get_post_format')):
	function noo_get_post_format($post_id = null, $post_type = '') {
		$post_id = (null === $post_id) ? get_the_ID() : $post_id;
		$post_type = ('' === $post_type) ? get_post_type($post_id) : $post_type;

		$post_format = '';
		
		if ($post_type == 'post') {
			$post_format = get_post_format($post_id);
		}
		
		if ($post_type == 'portfolio_project') {
			$post_format = noo_get_post_meta($post_id, '_noo_portfolio_media_type', 'image');
		}

		return $post_format;
	}
endif;

if (!function_exists('noo_has_featured_content')):
	function noo_has_featured_content($post_id = null) {
		$post_id = (null === $post_id) ? get_the_ID() : $post_id;

		$post_type = get_post_type($post_id);
		$prefix = '';
		$post_format = '';
		
		if ($post_type == 'post') {
			$prefix = '_noo_wp_post';
			$post_format = get_post_format($post_id);
		}
		
		if ($post_type == 'portfolio_project') {
			$prefix = '_noo_portfolio';
			$post_format = noo_get_post_meta($post_id, "{$prefix}_media_type", 'image');
		}
		
		switch ($post_format) {
			case 'image':
				$main_image = noo_get_post_meta($post_id, "{$prefix}_main_image", 'featured');
				if( $main_image == 'featured') {
					return has_post_thumbnail($post_id);
				}

				return has_post_thumbnail($post_id) || ( (bool)noo_get_post_meta($post_id, "{$prefix}_image", '') );
			case 'gallery':
				if (!is_singular()) {
					$preview_content = noo_get_post_meta($post_id, "{$prefix}_gallery_preview", 'slideshow');
					if ($preview_content == 'featured') {
						return has_post_thumbnail($post_id);
					}
				}
				
				return (bool)noo_get_post_meta($post_id, "{$prefix}_gallery", '');
			case 'video':
				if (!is_singular()) {
					$preview_content = noo_get_post_meta($post_id, "{$prefix}_preview_video", 'both');
					if ($preview_content == 'featured') {
						return has_post_thumbnail($post_id);
					}
				}
				
				$m4v_video = (bool)noo_get_post_meta($post_id, "{$prefix}_video_m4v", '');
				$ogv_video = (bool)noo_get_post_meta($post_id, "{$prefix}_video_ogv", '');
				$embed_video = (bool)noo_get_post_meta($post_id, "{$prefix}_video_embed", '');
				
				return $m4v_video || $ogv_video || $embed_video;
			case 'link':
			case 'quote':
				return false;
				
			case 'audio':
				$mp3_audio = (bool)noo_get_post_meta($post_id, "{$prefix}_audio_mp3", '');
				$oga_audio = (bool)noo_get_post_meta($post_id, "{$prefix}_audio_oga", '');
				$embed_audio = (bool)noo_get_post_meta($post_id, "{$prefix}_audio_embed", '');
				return $mp3_audio || $oga_audio || $embed_audio;
			default: // standard post format
				return has_post_thumbnail($post_id);
		}
		
		return false;
	}
endif;

if (!function_exists('noo_get_page_link_by_template')):
	function noo_get_page_link_by_template( $page_template ) {
		$pages = get_pages(array(
			'meta_key' => '_wp_page_template',
			'meta_value' => $page_template
		));

		if( $pages ){
			$link = get_permalink( $pages[0]->ID );
		}else{
			$link=home_url();
		}
		return $link;
	}
endif;

if (!function_exists('noo_current_url')):
	function noo_current_url($encoded = false) {
		global $wp;
		$current_url = esc_url( add_query_arg( $wp->query_string, '', home_url( $wp->request ) ) );
		if( $encoded ) {
			return urlencode($current_url);
		}
		return $current_url;
	}
endif;

if (!function_exists('noo_upload_dir_name')):
	function noo_upload_dir_name() {
		return 'noo_chilli';
	}
endif;

if (!function_exists('noo_upload_dir')):
	function noo_upload_dir() {
		$upload_dir = wp_upload_dir();

		return $upload_dir['basedir'] . '/' . noo_upload_dir_name();
	}
endif;

if (!function_exists('noo_upload_url')):
	function noo_upload_url() {
		$upload_dir = wp_upload_dir();

		return $upload_dir['baseurl'] . '/' . noo_upload_dir_name();
	}
endif;

if (!function_exists('noo_create_upload_dir')):
	function noo_create_upload_dir( $wp_filesystem = null ) {
		if( empty( $wp_filesystem ) ) {
			return false;
		}

		$upload_dir = wp_upload_dir();
		global $wp_filesystem;

		$noo_upload_dir = $wp_filesystem->find_folder( $upload_dir['basedir'] ) . noo_upload_dir_name();
		if ( ! $wp_filesystem->is_dir( $noo_upload_dir ) ) {
			if ( $wp_filesystem->mkdir( $noo_upload_dir, 0777 ) ) {
				return $noo_upload_dir;
			}

			return false;
		}

		return $noo_upload_dir;
	}
endif;


if (!function_exists('noo_handle_upload_file')):
	function noo_handle_upload_file($upload_data) {
		$return = false;
		$uploaded_file = wp_handle_upload($upload_data, array('test_form' => false));

		if (isset($uploaded_file['file'])) {
			$file_loc = $uploaded_file['file'];
			$file_name = basename($upload_data['name']);
			$file_type = wp_check_filetype($file_name);

			$attachment = array(
				'post_mime_type' => $file_type['type'],
				'post_title' => preg_replace('/\.[^.]+$/', '', basename($file_name)),
				'post_content' => '',
				'post_status' => 'inherit'
				);

			$attach_id = wp_insert_attachment($attachment, $file_loc);
			$attach_data = wp_generate_attachment_metadata($attach_id, $file_loc);
			wp_update_attachment_metadata($attach_id, $attach_data);

			$return = array('data' => $attach_data, 'id' => $attach_id);

			return $return;
		}

		return $return;
	}
endif;


// Get allowed HTML tag.
if( !function_exists('noo_allowed_html') ) :
	function noo_allowed_html() {
		return apply_filters( 'noo_allowed_html', array(
			'a' => array(
				'href' => array(),
				'target' => array(),
				'title' => array(),
				'rel' => array(),
				'class' => array(),
				'style' => array(),
			),
			'img' => array(
				'src' => array(),
				'class' => array(),
				'style' => array(),
			),
			'h1' => array(),
			'h2' => array(),
			'h3' => array(),
			'h4' => array(),
			'h5' => array(),
			'p' => array(
				'class' => array(),
				'style' => array()
			),
			'br' => array(
				'class' => array(),
				'style' => array()
			),
			'hr' => array(
				'class' => array(),
				'style' => array()
			),
			'span' => array(
				'class' => array(),
				'style' => array()
			),
			'em' => array(
				'class' => array(),
				'style' => array()
			),
			'strong' => array(
				'class' => array(),
				'style' => array()
			),
			'small' => array(
				'class' => array(),
				'style' => array()
			),
			'b' => array(
				'class' => array(),
				'style' => array()
			),
			'i' => array(
				'class' => array(),
				'style' => array()
			),
			'u' => array(
				'class' => array(),
				'style' => array()
			),
			'ul' => array(
				'class' => array(),
				'style' => array()
			),
			'ol' => array(
				'class' => array(),
				'style' => array()
			),
			'li' => array(
				'class' => array(),
				'style' => array()
			),
			'blockquote' => array(
				'class' => array(),
				'style' => array()
			),
		) );
	}
endif;


// Allow only unharmed HTML tag.
if( !function_exists('noo_html_content_filter') ) :
	function noo_html_content_filter( $content = '' ) {
		return wp_kses( $content, noo_allowed_html() );
	}
endif;
