<?php
if (!function_exists('noo_the_breadcrumbs')):
	
	function noo_the_breadcrumbs() {
		global $post;
		echo '<ul class="breadcrumb">';
		if (!is_home()) {
			
			echo '<li>';
			echo '<a href="' . home_url() . '">';
			echo '<i class="nooicon-home"></i>';
			echo esc_html__('Home', 'noo-chilli' );
			echo '</a>';
			echo '</li>';
			if (is_category()) {
				$the_cat = get_category(get_query_var('cat') , false);
				if ($the_cat->parent != 0) {
					echo '<li>' . get_category_parents($the_cat->parent, true, '</li><li>');
					echo '</li>';
				}
				echo '<li class="active"><span>';
				echo single_cat_title('', false);
				echo '</span></li>';
			}elseif ( is_tax( 'product_cat' ) ){
				$current_term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
				$ancestors = array_reverse( get_ancestors( $current_term->term_id, get_query_var( 'taxonomy' ) ) );
				foreach ( $ancestors as $ancestor ) {
					$ancestor = get_term( $ancestor, get_query_var( 'taxonomy' ) );
					echo '<li>';
					echo '<a href="' . get_term_link( $ancestor->slug, get_query_var( 'taxonomy' ) ) . '">' . esc_html( $ancestor->name ) . '</a>';
					echo '</li>';
						
				}
				echo '<li class="active"><span>';
				echo esc_html( $current_term->name );
				echo '</span></li>';
			}elseif ( is_tax( 'product_lookbook' ) ) {
				global $wp_query;
				$queried_object = $wp_query->get_queried_object();
				echo '<li class="active"><span>' . esc_html__('Products in Lookbook: ', 'noo-chilli' ) . '&#8220;' . $queried_object->name. '&#8221;' . '</span></li>';
			}elseif ( is_tax( 'product_tag' ) ) {
				global $wp_query;
				$queried_object = $wp_query->get_queried_object();
				echo '<li class="active"><span>' . esc_html__('Products tagged as ', 'noo-chilli' ) . '&#8220;' . $queried_object->name. '&#8221;' . '</span></li>';
			}elseif (is_tag()) {
				echo '<li class="active"><span>' . esc_html__('Posts Tagged as ', 'noo-chilli' ) . '&#8220;' . single_tag_title('', false) . '&#8221;' . '</span></li>';
			} elseif (is_singular('portfolio_project')) {
				$portfolio_page = noo_get_option('noo_portfolio_page', '');
				echo '<li>';
				echo '<a href="' . ( home_url('/') . ( !$portfolio_page ? get_post( $portfolio_page )->post_name : 'noo-portfolio' ) ) . '" title="' . esc_attr__('View All Portfolio', 'noo-chilli' ) . '">' . noo_get_option('noo_portfolio_heading_title', esc_html__( 'My Portfolio', 'noo-chilli' )) . '</a> ';
				echo '</li>';
				echo '<li class="active"><span>';
				the_title();
				echo '</span></li>';
			}elseif (NOO_WOOCOMMERCE_EXIST && is_post_type_archive( 'product' )){
				if (!is_search()) {
					echo '<li class="active"><span>' . esc_html__('Search Results', 'noo-chilli' );
					echo '</span></li>';
				} else {
					echo '<li class="active"><span>' . get_the_title() . '</span></li>';
				}
			}elseif (is_page()) {
				
				if ($post->post_parent) {
					$anc = get_post_ancestors($post->ID);
					$title = get_the_title();
					foreach ($anc as $ancestor) {
						echo '<li><a href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
					}
					echo '<li class="active"><span>' . $title . '</span></li>';
				} else {
					echo '<li class="active"><span>' . get_the_title() . '</span></li>';
				}
			}elseif (is_singular()) {
				if ($post->post_parent) {
					echo '<li>';
					the_category('</li><li>');
					echo '</li>';
				}
				echo '<li class="active"><span>';
				the_title();
				echo '</span></li>';
			}elseif (is_author()){
				global $author;
				$userdata = get_userdata($author);
				echo '<li class="active"><span>' . esc_html__('Posts by ', 'noo-chilli' ) . '&#8220;' . $userdata->display_name . '&#8221;' . '</span></li>';
				
			}elseif (is_day()) {
				echo '<li class="active"><span>' . esc_html__('Archive for', 'noo-chilli' );
				the_time('F j, Y');
				echo '</span></li>';
			} elseif (is_month()) {
				echo '<li class="active"><span>' . esc_html__('Archive for', 'noo-chilli' );
				the_time('F, Y');
				echo '</span></li>';
			} elseif (is_year()) {
				echo '<li class="active"><span>' . esc_html__('Archive for', 'noo-chilli' );
				the_time('Y');
				echo '</span></li>';
			} elseif (get_query_var('paged')) {
				echo '<li class="active"><span>' . esc_html__('Blog Archives', 'noo-chilli' );
				echo '</span></li>';
			} elseif (is_search()) {
				echo '<li class="active"><span>' . esc_html__('Search Results', 'noo-chilli' );
				echo '</span></li>';
			}
			
		}
		echo '</ul>';
	}
endif;
