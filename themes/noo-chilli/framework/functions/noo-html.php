<?php
/**
 * HTML Functions for NOO Framework.
 * This file contains various functions used for rendering site's small layouts.
 *
 * @package    NOO Framework
 * @version    1.0.0
 * @author     Kan Nguyen <khanhnq@nootheme.com>
 * @copyright  Copyright (c) 2014, NooTheme
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       http://nootheme.com
 */


// Pagination
require_once NOO_FRAMEWORK_FUNCTION . '/noo-html-pagination.php';

require_once NOO_FRAMEWORK_FUNCTION . '/noo-html-breadcrumbs.php';

if (!function_exists('noo_content_meta')):
	function noo_content_meta() {
		$post_type = get_post_type();

		if ( $post_type == 'post' ) {
			if ((!is_single() && noo_get_option( 'noo_blog_show_post_meta', true ) === false)
				|| (is_single() && noo_get_option( 'noo_blog_post_show_post_meta', true ) === false)) {
				return;
			}
		} elseif ($post_type == 'portfolio_project') {
			if (noo_get_option( 'noo_portfolio_show_post_meta', true ) === false) {
				return;
			}
		}

		$html = array();
		$html[] = '<p class="content-meta">';
		// Author
		$html[] = '<span><i class="fa fa-pencil"></i> ' . get_the_author() . '</span>';
		// Date
		$html[] = '<span>';
		$html[] = '<time class="entry-date" datetime="' . esc_attr(get_the_date('c')) . '">';
		$html[] = '<i class="fa fa-calendar"></i>';
		$html[] = esc_html(get_the_date());
		$html[] = '</time>';
		$html[] = '</span>';
		// Categories
		$categories_html = '';
		$separator = ', ';

		if (get_post_type() == 'portfolio_project') {
			if (has_term('', 'portfolio_category', NULL)) {
				$categories = get_the_terms(get_the_id() , 'portfolio_category');
				foreach ($categories as $category) {
					$categories_html .= '<a' . ' href="' . get_term_link($category->slug, 'portfolio_category') . '"' . ' title="' . esc_attr( sprintf( esc_html__("View all Portfolio Items in: &ldquo;%s&rdquo;", 'noo-chilli' ) , $category->name ) ) . '">' . '<i class="fa fa-bookmark"></i> ' . $category->name . '</a>' . $separator;
				}
			}
		} else {
			$categories = get_the_category();
			foreach ($categories as $category) {
				$categories_html.= '<a' . ' href="' . get_category_link($category->term_id) . '"' . ' title="' . esc_attr( sprintf( esc_html__("View all posts in: &ldquo;%s&rdquo;", 'noo-chilli' ) , $category->name ) ) . '">' . '<i class="fa fa-archive"></i> ' . $category->name . '</a>' . $separator;
			}
		}

		$html[] = '<span>' . trim($categories_html, $separator) . '</span>';
		// Comments
		$comments_html = '';

		if (comments_open()) {
			$comment_title = '';
			$comment_number = '';
			if (get_comments_number() == 0) {
				$comment_title = sprintf( esc_html__('Leave a comment on: &ldquo;%s&rdquo;', 'noo-chilli' ) , get_the_title() );
				$comment_number = esc_html__(' Leave a Comment', 'noo-chilli' );
			} else if (get_comments_number() == 1) {
				$comment_title = sprintf( esc_html__('View a comment on: &ldquo;%s&rdquo;', 'noo-chilli' ) , get_the_title() );
				$comment_number = ' 1 ' . esc_html__('Comment', 'noo-chilli' );
			} else {
				$comment_title = sprintf( esc_html__('View all comments on: &ldquo;%s&rdquo;', 'noo-chilli' ) , get_the_title() );
				$comment_number =  ' ' . get_comments_number() . ' ' . esc_html__('Comments', 'noo-chilli' );
			}
			
			$comments_html.= '<span><a' . ' href="' . esc_url(get_comments_link()) . '"' . ' title="' . esc_attr($comment_title) . '"' . ' class="meta-comments">' . '<i class="fa fa-comments"></i>' . $comment_number . '</a></span>';
		}

		$html[] = $comments_html;

		echo implode($html, "\n");
	}
endif;

if (!function_exists('noo_get_readmore_link')):
	function noo_get_readmore_link() {
        $read = noo_get_option('noo_blog_show_readmore', 1 );
        if($read == 1):
		//     return '<span class="noo-more"><a href="' . get_permalink() . '" class="read-more">'
		// . esc_html__('Read more', 'noo-chilli' )
		// . '</a></span>';
        else:

        endif;
	}
endif;

if (!function_exists('noo_readmore_link')):
	function noo_readmore_link() {
        $read = noo_get_option('noo_blog_show_readmore', 1 );
		if( $read == 1  ) {
			echo noo_get_readmore_link();
		} else {
			echo '';
		}
	}
endif;
function noo_relative_time($a=''){
    return human_time_diff($a, current_time( 'timestamp' ));
}
if (!function_exists('noo_list_comments')):
	function noo_list_comments($comment, $args, $depth) {
		$GLOBALS['comment'] = $comment;
		GLOBAL $post;
		$avatar_size = isset($args['avatar_size']) ? $args['avatar_size'] : 60;
?>
		<li id="li-comment-<?php comment_ID(); ?>" <?php comment_class(); ?>>
			<div class="comment-wrap">
				<div class="comment-img">
					<div class="img-thumbnail">
						<?php echo get_avatar($comment, $avatar_size); ?>
					</div>
				</div>
				<div id="comment-<?php comment_ID(); ?>" class="comment-block">
					<header class="comment-header">
						<cite class="comment-author"><?php echo get_comment_author_link(); ?></cite>
                        <?php if ($comment->user_id === $post->post_author):
                            ?>
                            <span class="ispostauthor">
                                <?php esc_html_e('Author', 'noo-chilli' ); ?>
                            </span>
                        <?php
                        endif; ?>
						<div class="comment-meta">
							<time datetime="<?php echo get_comment_time('c'); ?>">
                                <?php echo sprintf( esc_html__('%1$s ago', 'noo-chilli' ) , esc_html(noo_relative_time(get_comment_date('U')))); ?>
							</time>
							<span class="comment-edit">
								<?php edit_comment_link('<i class="fa fa-edit"></i> ' . esc_html__('Edit', 'noo-chilli' )); ?>
							</span>
						</div>
						<?php if ('0' == $comment->comment_approved): ?>
							<p class="comment-pending"><?php esc_html_e('Your comment is awaiting moderation.', 'noo-chilli' ); ?></p>
						<?php
		endif; ?>
					</header>
					<div class="comment-content">
						<?php comment_text(); ?>
					</div>
					<span class="pull-left">
							<?php comment_reply_link(array_merge($args, array(
			'reply_text' => (' <span class="comment-reply-link-after"><i class="fa fa-reply"></i></span>' . esc_html__('Reply', 'noo-chilli' )) ,
			'depth' => $depth,
			'max_depth' => $args['max_depth']
		))); ?>
						</span>
				</div>
			</div>
		<?php
	}
endif;

if ( ! function_exists( 'noo_portfolio_attributes' ) ) :
	function noo_portfolio_attributes( $post_id = null ) {
		if ( noo_get_option( 'noo_portfolio_enable_attribute', true ) === false) {
			return '';
		}

		$post_id = (null === $post_id) ? get_the_id() : $post_id;
		$attributes = get_the_terms( $post_id, 'portfolio_tag' );

		$html = array();
		$html[] = '<ul class="list-unstyled attribute-list">';
		$i=0;
		foreach( $attributes as $attribute ) {
			$html[] = '<li class="'.($i % 2 == 0 ? 'odd':'even').'">';
			$html[] = '<a href="' . get_term_link( $attribute->slug, 'portfolio_tag' ) . '">';
			$html[] = '<i class="fa fa-check"></i>';
			$html[] = $attribute->name;
			$html[] = '</a>';
			$html[] = '</li>';
			$i++;
		};
		$html[] = '</ul>';

		echo implode($html, "\n");
	}
endif;

if ( ! function_exists( 'noo_social_share' ) ) :
	function noo_social_share( $post_id = null ) {
		$post_id = (null === $post_id) ? get_the_id() : $post_id;
		$post_type =  get_post_type($post_id);
		$prefix = 'noo_blog';

		if($post_type == 'portfolio_project' ) {
			$prefix = 'noo_portfolio';
		}

		if(noo_get_option("{$prefix}_social", true ) === false) {
			return '';
		}

		$share_url     = urlencode( get_permalink() );
		$share_title   = urlencode( get_the_title() );
		$share_source  = urlencode( get_bloginfo( 'name' ) );
		$share_content = urlencode( get_the_content() );
		$share_media   = wp_get_attachment_thumb_url( get_post_thumbnail_id() );
		$popup_attr    = 'resizable=0, toolbar=0, menubar=0, status=0, location=0, scrollbars=0';

		$share_title  = noo_get_option( "{$prefix}_social_title", '' );
		$facebook     = noo_get_option( "{$prefix}_social_facebook", true );
		$twitter      = noo_get_option( "{$prefix}_social_twitter", true );
		$google		  = noo_get_option( "{$prefix}_social_google", true );
		$pinterest    = noo_get_option( "{$prefix}_social_pinterest", false );
		$linkedin     = noo_get_option( "{$prefix}_social_linkedin", false );
		$html = array();

		if ( $facebook || $twitter || $google || $pinterest || $linkedin ) {
			$html[] = '<div class="content-share">';
			if( $share_title !== '' ) {
				$html[] = '<p class="social-title">';
				$html[] = '  ' . $share_title;
				$html[] = '</p>';
			}
			$html[] = '<div class="noo-social social-share">';

			if($facebook) {
				$html[] = '<a href="#share" data-toggle="tooltip" data-placement="bottom" data-trigger="hover" class="noo-share"'
							. ' title="' . esc_html__( 'Share on Facebook', 'noo-chilli' ) . '"'
							. ' onclick="window.open(' 
								. "'http://www.facebook.com/sharer.php?u=" . esc_url($share_url) . "&amp;t=" . esc_attr( $share_title ) . "','popupFacebook','width=650,height=270," . esc_attr( $popup_attr ) . "');"
								. ' return false;">';
				$html[] = '<i class="fa fa-facebook"></i>';
				$html[] = '</a>';
			}

			if($twitter) {
				$html[] = '<a href="#share" class="noo-share"'
							. ' title="' . esc_html__( 'Share on Twitter', 'noo-chilli' ) . '"'
							. ' onclick="window.open('
								. "'https://twitter.com/intent/tweet?text=" . esc_attr( $share_title ) . "&amp;url=" . esc_url($share_url) . "','popupTwitter','width=500,height=370," . esc_attr( $popup_attr ) . "');"
								. ' return false;">';
				$html[] = '<i class="fa fa-twitter"></i></a>';
			}

			if($google) {
				$html[] = '<a href="#share" class="noo-share"'
							. ' title="' . esc_html__( 'Share on Google+', 'noo-chilli' ) . '"'
								. ' onclick="window.open('
								. "'https://plus.google.com/share?url=" . esc_url($share_url) . "','popupGooglePlus','width=650,height=226," . esc_attr( $popup_attr ) . "');"
								. ' return false;">';
				$html[] = '<i class="fa fa-google-plus"></i></a>';
			}

			if($pinterest) {
				$html[] = '<a href="#share" class="noo-share"'
							. ' title="' . esc_html__( 'Share on Pinterest', 'noo-chilli' ) . '"'
							. ' onclick="window.open('
								. "'http://pinterest.com/pin/create/button/?url=" . esc_url($share_url) . "&amp;media=" . esc_attr( $share_media ) . "&amp;description=" . esc_attr( $share_title ) . "','popupPinterest','width=750,height=265," . esc_attr( $popup_attr ) . "');"
								. ' return false;">';
				$html[] = '<i class="fa fa-pinterest"></i></a>';
			}

			if($linkedin) {
				$html[] = '<a href="#share" class="noo-share"'
							. ' title="' . esc_html__( 'Share on LinkedIn', 'noo-chilli' ) . '"'
							. ' onclick="window.open('
								. "'http://www.linkedin.com/shareArticle?mini=true&amp;url=" . esc_url($share_url) . "&amp;title=" . esc_attr( $share_title ) . "&amp;summary=" . esc_attr( $share_content ) . "&amp;source=" . esc_attr( $share_source ) . "','popupLinkedIn','width=610,height=480," . esc_attr( $popup_attr ) . "');"
								. ' return false;">';
				$html[] = '<i class="fa fa-linkedin"></i></a>';
			}

			$html[] = '</div>'; // .noo-social.social-share
			$html[] = '</div>'; // .share-wrap
		}

		echo implode("\n", $html);
	}
endif;

if (!function_exists('noo_social_icons')):
	function noo_social_icons() {
		$html = array();
		$html[] = '<ul class="noo-social pull-right">';
		
		$social_list = array(
			'facebook' => esc_html__('Facebook', 'noo-chilli' ) ,
			'twitter' => esc_html__('Twitter', 'noo-chilli' ) ,
			'google-plus' => esc_html__('Google+', 'noo-chilli' ) ,
			'pinterest' => esc_html__('Pinterest', 'noo-chilli' ) ,
			'linkedin' => esc_html__('LinkedIn', 'noo-chilli' ) ,
			'rss' => esc_html__('RSS', 'noo-chilli' ) ,
			'youtube' => esc_html__('YouTube', 'noo-chilli' ) ,
			'instagram' => esc_html__('Instagram', 'noo-chilli' ) ,
		);
		
		$social_html = array();
		foreach ($social_list as $key => $title) {
			$social = noo_get_option("noo_social_{$key}", '');
			if ($social) {
				$social_html[] = '<li><a href="' . esc_url( $social ) . '" title="' . $title . '" target="_blank">';
				$social_html[] = '<i class="fa fa-' . $key . '"></i>';
				$social_html[] = '</a></li>';
			}
		}

		if(empty($social_html)) {
			$social_html[] = '<li><a href="#">' . esc_html__('No Social Media Link', 'noo-chilli' ) . '</a></li>';
		}
		$html[] = implode($social_html, "\n");
		$html[] = '</ul>';
        $status_social = noo_get_option("status_social", '');
        if($status_social == 1):
		echo implode($html, "\n");
        endif;
	}
endif;

if(!function_exists('noo_gototop')):
	function noo_gototop(){
		if( noo_get_option( 'noo_back_to_top', true ) ) {
			echo '<a href="#" class="go-to-top hidden-print"><i class="fa fa-angle-up"></i></a>';
		}
		return ;
	}
	add_action('wp_footer','noo_gototop');
endif;

