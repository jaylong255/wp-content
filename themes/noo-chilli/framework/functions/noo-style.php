<?php
/**
 * Style Functions for NOO Framework.
 * This file contains functions for calculating style (normally it's css class) base on settings from admin side.
 *
 * @package    NOO Framework
 * @version    1.0.0
 * @author     Kan Nguyen <khanhnq@nootheme.com>
 * @copyright  Copyright (c) 2014, NooTheme
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       http://nootheme.com
 */

if (!function_exists('noo_body_class')):
	function noo_body_class($output) {
		global $wp_customize;
		if (isset($wp_customize)) {
			$output[] = 'is-customize-preview';
		}

		$page_layout = noo_get_page_layout();
		if ($page_layout == 'fullwidth') {
			$output[] = ' page-fullwidth';
		} elseif ($page_layout == 'left_sidebar') {
			$output[] = ' page-left-sidebar';
		} else {
			$output[] = ' page-right-sidebar';
		}

		if( noo_get_option('noo_layout_rtl', 'noo-chilli' ) == 'yes' ){
        	$output[] ='theme-rtl';
        }
		
		switch (noo_get_option('noo_site_layout', 'fullwidth')) {
			case 'boxed':
				// if(get_page_template_slug() != 'page-full-width.php')
				$output[] = 'boxed-layout';
			break;
			default:
				$output[] = 'full-width-layout';
			break;
		}

		// SmoothScroll
		if( noo_get_option( 'noo_smooth_scrolling', false ) ) {
			$output[] = 'enable-nice-scroll';
		}

		// Fixed left and/or right Navigation
		$navbar_position = noo_get_option('noo_header_nav_position', 'fixed_top');
		if ($navbar_position == 'fixed_left') {
			$output[] = 'navbar-fixed-left-layout';
		} elseif ($navbar_position == 'fixed_right') {
			$output[] = 'navbar-fixed-right-layout';
		}

		if( noo_is_one_page_enabled() ) {
			$output[] = 'one-page-layout';
		}

        $page_trans      = noo_get_post_meta(get_the_ID(),'_noo_wp_page_menu_transparent');
        $post_trans      = noo_get_post_meta(get_the_ID(),'_noo_wp_post_menu_transparent');
        $portfolio_trans = noo_get_post_meta(get_the_ID(),'_noo_wp_portfolio_menu_transparent');
        if( is_page() ):
            if(isset($page_trans) && $page_trans !=0){

                $output[] ='construc-page-menu-transparent';
            }
        endif;
        if( is_single() ):

            if(isset($post_trans) && $post_trans != 0){

                $output[] ='construc-page-menu-transparent';
            }
            if(isset($portfolio_trans) && $portfolio_trans != 0){

                $output[] ='construc-page-menu-transparent';
            }
        endif;

        if( is_archive() || is_home() ){
            if( noo_get_option('noo_blog_show_posttransparent',false) == true ){
                $output[] ='construc-page-menu-transparent';
            }
        }
        
		return $output;
	}
endif;
add_filter('body_class', 'noo_body_class');

if (!function_exists('noo_header_class')):
	function noo_header_class() {
		$class = '';

		$navbar_position = noo_get_option('noo_header_nav_position', 'fixed_top');
		if ($navbar_position == 'fixed_top') {
			$floating = noo_get_option( 'noo_header_nav_floating', false );
			if( $floating ) {
				if( noo_get_option( 'noo_header_nav_floating_bg_color', 'transparent' ) == 'transparent'
					&& noo_get_option( 'noo_header_nav_floating_offset_top', '0' ) == '0' )
				$class .= ' has-border';
			}
		}

		echo esc_attr( $class );
	}
endif;

if (!function_exists('noo_navbar_class')):
	function noo_navbar_class() {
        $class = 'noo-header-yesfix';
		$navbar_position = noo_get_option('noo_header_nav_position', 'fixed_top');
		if ($navbar_position == 'static_top') {
			$class = '';
		}

		echo esc_attr( $class );
	}
endif;

if (!function_exists('noo_main_class')):
	function noo_main_class() {
		$class = 'noo-main';
		$page_layout = noo_get_page_layout();
		if ($page_layout == 'fullwidth') {
			$class.= ' col-md-12';
		} elseif ($page_layout == 'left_sidebar') {
			$class.= ' col-md-9 left-sidebar';
		} else {
			$class.= ' col-md-9';
		}
		
		echo esc_attr( $class );
	}
endif;

if(!function_exists('noo_container_class')){
	function noo_container_class(){
		
		if(noo_is_fullwidth()) {
			echo  'container-fullwidth';
			return;
		}
		echo 'container-boxed max offset';
	}
}

if (!function_exists('noo_sidebar_class')):
	function noo_sidebar_class() {
		$class = ' noo-sidebar col-md-4';
		$page_layout = noo_get_page_layout();
		
		if ( $page_layout == 'left_sidebar' ) {
			$class .= ' noo-sidebar-left pull-left';
		}
		
		echo esc_attr( $class );
	}
endif;

if (!function_exists('noo_blog_class')):
	function noo_blog_class() {
		$class = ' post-area';
		$blog_style = noo_get_option('noo_blog_style', 'standard');
		
		if ($blog_style == 'masonry') {
			$class.= ' masonry-blog';
		} else {
			$class.= ' standard-blog';
		}
		
		echo esc_attr( $class );
	}
endif;

if (!function_exists('noo_page_class')):
	function noo_page_class() {
		$class = ' noo-page';
		
		echo esc_attr( $class );
	}
endif;

if (!function_exists('noo_portfolio_class')):
	function noo_portfolio_class() {
		$class = ' post-area';
		// $portfolio_style = noo_get_option('noo_portfolio_style', 'masonry');
		
		// if ($portfolio_style == 'standard') {
		// 	$class .= ' standard-portfolio';
		// } else {
			$class .= ' masonry-portfolio';
			if(noo_get_option('noo_portfolio_items_title', false) === true) {
				$class .= ' no-title';
			}

			if(noo_get_option( 'noo_portfolio_grid_style', 'standard' ) == 'masonry') {
				$class .= ' no-gap';
			}
		// }
		
		echo esc_attr( $class );
	}
endif;

if (!function_exists('noo_post_class')):
	function noo_post_class($output) {
		if (noo_has_featured_content()) {
			$output[] = 'has-featured';
		} else {
			$output[] = 'no-featured';
		}

		if(!is_single()) {

		}

		$post_id = get_the_id();
		$post_type = get_post_type($post_id);

		$post_format = noo_get_post_format($post_id, $post_type);

		// Post format class for NOO Portfolio
		if ($post_type == 'portfolio_project') {
			if( is_single() ) {
				$output[] = 'single-noo-portfolio';
			}
			if(!empty($post_format)) {
				$output[] = "media-{$post_format}";
			}
		}

		// Masonry Style
		if(noo_is_masonry_style()) {
			$prefix = ($post_type == 'portfolio_project') ? '_noo_portfolio' : '_noo_wp_post';
			// if it's portfolio page, get the size from setting
			$masonry_size = noo_get_post_meta($post_id, "{$prefix}_masonry_{$post_format}_size", 'regular');
			$output[] = 'masonry-item ' . $masonry_size;

			if($post_type == 'portfolio_project') {
			    $categories = wp_get_object_terms( $post_id, 'portfolio_category' );
			    foreach ( $categories as $category ) {
			      $output[] = 'noo-portfolio-' . $category->slug;
			    }
			}
		}
		
		return $output;
	}
	
	add_filter('post_class', 'noo_post_class');
endif;
