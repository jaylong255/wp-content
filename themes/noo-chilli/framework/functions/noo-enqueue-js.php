<?php
/**
 * NOO Framework Site Package.
 *
 * Register Script
 * This file register & enqueue scripts used in NOO Themes.
 *
 * @package    NOO Framework
 * @version    1.0.0
 * @author     Kan Nguyen <khanhnq@nootheme.com>
 * @copyright  Copyright (c) 2014, NooTheme
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       http://nootheme.com
 */
// =============================================================================

//
// Site scripts
//

if ( ! function_exists( 'noo_enqueue_site_scripts' ) ) :
	function noo_enqueue_site_scripts() {
        if( !is_admin() ){
		// Main script

        wp_register_script( 'vendor-modernizr', NOO_FRAMEWORK_URI . '/vendor/modernizr-2.7.1.min.js', null, null, false );
        wp_register_script( 'vendor-touchSwipe', NOO_FRAMEWORK_URI . '/vendor/jquery.touchSwipe.js', array( 'jquery' ), null, true );
        wp_register_script( 'vendor-bootstrap', NOO_FRAMEWORK_URI . '/vendor/bootstrap.min.js', array( 'vendor-touchSwipe' ), null, true );


        wp_register_script( 'vendor-hoverIntent', NOO_FRAMEWORK_URI . '/vendor/hoverIntent-r7.min.js', array( 'jquery' ), null, true );
        wp_register_script( 'vendor-superfish', NOO_FRAMEWORK_URI . '/vendor/superfish-1.7.4.min.js', array( 'jquery', 'vendor-hoverIntent' ), null, true );

        wp_register_script( 'owl.carousel', NOO_ASSETS_URI . '/js/owl.carousel.js', array( 'jquery' ), null, true );

        wp_register_script( 'jquery-ui-custom', NOO_ASSETS_URI . '/js/jquery-ui.js', array(), null, false );

        wp_register_script( 'jquery.ui.timepicker', NOO_ASSETS_URI . '/js/jquery.ui.timepicker.js', array(), null, true );
        wp_localize_script( 'jquery.ui.timepicker', 'datetime', array( 'lang' => substr(get_bloginfo ( 'language' ), 0, 2), 'rtl' => is_rtl() ) );

        wp_register_script( 'vendor-nav', NOO_ASSETS_URI . '/js/jquery.nav.js', null, null, true );

        wp_register_script( 'vendor-imagesloaded', NOO_FRAMEWORK_URI . '/vendor/imagesloaded.pkgd.min.js', null, null, true );
        wp_register_script( 'infinitescroll', NOO_ASSETS_URI . '/js/jquery.infinitescroll.min.js', array( 'jquery' ), null, true );
        wp_register_script( 'isotope', NOO_ASSETS_URI . '/js/jquery.isotope.min.js', array( 'jquery' ), null, true );

        wp_register_script( 'portfolio', NOO_ASSETS_URI . '/js/portfolio.js', array( 'jquery' ), null, true );
        wp_register_script( 'gallery-grid', NOO_ASSETS_URI . '/js/gallery-grid.js', array( 'jquery' ), null, true );
        wp_register_script( 'news-grid', NOO_ASSETS_URI . '/js/news-grid.js', array( 'jquery' ), null, true );

        wp_register_script( 'vendor-nivo-lightbox-js', NOO_FRAMEWORK_URI . '/vendor/nivo-lightbox/nivo-lightbox.min.js', array( 'jquery' ), null, true );

        wp_register_script( 'parallax', NOO_ASSETS_URI . '/js/jquery.parallax-1.1.3.js', array( 'jquery' ), null, true );


        wp_register_script( 'vendor-easing', NOO_FRAMEWORK_URI . '/vendor/easing-1.3.0.min.js', array( 'jquery' ), null, true );
        wp_register_script( 'vendor-appear', NOO_FRAMEWORK_URI . '/vendor/jquery.appear.js', array( 'jquery','vendor-easing' ), null, true );
        wp_register_script( 'vendor-countTo', NOO_FRAMEWORK_URI . '/vendor/jquery.countTo.js', array( 'jquery', 'vendor-appear' ), null, true );

        wp_enqueue_script( 'vendor-nav');
  
        if(is_single()){
            wp_enqueue_style( 'owl.carousel' );
            wp_enqueue_style( 'owl.theme' );
            wp_enqueue_script( 'owl.carousel' );
        }

        if( is_singular('portfolio_project') ){
            wp_enqueue_script( 'vendor-nivo-lightbox-js' );
        }
        wp_register_script( 'woo-script', NOO_ASSETS_URI . '/js/woo.js', array(), null, true );
        wp_enqueue_script('woo-script');

        wp_enqueue_script('woo-script');
        $nooWoo = array(
            'ajax_url'        => admin_url( 'admin-ajax.php', 'relative' ),
        );
        wp_localize_script('woo-script', 'nooWoo', $nooWoo);

        wp_register_script( 'noo-script', NOO_ASSETS_URI . '/js/noo.js', array( 'jquery','vendor-bootstrap', 'vendor-superfish' ), null, true );
        wp_enqueue_script('noo-script');
        $nooL10n = array(
            'ajax_url'        => admin_url( 'admin-ajax.php', 'relative' ),
            'home_url'        => home_url( '/' ),
            'is_blog'         => is_category() ? 'true' : 'false',
            'is_archive'      => is_post_type_archive('post') ? 'true' : 'false',
            'is_single'       => is_single() ? 'true' : 'false',
            'is_single_p'     => is_singular('portfolio_project') ? 'true' : 'false',
            'is_portfolio'    => is_post_type_archive( 'portfolio_project' ) || is_tax( 'portfolio_category') ? 'true' : 'false',
            'is_project'      => is_singular( 'portfolio_project' ) ? 'true' : 'false',
            'is_shop'         => NOO_WOOCOMMERCE_EXIST && is_shop() ? 'true' : 'false',
            'is_product'      => NOO_WOOCOMMERCE_EXIST && is_product() ? 'true' : 'false',
            'is_event'        => is_post_type_archive( 'tribe_events' ) ? 'true' : 'false',
        );


wp_enqueue_script( 'noo-cabas', NOO_ASSETS_URI . '/js/off-cavnass.js', array(), null, true );
        wp_localize_script('noo-script', 'nooL10n', $nooL10n);

            if ( is_singular() ) wp_enqueue_script( 'comment-reply' );
        }

	}
add_action( 'wp_enqueue_scripts', 'noo_enqueue_site_scripts' );
endif;
