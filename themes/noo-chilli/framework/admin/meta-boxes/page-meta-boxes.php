<?php
/**
 * NOO Meta Boxes Package
 *
 * Setup NOO Meta Boxes for Page
 * This file add Meta Boxes to WP Page edit page.
 *
 * @package    NOO Framework
 * @subpackage NOO Meta Boxes
 * @version    1.0.0
 * @author     Kan Nguyen <khanhnq@nootheme.com>
 * @copyright  Copyright (c) 2014, NooTheme
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       http://nootheme.com
 */

if (!function_exists('noo_page_meta_boxes')):
	function noo_page_meta_boxes() {
		// Declare helper object
		$prefix = '_noo_wp_page';
		$helper = new NOO_Meta_Boxes_Helper($prefix, array(
			'page' => 'page'
		));

		// Page Settings
		$meta_box = array(
			'id' => "{$prefix}_meta_box_page",
			'title' => esc_html__('Page Settings', 'noo-chilli' ) ,
			'description' => esc_html__('Choose various setting for your Page.', 'noo-chilli' ) ,
			'fields' => array(
				array(
					'label' => esc_html__('Custom Page Title', 'noo-chilli' ) ,
					'id' => "{$prefix}_custom_page_title",
					'desc'  => esc_html__( 'Leave empty to use Page Title.', 'noo-chilli' ),
					'type' => 'text',
				),
				array(
					'label' => esc_html__('Hide Page Title', 'noo-chilli' ) ,
					'id' => "{$prefix}_hide_page_title",
					'type' => 'checkbox',
				),
				array(
					'type' => 'divider'
				)
			)
		);

		if( noo_get_option('noo_page_heading', true) ) {
			$meta_box['fields'][] = array(
								'id'    => "{$prefix}_heading_image",
								'label' => esc_html__( 'Heading Background Image', 'noo-chilli' ),
								'desc'  => esc_html__( 'An unique heading image for this page', 'noo-chilli' ),
								'type'  => 'image',
							);
		}

		$helper->add_meta_box($meta_box);



		// Page Sidebar
		$meta_box = array(
			'id' => "{$prefix}_meta_box_sidebar",
			'title' => esc_html__('Sidebar', 'noo-chilli' ),
			'context'      => 'side',
			'priority'     => 'default',
			'fields' => array(
				array(
					'label' => esc_html__('Page Sidebar', 'noo-chilli' ) ,
					'id' => "{$prefix}_sidebar",
					'type' => 'sidebars',
					'std' => 'sidebar-main'
				) ,
			)
		);

		$helper->add_meta_box( $meta_box );
	}
endif;

add_action('add_meta_boxes', 'noo_page_meta_boxes');