<?php
if( !function_exists('noo_team_meta_boxs') ):
    function noo_team_meta_boxs(){
        // Declare helper object
        $prefix = '_noo_wp_team';
        $helper = new NOO_Meta_Boxes_Helper($prefix, array(
            'page' => 'team_member'
        ));
        // Post type: Gallery
        $meta_box = array(
            'id' => "{$prefix}_meta_box_team",
            'title' => esc_html__('Team Member Information:', 'noo-chilli' ),
            'fields' => array(
                array(
                    'id' => "{$prefix}_image",
                    'label' => esc_html__( 'Image', 'noo-chilli' ),
                    'type' => 'image',
                ),
                array(
                    'id' => "{$prefix}_name",
                    'label' => esc_html__( 'Name', 'noo-chilli' ),
                    'type' => 'text',
                ),
                array(
                    'id' => "{$prefix}_position",
                    'label' => esc_html__( 'Position', 'noo-chilli' ),
                    'type' => 'text',
                ),

            )
        );
        $helper->add_meta_box($meta_box);


        $meta_box = array(
            'id' => "{$prefix}_meta_box_team_social",
            'title' => esc_html__('Media Data: Social', 'noo-chilli' ),
            'fields' => array(
                array(
                    'id' => "{$prefix}_facebook",
                    'label' => esc_html__( 'Facebook', 'noo-chilli' ),
                    'type' => 'text',
                ),
                array(
                    'id' => "{$prefix}_twitter",
                    'label' => esc_html__( 'Twitter', 'noo-chilli' ),
                    'type' => 'text',
                ),
                array(
                    'id' => "{$prefix}_google",
                    'label' => esc_html__( 'Google +', 'noo-chilli' ),
                    'type' => 'text',
                ),
                array(
                    'id' => "{$prefix}_linkedin",
                    'label' => esc_html__( 'Linkedin', 'noo-chilli' ),
                    'type' => 'text',
                ),
                array(
                    'id' => "{$prefix}_flickr",
                    'label' => esc_html__( 'Flickr', 'noo-chilli' ),
                    'type' => 'text',
                ),
                array(
                    'id' => "{$prefix}_pinterest",
                    'label' => esc_html__( 'Pinterest', 'noo-chilli' ),
                    'type' => 'text',
                ),
                array(
                    'id' => "{$prefix}_instagram",
                    'label' => esc_html__( 'Google', 'noo-chilli' ),
                    'type' => 'text',
                ),
                array(
                    'id' => "{$prefix}_tumblr",
                    'label' => esc_html__( 'Tumblr', 'noo-chilli' ),
                    'type' => 'text',
                )
            )
        );

        $helper->add_meta_box($meta_box);
    }
    add_action('add_meta_boxes', 'noo_team_meta_boxs');
endif;


?>
