<?php
/**
 * NOO Meta Boxes Package
 *
 * Setup NOO Meta Boxes for Post
 * This file add Meta Boxes to WP Post edit page.
 *
 * @package    NOO Framework
 * @subpackage NOO Meta Boxes
 * @version    1.0.0
 * @author     Kan Nguyen <khanhnq@nootheme.com>
 * @copyright  Copyright (c) 2014, NooTheme
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       http://nootheme.com
 */
if (!function_exists('noo_post_meta_boxes')):
	function noo_post_meta_boxes() {
		// Declare helper object
		$prefix = '_noo_wp_post';
		$helper = new NOO_Meta_Boxes_Helper($prefix, array(
			'page' => 'post'
		));

		// Post type: Gallery
		$meta_box = array(
			'id' => "{$prefix}_meta_box_gallery",
			'title' => esc_html__('Gallery Settings', 'noo-chilli' ),
			'fields' => array(
				array(
					'id' => "{$prefix}_gallery",
					// 'label' => esc_html__( 'Your Gallery', 'noo-chilli' ),
					'type' => 'gallery',
				)
			)
		);

		$helper->add_meta_box($meta_box);


		


		// Page Settings: Single Post
		$meta_box = array(
			'id' => "{$prefix}_meta_box_single_page",
			'title' => esc_html__('Page Settings: Single Post', 'noo-chilli' ),
			'description' => esc_html__('Choose various setting for your Single Post page.', 'noo-chilli' ),
			'fields' => array(
				array(
					'type' => 'divider'
				),
				array(
					'label' => esc_html__('Page Layout', 'noo-chilli' ),
					'id' => "{$prefix}_global_setting",
					'type' => 'page_layout',
				),
                array(
                    'id'    => "{$prefix}_menu_transparent",
                    'label' => esc_html__( 'Enable Menu Transparent' , 'noo-chilli' ),
                    'desc'  => esc_html__( 'Enable Menu Transparent.', 'noo-chilli' ),
                    'type'  => 'checkbox',
                    'std'   => 'off',
                    'child-fields' => array(
                        'on'   => "{$prefix}_menu_transparent_logo"
                    )
                ),
				array(
					'label' => esc_html__('Override Global Settings?', 'noo-chilli' ),
					'id' => "{$prefix}_override_layout",
					'type' => 'checkbox',
					'child-fields' => array(
						'on' => "{$prefix}_layout,{$prefix}_sidebar"
					),
				),
				array(
					'label' => esc_html__('Page Layout', 'noo-chilli' ),
					'id' => "{$prefix}_layout",
					'type' => 'radio',
					'std' => 'sidebar',
					'options' => array(
						'fullwidth' => array(
							'label' => esc_html__('Full-Width', 'noo-chilli' ),
							'value' => 'fullwidth',
						),
						'sidebar' => array(
							'label' => esc_html__('With Right Sidebar', 'noo-chilli' ),
							'value' => 'sidebar',
						),
						'left_sidebar' => array(
							'label' => esc_html__('With Left Sidebar', 'noo-chilli' ),
							'value' => 'left_sidebar',
						),
					),
					// 'child-fields' => array(
					// 	'sidebar' => "{$prefix}_sidebar",
					// 	'left_sidebar' => "{$prefix}_sidebar",
					// ),
					
				),
				array(
					'label' => esc_html__('Post Sidebar', 'noo-chilli' ),
					'id' => "{$prefix}_sidebar",
					'type' => 'sidebars',
					'std' => 'sidebar-main'
				),


			)
		);

		if( noo_get_option('noo_page_heading', true) ) {
			$meta_box['fields'][] = array( 'type' => 'divider' );
			$meta_box['fields'][] = array(
								'id'    => '_heading_image',
								'label' => esc_html__( 'Heading Background Image', 'noo-chilli' ),
								'desc'  => esc_html__( 'An unique heading image for this post. If leave it blank, the default heading image of Blog ( in Customizer settings ) will be used.', 'noo-chilli' ),
								'type'  => 'image',
							);
		}

		$helper->add_meta_box( $meta_box );
	}
	
endif;

add_action('add_meta_boxes', 'noo_post_meta_boxes');
