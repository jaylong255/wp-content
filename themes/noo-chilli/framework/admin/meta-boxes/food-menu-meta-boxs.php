<?php
if( !function_exists('noo_food_meta_boxs') ):
    function noo_food_meta_boxs(){
        // Declare helper object
        $prefix = '_noo_wp_food';
        $helper = new NOO_Meta_Boxes_Helper($prefix, array(
            'page' => 'food_menu'
        ));
        // Post type: Gallery
        $meta_box = array(
            'id' => "{$prefix}_meta_box_food_menu",
            'title' => esc_html__('Menu Item options', 'noo-chilli' ),
            'fields' => array(
                array(
                    'id' => "{$prefix}_price",
                    'label' => esc_html__( 'Price', 'noo-chilli' ),
                    'type' => 'text',
                ),
                array(
                    'id' => "{$prefix}_attributes",
                    'label' => esc_html__( 'Attributes', 'noo-chilli' ),
                    'type' => 'text',
                ),
            )
        );

        $helper->add_meta_box($meta_box);
    }
    add_action('add_meta_boxes', 'noo_food_meta_boxs');
endif;


?>
