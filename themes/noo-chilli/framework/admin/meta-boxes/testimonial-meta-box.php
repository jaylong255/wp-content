<?php
    if( !function_exists('noo_testimonial_meta_boxs') ):
        function noo_testimonial_meta_boxs(){
            // Declare helper object
            $prefix = '_noo_wp_post';
            $helper = new NOO_Meta_Boxes_Helper($prefix, array(
                'page' => 'testimonial'
            ));
            // Post type: Gallery
            $meta_box = array(
                'id' => "{$prefix}_meta_box_testimonial",
                'title' => esc_html__('Testimonial options', 'noo-chilli' ),
                'fields' => array(
                    array(
                        'id' => "{$prefix}_image",
                        'label' => esc_html__( 'Image', 'noo-chilli' ),
                        'type' => 'image',
                    ),
                    array(
                        'id' => "{$prefix}_name",
                         'label' => esc_html__( 'Your Name', 'noo-chilli' ),
                        'type' => 'text',
                    ),
                    array(
                        'id' => "{$prefix}_position",
                         'label' => esc_html__( 'Your Position', 'noo-chilli' ),
                        'type' => 'text',
                    ),
                )
            );

            $helper->add_meta_box($meta_box);
        }
        add_action('add_meta_boxes', 'noo_testimonial_meta_boxs');
    endif;


?>
