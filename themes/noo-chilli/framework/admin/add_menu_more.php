<?php
if(class_exists('Noo_Chilli_Library')){
    class Noo_Add_Food_Menu{
        public function __construct(){
            add_action( 'admin_enqueue_scripts', array($this,'noo_enqueue'));
            add_action('admin_menu',array($this,'add_menu_page'));
            add_action('wp_ajax_noo_save_post', array($this,'noo_save_post'));

        }
        function noo_enqueue(){
            wp_enqueue_media();
        }
        public function add_menu_page(){
            add_submenu_page('edit.php?post_type=food_menu','Add Many Items','Add Many Items','manage_options','food-menu-item',array($this,'noo_menu_pages'));
            $submenu_item = array_pop( $GLOBALS['submenu']['edit.php?post_type=food_menu'] );
            $GLOBALS['submenu']['edit.php?post_type=food_menu'][11] = $submenu_item;
            ksort( $GLOBALS['submenu']['edit.php?post_type=food_menu'] );
        }
        public function noo_menu_pages(){
            ?>
            <div class="wrap noo-f-wrap">
                <div class="noo-ajax-bk"></div>
                <h2><?php echo esc_html_e('Add Food Menu', 'noo-chilli' ); ?></h2>
                <h3 class="noo-fsec"><?php echo esc_html_e('Choose Menu Section', 'noo-chilli' ); ?></h3>
                <?php wp_dropdown_categories( '&hide_empty=0&show_count=1&hierarchical=1&taxonomy=menu_sections&class=noo-f-cat'); ?>
                <table class="form-table">
                    <thead>
                    <tr>
                        <th class="noo-f-name"><?php echo esc_html_e('Name', 'noo-chilli' ); ?></th>
                        <th class="noo-f-price"><?php echo esc_html_e('Price', 'noo-chilli' ); ?></th>
                        <th class="noo-f-attributes"><?php echo esc_html_e('Attributes', 'noo-chilli' ); ?></th>
                        <th class="noo-f-image"><?php echo esc_html_e('Image', 'noo-chilli' ); ?></th>
                        <th class="noo-f-des"><?php echo esc_html_e('Description', 'noo-chilli' ); ?></th>
                        <th class="noo-f-remove"></th>
                    </tr>
                    </thead>
                    <tbody class="noo_tbody">
                    <tr class="menu-item">
                        <td class="noo-f-name">
                            <input type="text" name="name" value="" class="noo_food_name noo-data">
                        </td>
                        <td class="noo-f-price">
                            <input type="text" name="price" value="" class="noo_food_price noo-data">
                        </td>
                        <td class="noo-f-attributes">
                            <input type="text" name="attributes" value="" class="noo_food_attributes noo-data">
                        </td>
                        <td class="noo-f-image">
                            <img src="" class="noo_fimages hidde" alt="image">
                            <button class="uploadimage"><?php echo esc_html_e('Add Image', 'noo-chilli' ); ?></button>
                            <input type="hidden" name="image" value="" class="noo_food_image noo-data">
                        </td>
                        <td class="noo-f-des">
                            <textarea name="description" class="noo_food_description noo-data"></textarea>
                        </td>
                        <td class="noo-f-remove"><button class="remove_item">x</button></td>
                    </tr>

                    </tbody>
                </table>
                <p>
                    <button class="add_menu_save btn-success"><?php echo esc_html_e('Save Menu', 'noo-chilli' ); ?></button> <button class="add_menu"><?php echo esc_html_e('Add Menu Item', 'noo-chilli' ); ?></button>
                </p>

            </div>
            <script>
                function images_upload(){
                    var frame;
                    jQuery('.uploadimage').on('click',function(event){
                        var $upload = jQuery(this);
                        event.preventDefault();
                        // If the media frame already exists, reopen it.
                        if ( frame ) {
                            frame.open();
                            return;
                        }
                        // Create a new media frame
                        frame = wp.media.frames.file_frame = wp.media({
                            frame:    'post',
                            state:    'insert',
                            multiple: false
                        });
                        // When an image is selected in the media frame...
                        frame.on( 'insert', function() {
                            // Get media attachment details from the frame state
                            var attachment = frame.state().get('selection').first().toJSON();
                            $upload.prev().attr('src',attachment.url).removeClass('hidde');
                            $upload.next().val(attachment.id);
                        });

                        // Finally, open the modal on click
                        frame.open();
                    });
                }
                function remove_item(){
                    jQuery('.remove_item').click(function(){
                        jQuery(this).parent().parent().remove();
                    });
                }
                jQuery(document).ready(function(){
                    remove_item();
                    images_upload();
                    jQuery(document).on('add_menu_click',function(){
                        images_upload();
                        remove_item();
                    });
                    jQuery('.add_menu').click(function(){
                        var $item = jQuery('.menu-item:first').clone();
                        $item.find('.noo-data').val(' ');
                        $item.find('.remove_item').addClass('item-blog');
                        $item.find('.noo_fimages').attr('src','').addClass('hidde');
                        jQuery('.noo_tbody').append($item);
                        jQuery(this).trigger('add_menu_click');
                    }) ;
                    jQuery('.add_menu_save').on('click',function(){
                        var $menu =  jQuery(this);
                        if(jQuery('.noo_food_name').val() == ''){
                            jQuery('.noo_food_name').focus();
                            return false;
                        }
                        $menu.addClass('add_menu_save-eff');
                        jQuery('.noo-ajax-bk').css('display','block');
                        var $name = '';
                        var $price = '';
                        var $attributes = '';
                        var $description = '';
                        var $image = '';
                        var $cat = jQuery('.noo-f-cat').val();
                        jQuery('.menu-item').each(function(){
                            $name+= jQuery(this).find('.noo_food_name').val()+'/';
                            $price+= jQuery(this).find('.noo_food_price').val()+'/';
                            $attributes+= jQuery(this).find('.noo_food_attributes').val()+'/';
                            $description += jQuery(this).find('.noo_food_description').val()+'/';
                            $image+= jQuery(this).find('.noo_food_image').val()+'/';
                        });
                        jQuery.ajax({
                            url: '<?php echo admin_url('admin-ajax.php'); ?>',
                            type: 'post',
                            data:({
                                action: 'noo_save_post',
                                name: $name,
                                price: $price,
                                attributes: $attributes,
                                description: $description,
                                image: $image,
                                cat : $cat
                            }),
                            success: function(data){
                                if(data ==1){
                                    $menu.removeClass('add_menu_save-eff');
                                    jQuery('.noo-ajax-bk').css('display','none');
                                    jQuery(".menu-item:not(:eq(0))").remove();
                                    jQuery('.menu-item').find('.noo-data').val(' ');
                                    jQuery('.menu-item').find('.noo_fimages').attr('src','').addClass('hidde');
                                }
                            }
                        });
                    });
                });
            </script>
        <?php
        }

        public function noo_save_post(){
            $name        = $_POST['name'];
            $price       = $_POST['price'];
            $attributes  = $_POST['attributes'];
            $description = $_POST['description'];
            $image       = $_POST['image'];
            $cat         = $_POST['cat'];
            $arr_name    = explode('/',$name);
            array_pop($arr_name);

            $arr_price   = explode('/',$price);
            array_pop($arr_price);

            $arr_attr    = explode('/',$attributes);
            array_pop($arr_attr);

            $arr_description = explode('/',$description);
            array_pop($arr_description);

            $arr_img = explode('/',$image);
            array_pop($arr_img);

            $count = count($arr_name);
            for($i =0; $i< $count; $i++){
                if($arr_name[$i] != ''):
                    $post_detail = array(
                        'post_status' =>  'publish',
                        'post_type' =>  'food_menu',
                        'post_title'    =>  trim(esc_attr($arr_name[$i])),
                        'post_content'  => trim($arr_description[$i]),
                        'tax_input'     => array( 'menu_sections' => esc_attr($cat) )
                    );
                    $post_id = wp_insert_post( $post_detail );
                    if( !$post_id || is_wp_error( $post_id )){
                        continue;
                    }
                    if($arr_img[$i] != ''){
                        set_post_thumbnail($post_id,esc_attr($arr_img[$i]));
                    }
                    if($arr_price[$i] != ''){
                        add_post_meta( $post_id, '_noo_wp_food_price', esc_attr($arr_price[$i]) );
                    }
                    if($arr_attr[$i] != ''){
                        add_post_meta( $post_id, '_noo_wp_food_attributes', esc_attr($arr_attr[$i]) );
                    }
                endif;
            }
            echo 1;
            exit;
        }
    }
    new Noo_Add_Food_Menu();
}
?>