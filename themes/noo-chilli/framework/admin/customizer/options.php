<?php
/**
 * NOO Customizer Package.
 *
 * Register Options
 * This file register options used in NOO-Customizer
 *
 * @package    NOO Framework
 * @version    1.0.0
 * @author     Kan Nguyen <khanhnq@nootheme.com>
 * @copyright  Copyright (c) 2014, NooTheme
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       http://nootheme.com
 */
// =============================================================================


// 0. Remove Unused WP Customizer Sections
if ( ! function_exists( 'noo_customizer_remove_wp_native_sections' ) ) :
    function noo_customizer_remove_wp_native_sections( $wp_customize ) {
        // $wp_customize->remove_section( 'title_tagline' );
        // $wp_customize->remove_section( 'colors' );
        // $wp_customize->remove_section( 'background_image' );
        $wp_customize->remove_section( 'nav' );
        $wp_customize->remove_section( 'static_front_page' );
    }

    add_action( 'customize_register', 'noo_customizer_remove_wp_native_sections' );
endif;


//
// Register NOO Customizer Sections and Options
//

// 1. Site Enhancement options.
if ( ! function_exists( 'noo_customizer_register_options_general' ) ) :
    function noo_customizer_register_options_general( $wp_customize ) {

        // declare helper object.
        $helper = new NOO_Customizer_Helper( $wp_customize );

        // Section: Site Enhancement
        $helper->add_section(
            'noo_customizer_section_site_enhancement',
            esc_html__( 'Site Enhancement', 'noo-chilli' ),
            esc_html__( 'Enable/Disable some features for your site.', 'noo-chilli' )
        );

        // Control: Favicon
        if ( ! function_exists( 'has_site_icon' ) ) :
            $helper->add_control(
                'noo_custom_favicon',
                'noo_image',
                esc_html__( 'Custom Favicon', 'noo-chilli' ),
                '',
                array(),
                array( 'transport' => 'postMessage' )
            );
        endif;

        // Control: Back to Top
        $helper->add_control(
            'noo_back_to_top',
            'noo_switch',
            esc_html__( 'Back To Top Button', 'noo-chilli' ),
            1,
            array(),
            array( 'transport' => 'postMessage' )
        );
        $helper->add_control(
            'noo_page_heading',
            'noo_switch',
            esc_html__( 'Enable Page Heading', 'noo-chilli' ),
            1,
            array( 'json' => array(
                'on_child_options'  => 'noo_blog_heading_title,noo_blog_heading_image'
            )
            ),
            array( 'transport' => 'postMessage' )
        );



        // Control: Enable MailChimp Subscribe
         $helper->add_control(
         	'noo_mailchimp',
         	'noo_switch',
         	esc_html__( 'Enable MailChimp Subscribe', 'noo-chilli' ),
         	1,
         	array( 'json' => array( 'on_child_options' => 'noo_mailchimp_api_key' ) ),
         	array( 'transport' => 'postMessage' )
         );

  //       Control: MailChimp Settings
         $helper->add_control(
         	'noo_mailchimp_api_key',
         	'mailchimp',
         	esc_html__( 'MailChimp Settings', 'noo-chilli' ),
         	'',
         	array(),
         	array( 'transport' => 'postMessage' )
         );

    }
    add_action( 'customize_register', 'noo_customizer_register_options_general' );
endif;

// 2. Design and Layout options.
if ( ! function_exists( 'noo_customizer_register_options_layout' ) ) :
    function noo_customizer_register_options_layout( $wp_customize ) {

        // declare helper object.
        $helper = new NOO_Customizer_Helper( $wp_customize );

        // Section: Layout
        $helper->add_section(
            'noo_customizer_section_layout',
            esc_html__( 'Design and Layout', 'noo-chilli' ),
            esc_html__( 'Set Style and Layout for your site. Boxed Layout will come with additional setting options for background color and image.', 'noo-chilli' )
        );

        // Control: Site Layout
        $helper->add_control(
            'noo_site_layout',
            'noo_radio',
            esc_html__( 'Site Layout', 'noo-chilli' ),
            'fullwidth',
            array(
                'choices' => array( 'fullwidth' => esc_html__( 'Fullwidth', 'noo-chilli' ), 'boxed' => esc_html__( 'Boxed', 'noo-chilli' ) ),
                'json'  => array(
                    'child_options' => array(
                        'boxed' => 'noo_layout_site_width
									,noo_layout_site_max_width
									,noo_layout_bg_color
                                    ,noo_layout_bg_image_sub_section
                                    ,noo_layout_bg_image
                                    ,noo_layout_bg_repeat
                                    ,noo_layout_bg_align
                                    ,noo_layout_bg_attachment
                                    ,noo_layout_bg_cover'
                    )
                )
            ),
            array( 'transport' => 'postMessage' )
        );

        // Control: Site Width (%)
        $helper->add_control(
            'noo_layout_site_width',
            'ui_slider',
            esc_html__( 'Site Width (%)', 'noo-chilli' ),
            '90',
            array(
                'json' => array(
                    'data_min' => 60,
                    'data_max' => 100,
                )
            ),
            array( 'transport' => 'postMessage' )
        );

        // Control: Site Max Width (px)
        $helper->add_control(
            'noo_layout_site_max_width',
            'ui_slider',
            esc_html__( 'Site Max Width (px)', 'noo-chilli' ),
            '1200',
            array(
                'json' => array(
                    'data_min'  => 980,
                    'data_max'  => 1600,
                    'data_step' => 10,
                )
            ),
            array( 'transport' => 'postMessage' )
        );

        // Control: Background Color
        $helper->add_control(
            'noo_layout_bg_color',
            'color_control',
            esc_html__( 'Background Color', 'noo-chilli' ),
            '#ffffff',
            array(),
            array( 'transport' => 'postMessage' )
        );

        // Sub-section: Background Image
        $helper->add_sub_section(
            'noo_layout_bg_image_sub_section',
            esc_html__( 'Background Image', 'noo-chilli' ),
            wp_kses( __( 'Upload your background image here, you have various settings for your image:<br/><strong>Repeat Image</strong>: enable repeating your image, you will need it when using patterned background.<br/><strong>Alignment</strong>: Set the position to align your background image.<br/><strong>Attachment</strong>: Make your image scroll with your site or fixed.<br/><strong>Auto resize</strong>: Enable it to ensure your background image always fit the windows.', 'noo-chilli' ), noo_allowed_html() )
        );

        // Control: Background Image
        $helper->add_control(
            'noo_layout_bg_image',
            'noo_image',
            esc_html__( 'Background Image', 'noo-chilli' ),
            null,
            array(),
            array( 'transport' => 'postMessage' )
        );

        // Control: Repeat Image
        $helper->add_control(
            'noo_layout_bg_repeat',
            'radio',
            esc_html__( 'Background Repeat', 'noo-chilli' ),
            'no-repeat',
            array(
                'choices' => array(
                    'repeat' => esc_html__( 'Repeat', 'noo-chilli' ),
                    'no-repeat' => esc_html__( 'No Repeat', 'noo-chilli' ),
                )
            ),
            array( 'transport' => 'postMessage' )
        );

        // Control: Align Image
        $helper->add_control(
            'noo_layout_bg_align',
            'select',
            esc_html__( 'BG Image Alignment', 'noo-chilli' ),
            'left top',
            array(
                'choices' => array(
                    'left top'       => esc_html__( 'Left Top', 'noo-chilli' ),
                    'left center'     => esc_html__( 'Left Center', 'noo-chilli' ),
                    'left bottom'     => esc_html__( 'Left Bottom', 'noo-chilli' ),
                    'center top'     => esc_html__( 'Center Top', 'noo-chilli' ),
                    'center center'     => esc_html__( 'Center Center', 'noo-chilli' ),
                    'center bottom'     => esc_html__( 'Center Bottom', 'noo-chilli' ),
                    'right top'     => esc_html__( 'Right Top', 'noo-chilli' ),
                    'right center'     => esc_html__( 'Right Center', 'noo-chilli' ),
                    'right bottom'     => esc_html__( 'Right Bottom', 'noo-chilli' ),
                )
            ),
            array( 'transport' => 'postMessage' )
        );

        // Control: Enable Scrolling Image
        $helper->add_control(
            'noo_layout_bg_attachment',
            'radio',
            esc_html__( 'BG Image Attachment', 'noo-chilli' ),
            'fixed',
            array(
                'choices' => array(
                    'fixed' => esc_html__( 'Fixed Image', 'noo-chilli' ),
                    'scroll' => esc_html__( 'Scroll with Site', 'noo-chilli' ),
                )
            ),
            array( 'transport' => 'postMessage' )
        );

        // Control: Auto Resize
        $helper->add_control(
            'noo_layout_bg_cover',
            'noo_switch',
            esc_html__( 'Auto Resize', 'noo-chilli' ),
            0,
            array(),
            array( 'transport' => 'postMessage' )
        );

        // Control: Theme RTL
        $helper->add_control(
            'noo_layout_rtl',
            'noo_radio',
            esc_html__( 'Theme RTL', 'noo-chilli' ),
            'no',
            array(
                'choices' => array( 'no' => esc_html__( 'No', 'noo-chilli' ), 'yes' => esc_html__( 'Yes', 'noo-chilli' ) ),
            ),
            array( 'transport' => 'postMessage' )
        );

        // Sub-Section: Icon Theme
        $helper->add_sub_section(
            'noo_general_sub_section_icon_theme',
            esc_html__( 'Icon Theme', 'noo-chilli' ),
            esc_html__( 'Here you can show / hide or change Icon Theme', 'noo-chilli' )
        );

        // Hide Icon Theme
        $helper->add_control(
            'noo_site_hide_icon',
            'noo_switch',
            esc_html__( 'Hide Icon Theme', 'noo-chilli' ),
            0,
            array(),
            array( 'transport' => 'postMessage' )
        );

        // Custom Icon Theme
        $helper->add_control(
            'noo_site_custom_icon',
            'noo_image',
            esc_html__( 'Custom Icon Theme', 'noo-chilli' ),
            '',
            array(),
            array( 'transport' => 'postMessage' )
        );

        // Sub-Section: Links Color
        $helper->add_sub_section(
            'noo_general_sub_section_links_color',
            esc_html__( 'Color', 'noo-chilli' ),
            esc_html__( 'Here you can set the color for links and various elements on your site.', 'noo-chilli' )
        );

        // Control: Site Links Hover Color
        $helper->add_control(
            'noo_site_link_hover_color',
            'color_control',
            esc_html__( 'Primary Color', 'noo-chilli' ),
            noo_default_primary_color(),
            array(),
            array( 'transport' => 'postMessage' )
        );
    }
    add_action( 'customize_register', 'noo_customizer_register_options_layout' );
endif;

// 3. Typography options.
if ( ! function_exists( 'noo_customizer_register_options_typo' ) ) :
    function noo_customizer_register_options_typo( $wp_customize ) {

        // declare helper object.
        $helper = new NOO_Customizer_Helper( $wp_customize );

        // Section: Typography
        $helper->add_section(
            'noo_customizer_section_typo',
            esc_html__( 'Typography', 'noo-chilli' ),
            wp_kses( __( 'Customize your Typography settings. Chilli integrated all Google Fonts. See font preview at <a target="_blank" href="http://www.google.com/fonts/">Google Fonts</a>.', 'noo-chilli' ), noo_allowed_html() )
        );

        // Control: Use Custom Fonts
        $helper->add_control(
            'noo_typo_use_custom_fonts',
            'noo_switch',
            esc_html__( 'Use Custom Fonts?', 'noo-chilli' ),
            0,
            array( 'json' => array(
                'on_child_options'  => 'noo_typo_headings_font,noo_typo_body_font'
            )
            ),
            array( 'transport' => 'postMessage' )
        );

        // Control: Use Custom Font Color
        $helper->add_control(
            'noo_typo_use_custom_fonts_color',
            'noo_switch',
            esc_html__( 'Custom Font Color?', 'noo-chilli' ),
            0,
            array( 'json' => array(
                'on_child_options'  => 'noo_typo_headings_font_color,noo_typo_body_font_color'
            )
            ),
            array( 'transport' => 'postMessage' )
        );

        // Sub-Section: Headings
        $helper->add_sub_section(
            'noo_typo_sub_section_headings',
            esc_html__( 'Headings', 'noo-chilli' )
        );

        // Control: Headings font
        $helper->add_control(
            'noo_typo_headings_font',
            'google_fonts',
            esc_html__( 'Headings Font', 'noo-chilli' ),
            noo_default_headings_font_family(),
            array(
                'weight' => '700',
                'style'	=> 'italic'
            ),
            array( 'transport' => 'postMessage' )
        );

        // Control: Headings Font Color
        $helper->add_control(
            'noo_typo_headings_font_color',
            'color_control',
            esc_html__( 'Font Color', 'noo-chilli' ),
            noo_default_headings_color(),
            array(),
            array( 'transport' => 'postMessage' )
        );

        // Control: Headings Font Uppercase
        $helper->add_control(
            'noo_typo_headings_uppercase',
            'checkbox',
            esc_html__( 'Transform to Uppercase', 'noo-chilli' ),
            0,
            array(),
            array( 'transport' => 'postMessage' )
        );

        // Sub-Section: Body
        $helper->add_sub_section(
            'noo_typo_sub_section_body',
            esc_html__( 'Body', 'noo-chilli' )
        );

        // Control: Body font
        $helper->add_control(
            'noo_typo_body_font',
            'google_fonts',
            esc_html__( 'Body Font', 'noo-chilli' ),
            noo_default_font_family(),
            array(),
            array( 'transport' => 'postMessage' )
        );

        // Control: Body Font Size
        $helper->add_control(
            'noo_typo_body_font_size',
            'font_size',
            esc_html__( 'Font Size (px)', 'noo-chilli' ),
            noo_default_font_size(),
            array(),
            array( 'transport' => 'postMessage' )
        );

        // Control: Body Font Color
        // $helper->add_control(
        //     'noo_typo_body_font_color',
        //     'color_control',
        //     esc_html__( 'Font Color', 'noo-chilli' ),
        //     noo_default_text_color(),
        //     array(),
        //     array( 'transport' => 'postMessage' )
        // );
    }
    add_action( 'customize_register', 'noo_customizer_register_options_typo' );
endif;


// 6. Header options.
if ( ! function_exists( 'noo_customizer_register_options_header' ) ) :
    function noo_customizer_register_options_header( $wp_customize ) {

        // declare helper object.
        $helper = new NOO_Customizer_Helper( $wp_customize );

        // Section: Header
        $helper->add_section(
            'noo_customizer_section_header',
            esc_html__( 'Header', 'noo-chilli' ),
            esc_html__( 'Customize settings for your Header, including Navigation Bar (Logo and Navigation) and an optional Top Bar.', 'noo-chilli' ),
            true
        );

        // Sub-section: General Options
        $helper->add_sub_section(
            'noo_header_sub_section_general',
            esc_html__( 'General Options', 'noo-chilli' ),
            ''
        );

        // Sub-Section: Logo
        $helper->add_sub_section(
            'noo_header_sub_section_logo',
            esc_html__( 'Logo', 'noo-chilli' ),
            esc_html__( 'All the settings for Logo go here. If you do not use Image for Logo, plain text will be used.', 'noo-chilli' )
        );

        // Control: Use Image for Logo
        $helper->add_control(
            'noo_header_use_image_logo',
            'noo_switch',
            esc_html__( 'Use Image for Logo?', 'noo-chilli' ),
            0,
            array(
                'json' => array(
                    'on_child_options'   => 'noo_header_logo_image
                                        ,noo_header_logo_retina_image
                                        ,noo_header_logo_image_height',
                    'off_child_options'  => 'blogname
                                        ,noo_header_logo_font
                                        ,noo_header_logo_font_size
                                        ,noo_header_logo_font_color
                                        ,noo_header_logo_uppercase'
                )
            ),
            array( 'transport' => 'postMessage' )
        );

        // Control: Blog Name
        $helper->add_control(
            'blogname',
            'text',
            esc_html__( 'Blog Name', 'noo-chilli' ),
            get_bloginfo( 'name' ),
            array(),
            array( 'transport' => 'postMessage' )
        );

        // Control: Logo font
        $helper->add_control(
            'noo_header_logo_font',
            'google_fonts',
            esc_html__( 'Logo Font', 'noo-chilli' ),
            noo_default_logo_font_family(),
            array(
                'weight' => '700',
                'style' => 'italic'
            ),
            array( 'transport' => 'postMessage' )
        );

        // Control: Logo Font Size
        $helper->add_control(
            'noo_header_logo_font_size',
            'ui_slider',
            esc_html__( 'Font Size (px)', 'noo-chilli' ),
            '30',
            array(
                'json' => array(
                    'data_min' => 15,
                    'data_max' => 80,
                )
            ),
            array( 'transport' => 'postMessage' )
        );

        // Control: Logo Font Color
        $helper->add_control(
            'noo_header_logo_font_color',
            'color_control',
            esc_html__( 'Font Color', 'noo-chilli' ),
            noo_default_logo_color(),
            array(),
            array( 'transport' => 'postMessage' )
        );

        // Control: Logo Font Uppercase
        $helper->add_control(
            'noo_header_logo_uppercase',
            'checkbox',
            esc_html__( 'Transform to Uppercase', 'noo-chilli' ),
            0,
            array(),
            array( 'transport' => 'postMessage' )
        );

        // Control: Logo Image
        $helper->add_control(
            'noo_header_logo_image',
            'noo_image',
            esc_html__( 'Upload Your Logo', 'noo-chilli' ),
            '',
            array(),
            array( 'transport' => 'postMessage' )
        );


        // Control: Logo Image Height
        $helper->add_control(
            'noo_header_logo_image_height',
            'ui_slider',
            esc_html__( 'Image Height (px)', 'noo-chilli' ),
            '90',
            array(
                'json' => array(
                    'data_min' => 30,
                    'data_max' => 90,
                )
            ),
            array( 'transport' => 'postMessage' )
        );

        // Sub-Section: Navigation Bar
        $helper->add_sub_section(
            'noo_header_sub_section_nav',
            esc_html__( 'Navigation Bar', 'noo-chilli' ),
            esc_html__( 'Adjust settings for Navigation Bar. You also can customize some settings for the Toggle Button on Mobile in this section.', 'noo-chilli' )
        );

        // Control: NavBar Style
        $helper->add_control(
            'noo_header_nav_style',
            'noo_radio',
            esc_html__( 'NavBar Style', 'noo-chilli' ),
            'relative',
            array(
                'choices' => array(
                    'relative' => esc_html__( 'Relative', 'noo-chilli' ),
                    'absolute' => esc_html__( 'Absolute', 'noo-chilli' ),
                ),
                'json' => array(
                    'child_options' => array(
                        'relative'   => 'noo_header_nav_bg_color',
                    )
                )
            ),
            array( 'transport' => 'postMessage' )
        );

        // Control: Background Color
        $helper->add_control(
            'noo_header_nav_bg_color',
            'color_control',
            esc_html__( 'Navbar Background Color', 'noo-chilli' ),
            '#000000',
            array(),
            array( 'transport' => 'postMessage' )
        );

        // Control: NavBar Position
        $helper->add_control(
            'noo_header_nav_position',
            'noo_radio',
            esc_html__( 'NavBar Position', 'noo-chilli' ),
            'fixed_top',
            array(
                'choices' => array(
                    'static_top'       => esc_html__( 'Static Top', 'noo-chilli' ),
                    'fixed_top'     => esc_html__( 'Fixed Top', 'noo-chilli' ),
                ),
                // 'json' => array(
                //     'child_options' => array(
                //         'fixed_top'   => 'noo_header_nav_shrinkable,noo_header_nav_smart_scroll,noo_header_sub_section_nav_floating,noo_header_nav_floating',
                //     )
                // )
            ),
            array( 'transport' => 'postMessage' )
        );
        
        // Control: Divider 2
        $helper->add_control( 'noo_header_nav_divider_2', 'divider', '' );

        // Control: Custom NavBar Font
        $helper->add_control(
            'noo_header_custom_nav_font',
            'noo_switch',
            esc_html__( 'Use Custom NavBar Font and Color?', 'noo-chilli' ),
            0,
            array( 'json' => array(
                'on_child_options'  => 'noo_header_nav_font,noo_header_nav_link_color,noo_header_nav_link_hover_color'
            )
            ),
            array( 'transport' => 'postMessage' )
        );

        // Control: NavBar font
        $helper->add_control(
            'noo_header_nav_font',
            'google_fonts',
            esc_html__( 'NavBar Font', 'noo-chilli' ),
            noo_default_headings_font_family(),
            array(
                'weight' => '700',
                'style'	=> 'italic'
            ),
            array( 'transport' => 'postMessage' )
        );

        // Control: NavBar Font Size
        $helper->add_control(
            'noo_header_nav_font_size',
            'ui_slider',
            esc_html__( 'Font Size (px)', 'noo-chilli' ),
            '14',
            array(
                'json' => array(
                    'data_min' => 9,
                    'data_max' => 30,
                )
            ),
            array( 'transport' => 'postMessage' )
        );

        // Control: NavBar Link Color
        $helper->add_control(
            'noo_header_nav_link_color',
            'color_control',
            esc_html__( 'Link Color', 'noo-chilli' ),
            '',
            array(),
            array( 'transport' => 'postMessage' )
        );


        // Control: NavBar Link Hover Color
        $helper->add_control(
            'noo_header_nav_link_hover_color',
            'color_control',
            esc_html__( 'Link Hover Color', 'noo-chilli' ),
            '',
            array(),
            array( 'transport' => 'postMessage' )
        );


        // Control: NavBar Font Uppercase
        $helper->add_control(
            'noo_header_nav_uppercase',
            'checkbox',
            esc_html__( 'Transform to Uppercase', 'noo-chilli' ),
            1,
            array(),
            array( 'transport' => 'postMessage' )
        );

        // Control: NavBar Height (px)
        $helper->add_control(
            'noo_header_nav_height',
            'ui_slider',
            esc_html__( 'NavBar Height (px)', 'noo-chilli' ),
            '85',
            array(
                'json' => array(
                    'data_min' => 85,
                    'data_max' => 300,
                )
            ),
            array( 'transport' => 'postMessage' )
        );

        // Control: NavBar Link Spacing (px)
        $helper->add_control(
            'noo_header_nav_link_spacing',
            'ui_slider',
            esc_html__( 'NavBar Link Spacing (px)', 'noo-chilli' ),
            '22',
            array(
                'json' => array(
                    'data_min' => 10,
                    'data_max' => 50,
                )
            ),
            array( 'transport' => 'postMessage' )
        );

        // Sub-Section: Floating NavBar
        $helper->add_sub_section(
            'noo_header_sub_section_nav_floating',
            esc_html__( 'Floating Navigation Bar', 'noo-chilli' ),
            esc_html__( 'Enable/disable Floating Header that will lay above Home Slider and Headline Image with same background and float on top of your site when scrolling down.', 'noo-chilli' )
        );

        // Sub-Section: Header Mini Cart
        $helper->add_sub_section(
            'noo_header_sub_section_header_option',
            esc_html__( 'Header Mini Cart', 'noo-chilli' ),
            esc_html__( 'Adjust settings for Header. You also can customize some settings for mini cart.', 'noo-chilli' )
        );

        if( defined('WOOCOMMERCE_VERSION') ) {

            // Control: Header Cart
            $helper->add_control(
                'noo_header_minicart',
                'noo_switch',
                esc_html__( 'Enable Header Minicart', 'noo-chilli' ),
                1,
                array(),
                array( 'transport' => 'postMessage' )
            );
        }

        // Sub-Section: Top Bar
        $helper->add_sub_section(
            'noo_header_sub_section_top_bar',
            esc_html__( 'Top Bar', 'noo-chilli' ),
            esc_html__( 'Top Bar lays on top of your site, above Navigation Bar. It is suitable for placing contact information and social media link. Enable to control its layout and content.', 'noo-chilli' )
        );

        // Control: Header TopBar
        $helper->add_control(
            'noo_header_top_bar',
            'noo_switch',
            esc_html__( 'Enable Top Bar', 'noo-chilli' ),
            1,
            array(
                'json' => array(
                    'on_child_options'  => 'noo_top_bar_welcome_text,
                    noo_top_bar_hotline,
                    noo_top_bar_email,
                    noo_top_bar_show_search'
                )
            ),
            array( 'transport' => 'postMessage' )
        );

        // Control: Top Bar Welcome Text
        $helper->add_control(
            'noo_top_bar_welcome_text',
            'text',
            esc_html__( 'Welcome Text', 'noo-chilli' ),
            '',
            array(),
            array( 'transport' => 'postMessage' )
        );

        // Control: Top Bar Hotline
        $helper->add_control(
            'noo_top_bar_hotline',
            'text',
            esc_html__( 'Hotline', 'noo-chilli' ),
            '',
            array(),
            array( 'transport' => 'postMessage' )
        );

        // Control: Top Bar Email
        $helper->add_control(
            'noo_top_bar_email',
            'text',
            esc_html__( 'Email Address', 'noo-chilli' ),
            '',
            array(),
            array( 'transport' => 'postMessage' )
        );

        // Control: Show Search
        $helper->add_control(
            'noo_top_bar_show_search',
            'checkbox',
            esc_html__( 'Show Search Box', 'noo-chilli' ),
            1,
            array(),
            array( 'transport' => 'postMessage' )
        );

    }
    add_action( 'customize_register', 'noo_customizer_register_options_header' );
endif;

// 7. Footer options.
if ( ! function_exists( 'noo_customizer_register_options_footer' ) ) :
    function noo_customizer_register_options_footer( $wp_customize ) {

        // declare helper object.
        $helper = new NOO_Customizer_Helper( $wp_customize );

        // Section: Footer
        $helper->add_section(
            'noo_customizer_section_footer',
            esc_html__( 'Footer', 'noo-chilli' ),
            esc_html__( 'Footer contains Widgetized area and Footer Bottom. You can change any parts.', 'noo-chilli' )
        );

        // Control: Footer Columns (Widgetized)
        // $helper->add_control(
        // 	'noo_footer_top',
        // 	'noo_switch',
        // 	esc_html__( 'Enable Footer Top', 'noo-chilli' ),
        // 	1,
        // 	array(
        // 		'json' => array(
        // 			'on_child_options'  => 'noo_footer_top_logo,nav_menu_locations[footer-menu],noo_footer_top_social'
        // 		)
        // 	),
        // 	array( 'transport' => 'postMessage' )
        // );

        // Control: Footer Logo
         $helper->add_control(
         	'noo_footer_top_logo',
         	'noo_image',
         	esc_html__( 'Upload Footer Background', 'noo-chilli' ),
         	'',
         	array(),
         	array( 'transport' => 'postMessage' )
         );

        $menus        = wp_get_nav_menus();
        $menu_choices = array( 0 => esc_html__( '&mdash; Select &mdash;', 'noo-chilli' ) );
        foreach ( $menus as $menu ) {
            $menu_choices[ $menu->term_id ] = wp_html_excerpt( $menu->name, 40, '&hellip;' );
        }


        // Control: Divider 1
        $helper->add_control( 'noo_footer_divider_1', 'divider', '' );

        // Control: Footer Columns (Widgetized)
        $helper->add_control(
            'noo_footer_widgets',
            'select',
            esc_html__( 'Footer Columns (Widgetized)', 'noo-chilli' ),
            '3',
            array(
                'choices' => array(
                    0       => esc_html__( 'None (No Footer Main Content)', 'noo-chilli' ),
                    1     => esc_html__( 'One', 'noo-chilli' ),
                    2     => esc_html__( 'Two', 'noo-chilli' ),
                    3     => esc_html__( 'Three', 'noo-chilli' ),
                    4     => esc_html__( 'Four', 'noo-chilli' )
                )
            ),
            array( 'transport' => 'postMessage' )
        );
          // Control: Footer Columns (Widgetized)
        $helper->add_control(
            'noo_footer_bottom',
            'select',
            esc_html__( 'Show Footer bottom', 'noo-chilli' ),
            '1',
            array(
                'choices' => array(
                    1       => esc_html__( 'Yes', 'noo-chilli' ),
                    0     => esc_html__( 'No', 'noo-chilli' ),
                )
            ),
            array( 'transport' => 'postMessage' )
        );
    }
    add_action( 'customize_register', 'noo_customizer_register_options_footer' );
endif;

// 8. WP Sidebar options.
if ( ! function_exists( 'noo_customizer_register_options_sidebar' ) ) :
    function noo_customizer_register_options_sidebar( $wp_customize ) {

        global $wp_version;
        if ( $wp_version >= 4.0 ) {
            // declare helper object.
            $helper = new NOO_Customizer_Helper( $wp_customize );

            // Change the sidebar panel priority
            $widget_panel = $wp_customize->get_panel('widgets');
            if(!empty($widget_panel)) {
                $widget_panel->priority = $helper->get_new_section_priority();
            }
        }
    }
    add_action( 'customize_register', 'noo_customizer_register_options_sidebar' );
endif;

// 8.2. Event options.
if ( ! function_exists( 'noo_customizer_register_options_event' ) ) :
    function noo_customizer_register_options_event( $wp_customize ) {
        // declare helper object.
        $helper = new NOO_Customizer_Helper( $wp_customize );

        // Section: Event
        $helper->add_section(
            'noo_customizer_section_event',
            esc_html__( 'Event', 'noo-chilli' ),
            esc_html__( 'In this section you have settings for your Event page, Archive page and Single Event page.', 'noo-chilli' ),
            true
        );

        // Sub-section: Event Page (Index Page)
        $helper->add_sub_section(
            'noo_event_sub_section_event_page',
            esc_html__( 'Event List', 'noo-chilli' ),
            esc_html__( 'Choose Layout settings for your Event List', 'noo-chilli' )
        );

        // Control: Event Layout
        $helper->add_control(
            'noo_event_layout',
            'noo_radio',
            esc_html__( 'Event Layout', 'noo-chilli' ),
            'sidebar',
            array(
                'choices' => array(
                    'fullwidth'   => esc_html__( 'Full-Width', 'noo-chilli' ),
                    'sidebar'   => esc_html__( 'With Right Sidebar', 'noo-chilli' ),
                    'left_sidebar'   => esc_html__( 'With Left Sidebar', 'noo-chilli' )
                ),
                'json' => array(
                    'child_options' => array(
                        'fullwidth'   => '',
                        'sidebar'   => 'noo_event_sidebar',
                        'left_sidebar'   => 'noo_event_sidebar'
                    )
                )
            ),
            array( 'transport' => 'postMessage' )
        );

        // Control: Event Sidebar
        $helper->add_control(
            'noo_event_sidebar',
            'widgets_select',
            esc_html__( 'Event Sidebar', 'noo-chilli' ),
            'sidebar-main',
            array(),
            array( 'transport' => 'postMessage' )
        );

        // Control: Divider 1
        $helper->add_control( 'noo_event_divider_1', 'divider', '' );

        $helper->add_control(
            'noo_event_heading_title',
            'text',
            esc_html__( 'Event Heading', 'noo-chilli' ),
            esc_html__('Events', 'noo-chilli' ),
            array(),
            array( 'transport' => 'postMessage' )
        );

        // Control: Heading Image
        $helper->add_control(
            'noo_event_heading_image',
            'noo_image',
            esc_html__( 'Heading Background Image', 'noo-chilli' ),
            '',
            array(),
            array( 'transport' => 'postMessage' )
        );
    }
    add_action( 'customize_register', 'noo_customizer_register_options_event' );
endif;

// 9. Blog options.
if ( ! function_exists( 'noo_customizer_register_options_blog' ) ) :
    function noo_customizer_register_options_blog( $wp_customize ) {

        // declare helper object.
        $helper = new NOO_Customizer_Helper( $wp_customize );

        // Section: Blog
        $helper->add_section(
            'noo_customizer_section_blog',
            esc_html__( 'Blog', 'noo-chilli' ),
            esc_html__( 'In this section you have settings for your Blog page, Archive page and Single Post page.', 'noo-chilli' ),
            true
        );

        // Sub-section: Blog Page (Index Page)
        $helper->add_sub_section(
            'noo_blog_sub_section_blog_page',
            esc_html__( 'Post List', 'noo-chilli' ),
            esc_html__( 'Choose Layout settings for your Post List', 'noo-chilli' )
        );

        // Control: Blog Layout
        $helper->add_control(
            'noo_blog_layout',
            'noo_radio',
            esc_html__( 'Blog Layout', 'noo-chilli' ),
            'sidebar',
            array(
                'choices' => array(
                    'fullwidth'   => esc_html__( 'Full-Width', 'noo-chilli' ),
                    'sidebar'   => esc_html__( 'With Right Sidebar', 'noo-chilli' ),
                    'left_sidebar'   => esc_html__( 'With Left Sidebar', 'noo-chilli' )
                ),
                'json' => array(
                    'child_options' => array(
                        'fullwidth'   => '',
                        'sidebar'   => 'noo_blog_sidebar',
                        'left_sidebar'   => 'noo_blog_sidebar'
                    )
                )
            ),
            array( 'transport' => 'postMessage' )
        );

        // Control: Blog Sidebar
        $helper->add_control(
            'noo_blog_sidebar',
            'widgets_select',
            esc_html__( 'Blog Sidebar', 'noo-chilli' ),
            'sidebar-main',
            array(),
            array( 'transport' => 'postMessage' )
        );

        // Control: Divider 1
        $helper->add_control( 'noo_blog_divider_1', 'divider', '' );

        $helper->add_control(
            'noo_blog_heading_title',
            'text',
            esc_html__( 'Blog Heading', 'noo-chilli' ),
            esc_html__('Blog', 'noo-chilli' ),
            array(),
            array( 'transport' => 'postMessage' )
        );

        // Control: Heading Image
        $helper->add_control(
            'noo_blog_heading_image',
            'noo_image',
            esc_html__( 'Heading Background Image', 'noo-chilli' ),
            '',
            array(),
            array( 'transport' => 'postMessage' )
        );

        // Control: Show Post Meta
        // $helper->add_control(
        //     'noo_blog_show_posttransparent',
        //     'checkbox',
        //     esc_html__( 'Enable Menu Transparent', 'noo-chilli' ),
        //     1,
        //     array(),
        //     array( 'transport' => 'postMessage' )
        // );

        // Control: Divider 2
        $helper->add_control( 'noo_blog_divider_2', 'divider', '' );


        // Control: Show Post Meta
        $helper->add_control(
            'noo_blog_show_post_meta',
            'checkbox',
            esc_html__( 'Show Post Meta', 'noo-chilli' ),
            1,
            array(),
            array( 'transport' => 'postMessage' )
        );

        // Control: Show Post Image
        $helper->add_control(
            'noo_blog_show_post_image',
            'checkbox',
            esc_html__( 'Show Post Image', 'noo-chilli' ),
            1,
            array(),
            array( 'transport' => 'postMessage' )
        );

        // Control: Show Readmore link
        $helper->add_control(
            'noo_blog_show_readmore',
            'checkbox',
            esc_html__( 'Show Readmore link', 'noo-chilli' ),
            1,
            array(),
            array( 'transport' => 'postMessage' )
        );



        // Sub-section: Single Post
        $helper->add_sub_section(
            'noo_blog_sub_section_post',
            esc_html__( 'Single Post', 'noo-chilli' )
        );

        // Control: Heading Image
        $helper->add_control(
            'noo_blog_single_heading_image',
            'noo_image',
            esc_html__( 'Default Heading Background Image', 'noo-chilli' ),
            '',
            array(),
            array( 'transport' => 'postMessage' )
        );

        // Control: Show Post Meta
        $helper->add_control(
            'noo_blog_post_show_post_meta',
            'checkbox',
            esc_html__( 'Show Post Meta', 'noo-chilli' ),
            1,
            array(),
            array( 'transport' => 'postMessage' )
        );

        // Control: Show Post Tags
        $helper->add_control(
            'noo_blog_post_show_post_thumbnail',
            'checkbox',
            esc_html__( 'Show Post thumbnail', 'noo-chilli' ),
            1,
            array(),
            array( 'transport' => 'postMessage' )
        );

        // Control: Show Author Bio
        $helper->add_control(
            'noo_blog_post_author_bio',
            'checkbox',
            esc_html__( 'Show Author\'s Bio', 'noo-chilli' ),
            1,
            array(),
            array( 'transport' => 'postMessage' )
        );

        // Control: Show navigation
        // $helper->add_control(
        //     'noo_blog_post_navigation',
        //     'checkbox',
        //     esc_html__( 'Show navigation', 'noo-chilli' ),
        //     1,
        //     array(),
        //     array( 'transport' => 'postMessage' )
        // );

        // Control: Divider 2
        $helper->add_control( 'noo_blog_post_divider_2', 'divider', '' );

    }
    add_action( 'customize_register', 'noo_customizer_register_options_blog' );
endif;

// 9.0 Woocommer
if( !function_exists('noo_customizer_register_options_woocommer') ):
        function noo_customizer_register_options_woocommer( $wp_customize ){
            $helper = new NOO_Customizer_Helper( $wp_customize );

            // Section: WooCommerce

            $helper->add_section(
              'noo_customizer_section_shop',
              esc_html__('WooCommerce', 'noo-chilli' ),
              '',
              true
            );

            // Sub-section: Shop page
            $helper->add_sub_section(
              'noo_woocommerce_sub_section_shop_page',
              esc_html__('Shop Page', 'noo-chilli' ),
              esc_html__( 'Choose Layout and Headline Settings for your Shop Page.', 'noo-chilli' )
            );

            // Control: Shop Layout
            $helper->add_control(
                'noo_shop_layout',
                'noo_radio',
                esc_html__( 'Shop Layout', 'noo-chilli' ),
                'fullwidth',
                array(
                    'choices' => array(
                        'fullwidth'   => esc_html__( 'Full-Width', 'noo-chilli' ),
                        'sidebar'   => esc_html__( 'With Right Sidebar', 'noo-chilli' ),
                        'left_sidebar'   => esc_html__( 'With Left Sidebar', 'noo-chilli' )
                    ),
                    'json' => array(
                        'child_options' => array(
                            'fullwidth'   => '',
                            'sidebar'   => 'noo_shop_sidebar',
                            'left_sidebar'   => 'noo_shop_sidebar',
                        )
                    )
                ),
                array( 'transport' => 'postMessage' )
            );

            // Control: Shop Sidebar
            $helper->add_control(
                'noo_shop_sidebar',
                'widgets_select',
                esc_html__( 'Shop Sidebar', 'noo-chilli' ),
                '',
                array(),
                array( 'transport' => 'postMessage' )
            );
            // Control: Heading Title
            $helper->add_control(
                'noo_shop_heading_title',
                'text',
                esc_html__( 'Shop Heading', 'noo-chilli' ),
                esc_html__('Shop', 'noo-chilli' ),
                array(),
                array( 'transport' => 'postMessage' )
            );

            // Control: Heading Image
            $helper->add_control(
                'noo_shop_heading_image',
                'noo_image',
                esc_html__( 'Heading Background Image', 'noo-chilli' ),
                '',
                array(),
                array( 'transport' => 'postMessage' )
            );
            // Control: Number of Product per Page
            $helper->add_control(
                'noo_shop_num',
                'ui_slider',
                esc_html__( 'Products Per Page', 'noo-chilli' ),
                '12',
                array(
                    'json' => array(
                        'data_min'  => '4',
                        'data_max'  => '30',
                        'data_step' => '1'
                    )
                ),
                array( 'transport' => 'postMessage' )
            );

            // Sub-section: Single Product
            $helper->add_sub_section(
                'noo_woocommerce_sub_section_product',
                esc_html__( 'Single Product', 'noo-chilli' )
            );

            // Control: Heading Image
            $helper->add_control(
                'noo_product_heading_image',
                'noo_image',
                esc_html__( 'Default Heading Background Image', 'noo-chilli' ),
                '',
                array(),
                array( 'transport' => 'postMessage' )
            );

            // Control: Products related
            $helper->add_control(
                'noo_woocommerce_product_related',
                'text',
                esc_html__( 'Related Products Count', 'noo-chilli' ),
                4,
                array(),
                array( 'transport' => 'postMessage' )
            );
        }
    add_action( 'customize_register', 'noo_customizer_register_options_woocommer' );
endif;

// 15. Custom Code
if ( ! function_exists( 'noo_customizer_register_options_custom_code' ) ) :
    function noo_customizer_register_options_custom_code( $wp_customize ) {

        // declare helper object.
        $helper = new NOO_Customizer_Helper( $wp_customize );

        // Section: Custom Code
        $helper->add_section(
            'noo_customizer_section_custom_code',
            esc_html__( 'Custom Code', 'noo-chilli' ),
            wp_kses( __( 'In this section you can add custom JavaScript and CSS to your site.<br/>Your Google analytics tracking code should be added to Custom JavaScript field.', 'noo-chilli' ), noo_allowed_html() )
        );

        // Control: Custom JS (Google Analytics)
        $helper->add_control(
            'noo_custom_javascript',
            'textarea',
            esc_html__( 'Custom JavaScript', 'noo-chilli' ),
            '',
            array(),
            array( 'transport' => 'postMessage' )
        );

        // Control: Custom CSS
        $helper->add_control(
            'noo_custom_css',
            'textarea',
            esc_html__( 'Custom CSS', 'noo-chilli' ),
            '',
            array(),
            array( 'transport' => 'postMessage' )
        );
    }
    add_action( 'customize_register', 'noo_customizer_register_options_custom_code' );
endif;

// 16. Import/Export Settings.
if ( ! function_exists( 'noo_customizer_register_options_tools' ) ) :
    function noo_customizer_register_options_tools( $wp_customize ) {

        // declare helper object.
        $helper = new NOO_Customizer_Helper( $wp_customize );

        // Section: Custom Code
        $helper->add_section(
            'noo_customizer_section_tools',
            esc_html__( 'Import/Export Settings', 'noo-chilli' ),
            esc_html__( 'All themes from NooTheme share the same theme setting structure so you can export then import settings from one theme to another conveniently without any problem.', 'noo-chilli' )
        );

        // Sub-section: Import Settings
        $helper->add_sub_section(
            'noo_tools_sub_section_import',
            esc_html__( 'Import Settings', 'noo-chilli' ),
            wp_kses( __( 'Click Upload button then choose a JSON file (.json) from your computer to import settings to this theme.<br/>All the settings will be loaded for preview here and will not be saved until you click button "Save and Publish".', 'noo-chilli' ), noo_allowed_html() )
        );

        // Control: Upload Settings
        $helper->add_control(
            'noo_tools_import',
            'import_settings',
            esc_html__( 'Upload', 'noo-chilli' )
        );

        // Sub-section: Export Settings
        $helper->add_sub_section(
            'noo_tools_sub_section_export',
            esc_html__( 'Export Settings', 'noo-chilli' ),
            esc_html__( 'Simply click Download button to export all your settings to a JSON file (.json).<br/>You then can use that file to restore theme settings to any theme of NooTheme.', 'noo-chilli' )
        );

        // Control: Download Settings
        $helper->add_control(
            'noo_tools_export',
            'export_settings',
            esc_html__( 'Download', 'noo-chilli' )
        );

    }
    add_action( 'customize_register', 'noo_customizer_register_options_tools' );
endif;

