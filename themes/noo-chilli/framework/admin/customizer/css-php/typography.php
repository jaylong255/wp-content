<?php
// Variables
$noo_typo_use_custom_fonts = noo_get_option( 'noo_typo_use_custom_fonts', false );
$noo_typo_headings_uppercase = noo_get_option( 'noo_typo_headings_uppercase', false );
$noo_typo_body_font_size = noo_get_option( 'noo_typo_body_font_size', noo_default_font_size() );

if( $noo_typo_use_custom_fonts ) :
	$noo_typo_headings_font = noo_get_option( 'noo_typo_headings_font', noo_default_font_family() );
	$noo_typo_headings_font_weight = noo_get_option( 'noo_typo_headings_font_weight', 'bold' );
	$noo_typo_headings_uppercase = noo_get_option( 'noo_typo_headings_uppercase', false );

	$noo_typo_body_font = noo_get_option( 'noo_typo_body_font', noo_default_font_family() );
	$noo_typo_body_font_style = noo_get_option( 'noo_typo_body_font_style', 'normal' );
	$noo_typo_body_font_weight = noo_get_option( 'noo_typo_body_font_weight', noo_default_font_weight() );

?>

/* Body style */
/* ===================== */
body {
	font-family: "<?php echo esc_html( $noo_typo_body_font ); ?>", sans-serif;
	font-size: <?php echo esc_html( $noo_typo_body_font_size ) . 'px'; ?>;
	font-style: <?php echo esc_html( $noo_typo_body_font_style ); ?>;
	font-weight: <?php echo esc_html( $noo_typo_body_font_weight ); ?>;
}

/* Headings */
/* ====================== */
body h1, body h2, body h3, body h4, body h5, body h6,
body .h1, body .h2, body .h3, body .h4, body .h5, body .h6,
body .noo-footer div.widget h4.widget-title,
body .services-item .services-meta h3,
body .noo_our_blog li h3 a,
body .noo_whychoose .whycontent h4,
body .noo-portfolio-list .noo-list-content h3 a,
body .entry-title,
body .comments-title,
body #respond #reply-title,
body .header-title
    {
	font-family: "<?php echo esc_html( $noo_typo_headings_font ); ?>", sans-serif;
	font-weight: <?php echo esc_html( $noo_typo_headings_font_weight ); ?>;	
	<?php if ( !empty( $noo_typo_headings_uppercase ) ) : ?>
		text-transform: uppercase;
	<?php else : ?>
		text-transform: none;
	<?php endif; ?>
}

/* Other style */
/* ====================== */
.comment-author,
.comment-reply-title,
.footer-top .noo-menu li a,
.pagination .page-numbers,
.entry-tags span,
ol,
.widget.widget_recent_entries li a,
.default_list_products .woocommerce ul.products.grid li.product figcaption h3.product_title,
.default_list_products .woocommerce ul.products li.product figure figcaption .product_title,
.woocommerce div.product .wpn_buttons,
.woocommerce div.product .product-navigation .next-product a > span,
.woocommerce div.product .product-navigation .next-product a .next-product-info .next-desc .amount,
.woocommerce div.product .product-navigation .prev-product a > span,
.woocommerce div.product div.summary .variations_form label,
.woocommerce div.product div.summary .product_meta > span,
.woocommerce .list_products_toolbar .products-toolbar span,
.woocommerce ul.products li.product .price,
.woocommerce ul.products.list li.product h3.product_title,
.woocommerce div.product span.price,
.woocommerce div.product p.price,
.woocommerce div.product .woocommerce-tabs .nav-tabs > li > a,
.woocommerce .quantity .plus,
.woocommerce .quantity .minus,
.woocommerce #reviews #comments ol.commentlist li .comment-text p.meta strong,
.woocommerce table.shop_attributes th,
.woocommerce table.cart .product-price,
.woocommerce table.cart .product-subtotal,
.woocommerce .checkout #order_review td.product-total,
.woocommerce .checkout #order_review .cart-subtotal td,
.woocommerce .checkout #order_review .order-total td,
.woocommerce .view_order .wrap_order_details table tr .amount,
.woocommerce .checkout_complete ul.order_details.general li.total strong,
.woocommerce table.my_account_orders tr td.order-total .amount,
.woocommerce .widget_price_filter .price_slider_amount,
.home-lookbook-detai .lookbook-detail-content ol li b {
	font-family: "<?php echo esc_html( $noo_typo_headings_font ); ?>", sans-serif;
}
<?php else : ?>
/* Body style */
/* ===================== */
body,
.noo-ds,
.masonry-filters.filter-gallery a,
.widget ul li a, .widget ol li a
{
	font-size: <?php echo esc_html( $noo_typo_body_font_size ) . 'px'; ?>;
}
.news-grid-item .news-content .news-date,
.news-grid-item .news-content .news-author-cm,
.blog-content .news-date,
.blog-content .news-author-cm
{
	font-size: <?php echo esc_html( $noo_typo_body_font_size - 2 ) . 'px'; ?>;
}
.news-grid-item .news-content p,
.col-menu-today .menu-content .menu-attributes .attr,
.noo-sub-title,
.noo_team_item .team_position,
.noo-menu-with-slider .menu-content .menu-attributes .attr,
.blog-content.style-new .entry-content,
.blog-content .entry-meta > span,
.widget_categories ul li span
{
	font-size: <?php echo esc_html( $noo_typo_body_font_size - 2 ) . 'px'; ?>;
}
.noo-services-square .services-square-icon h3,
.news-grid-item .news-content h4{
	font-size: <?php echo esc_html( $noo_typo_body_font_size + 2 ) . 'px'; ?>;
}

/* Headings */
/* ====================== */
body h1, body h2, body h3, body h4, body h5, body h6,
body .h1, body .h2, body .h3, body .h4, body .h5, body .h6,
body .noo-footer div.widget h4.widget-title,
body .services-item .services-meta h3,
body .noo_our_blog li h3 a,
body .noo_whychoose .whycontent h4,
body .noo-portfolio-list .noo-list-content h3 a,
body .entry-title,
body .comments-title,
body #respond #reply-title{
	<?php if ( !empty( $noo_typo_headings_uppercase ) ) : ?>
		text-transform: uppercase;
	<?php else : ?>
		text-transform: none;
	<?php endif; ?>
}
<?php endif; ?>