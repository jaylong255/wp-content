<?php
// Variables
$default_link_color = noo_default_text_color();

$noo_site_link_color = $default_link_color;
$noo_site_link_hover_color = noo_get_option( 'noo_site_link_hover_color',  noo_default_primary_color() );

$noo_site_link_color_lighten_10 = lighten( $noo_site_link_hover_color, '10%' );
$noo_site_link_color_darken_5   = darken( $noo_site_link_hover_color, '5%' );
$noo_site_link_color_darken_15   = darken( $noo_site_link_hover_color, '15%' );

$default_font_color = noo_default_text_color();
$default_headings_color = noo_default_headings_color();

$noo_typo_use_custom_fonts_color = noo_get_option( 'noo_typo_use_custom_fonts_color', false );
$noo_typo_body_font_color = $noo_typo_use_custom_fonts_color ? noo_get_option( 'noo_typo_body_font_color', $default_font_color ) : $default_font_color;
$noo_typo_headings_font_color = $noo_typo_use_custom_fonts_color ? noo_get_option( 'noo_typo_headings_font_color', $default_headings_color ) : $default_headings_color; 

$noo_header_custom_nav_font = noo_get_option( 'noo_header_custom_nav_font', false );
$noo_header_nav_link_color = $noo_header_custom_nav_font ? noo_get_option( 'noo_header_nav_link_color', 'white' ) : 'white';
$noo_header_nav_link_hover_color = $noo_header_custom_nav_font ? noo_get_option( 'noo_header_nav_link_hover_color', $noo_site_link_hover_color ) : $noo_site_link_hover_color;

$noo_site_hide_icon = noo_get_option( 'noo_site_hide_icon' , false);
$noo_site_custom_icon = noo_get_image_option( 'noo_site_custom_icon', '' );

?>

body {
	color: <?php echo esc_html($noo_typo_body_font_color); ?>;
}

body h1, body h2, body h3, body h4, body h5, body h6,
body .h1, body .h2, body .h3, body .h4, body .h5, body .h6,
body h1 a, body h2 a, body h3 a, body h4 a, body h5 a, body h6 a,
body .h1 a, body .h2 a, body .h3 a, body .h4 a, body .h5 a, body .h6 a,
body .services-item .services-meta h3,
body .noo_our_blog li h3 a,
body .noo_whychoose .whycontent h4,
body .noo-portfolio-list .noo-list-content h3 a,
body .entry-title,
body .comments-title,
body #respond #reply-title,
body .noo-title,
body .noo-services-square .services-square-icon h3,
body .single-event-header .events-meta .events-meta-bk h2.tribe-events-single-event-title,
body .blog-content .entry-title a,
body .widget-title,
.woocommerce-cart table.shop_table .product-name a,
.woocommerce .cart-collaterals .cart_totals h2,
.woocommerce-page .cart-collaterals .cart_totals h2,
.woocommerce-cart table.shop_table .noo_coupon_code .coupon label,
body .noo-shop-sidebar .widget-title,
.menu-sections li .menu-content h6,
.masonry-filters ul li a,
.menu-grid-item .menu-item .menu-grid-content h6,
.menu-grid-item .menu-item-style2 .menu-grid-content .attr,
.woocommerce-checkout .woocommerce-billing-fields h3,
.woocommerce-checkout .order_review_wrap #order_review_heading,
.woocommerce-checkout .woocommerce-shipping-fields #ship-to-different-address label
{
	color: <?php echo esc_html($noo_typo_headings_font_color); ?>;
}

body h1 a:hover, body h2 a:hover, body h3 a:hover, body h4 a:hover, body h5 a:hover, body h6 a:hover,
body .h1 a:hover, body .h2 a:hover, body .h3 a:hover, body .h4 a:hover, body .h5 a:hover, body .h6 a:hover{
	color: <?php echo esc_html($noo_site_link_hover_color); ?>;
}

/* Global Link */
/* ====================== */
a {
	color: <?php echo esc_html($noo_site_link_color); ?>;
}

body .noo-footer a:hover,
body a:hover,
body a:focus,
body .text-primary,
body a.text-primary:hover,
ol,
.noo-portlio-ds span a:hover,
.noo-page-heading .noo-page-breadcrumb ul li a:hover,
body .noo-search li span:hover,
body .tp-caption a:hover{
	color: <?php echo esc_html($noo_site_link_hover_color); ?>;
}

.bg-primary,
.btn-default:hover {
	background-color: <?php echo esc_html($noo_site_link_hover_color); ?>;
	border-color: <?php echo esc_html($noo_site_link_hover_color); ?>;
}
a.bg-primary:hover,
body .site .tp-constru:hover,
body .site .tp-constru:focus{
	background-color: <?php echo esc_html($noo_site_link_color_darken_15); ?>;
}
.bg-primary-overlay{
  background: <?php echo fade($noo_site_link_hover_color, '90%'); ?>;
}

<?php if( $noo_site_custom_icon != '' ) : ?>
.noo-title-block:after{
background-image: url("<?php echo esc_html($noo_site_custom_icon); ?>");
}
<?php endif; ?>

<?php if ( $noo_site_hide_icon || $noo_site_custom_icon == '' ) : ?>
.tribe-events-loop .type-tribe_events .noo-title-block:after
{
display: table !important;
visibility: hidden;
}
.noo-title-block:after{
display:none;
}
<?php else : ?>
.tribe-events-loop .type-tribe_events .noo-title-block:after
{
visibility: visible;
}
.noo-title-block:after{
display:table;
}
<?php endif; ?>

/* Navigation Color */
/* ====================== */

/* Default menu style */

body .noo-menu li > a:hover,
body .noo-menu li > a:active,
body .noo-menu li.current-menu-item > a{
	color: <?php echo esc_html($noo_header_nav_link_hover_color); ?>;
}

/* NavBar: Link */


body.page-menu-transparent .navbar:not(.navbar-fixed-top) .noo-menu > li > a:hover,
.noo-menu li > a:hover,
.noo-menu li > a:focus,
.noo-menu li:hover > a,
.noo-menu li.sfHover > a,
.noo-menu li.current-menu-item > a {
	color: <?php echo esc_html($noo_header_nav_link_hover_color); ?>;
}
.noo-menu > li a.cart-button span em{
	background-color: <?php echo esc_html($noo_header_nav_link_hover_color); ?>;
}

.noo-menu li > a,
.navbar-nav li > a {
	color: <?php echo esc_html($noo_header_nav_link_color); ?>;
}
/* Border color */
@media (min-width: 992px) {
	.navbar-default .noo-menu.sf-menu > li > ul.sub-menu:before,
	.noo-menu.sf-menu > li.align-center > ul.sub-menu:before,
	.noo-menu.sf-menu > li.align-right > ul.sub-menu:before,
	.noo-menu.sf-menu > li.align-left > ul.sub-menu:before,
	.noo-menu.sf-menu > li.full-width.sfHover > a:before {
		border-bottom-color: <?php echo esc_html($noo_site_link_hover_color); ?> !important;
	}
}

/* Dropdown Color */
.noo-menu li ul.sub-menu li > a:hover,
.noo-menu li ul.sub-menu li > a:focus,
.noo-menu li ul.sub-menu li:hover > a,
.noo-menu li ul.sub-menu li.sfHover > a,
.noo-menu li ul.sub-menu li.current-menu-item > a,
body .tp-caption a{
	color: <?php echo esc_html($noo_header_nav_link_hover_color); ?>;
}


/* Other Text/Link Color */
/* ====================== */
body .noo_our_blog li .noo_reading,
body .noo-slider-content .noo_readmore,
body .services-item .services-content .fa,
.woocommerce div.product .noo-shop-item .price,
.woocommerce-cart table.shop_table .product-subtotal,
.woocommerce .cart-collaterals .cart_totals table tr td .shipping-calculator-button,
.woocommerce-page .cart-collaterals .cart_totals table tr td .shipping-calculator-button,
.woocommerce-checkout .order_review_wrap #order_review table.shop_table tfoot td strong,
.woocommerce-checkout .woocommerce-info a,
.woocommerce .widget_price_filter .price_label,
.woocommerce div.product .noo-shop-item span.price,
.woocommerce div.product .entry-summary div[itemprop='offers'] .price span,
.woocommerce div.product .entry-summary .woocommerce-product-rating .star-rating,
.woocommerce div.product .entry-summary div[itemprop='offers'] .price,
.woocommerce div.product .entry-summary .product_share a:hover,
.widget_product_categories ul li a:hover,
.widget_product_categories ul li.current-cat a,
.noo-menu.noo_megamenu ul.sub-menu li ul.sub-menu li:hover a,
.noo-menu.noo_megamenu ul.sub-menu li a.sf-with-ul:hover,
.noo-menu.noo_megamenu .noo-menu.noo_widget_area .noo_megamenu_widget_area li a:hover,
.recent-tweets li .tw-content a.quick_hashtag:hover, .recent-tweets li .tw-content a.quick_anchor:hover,
.blog-content .entry-meta .noo-tags-link:hover, .blog-content .entry-meta .noo-cat-link:hover,
.news-grid-item .news-content .readmore
{
	color: <?php echo esc_html($noo_site_link_hover_color); ?>;
}
.woocommerce div.product .entry-summary p.cart a.single_add_to_cart_button:hover,
.single-product .entry-summary .yith-wcwl-add-to-wishlist .yith-wcwl-add-button.show a:hover,
.noo-tribe-fullname .add-to-cart button
{
	background-color: <?php echo esc_html($noo_site_link_hover_color); ?> !important;
}
/*Service page*/
.noo-contact-wrap .noo-contact-info p {
	border-bottom: 1px solid <?php echo esc_html($noo_site_link_hover_color); ?>;
}
.noo-pflisder .owl-controls .owl-page.active span, 
.noo-pflisder .owl-controls.clickable .owl-page:hover span,
body .footer-bottom .mailchimp-widget button,
body .wpcf7-form .wpcf7-submit,
body .site .tp-constru,
body .wpb_accordion .wpb_accordion_wrapper h3.ui-accordion-header-active,
body .wpb_accordion .wpb_accordion_wrapper h3.wpb_accordion_header:hover,
.woocommerce div.product .noo-shop-item span.onsale,
.noo-shop-meta,
.woocommerce nav.woocommerce-pagination ul li a.current,
.woocommerce nav.woocommerce-pagination ul li span.current,
.woocommerce nav.woocommerce-pagination ul li a:hover,
.woocommerce nav.woocommerce-pagination ul li span:hover,
.woocommerce nav.woocommerce-pagination ul li a:focus,
.woocommerce nav.woocommerce-pagination ul li span:focus,
.woocommerce .widget_price_filter .price_slider_amount .button,
.woocommerce .widget_price_filter .ui-slider .ui-slider-range,
.woocommerce div.product form.cart .button:hover,
.woocommerce div.product .woocommerce-tabs ul.tabs li.active,
.woocommerce div.product .woocommerce-tabs ul.tabs li:hover,
.quick_readmore,
.woocommerce #reviews #review_form #submit,
.woocommerce table.shop_table input.button,
.woocommerce table.shop_table input.button,
body.woocommerce-cart .wc-proceed-to-checkout a.button.alt,
.woocommerce #payment #place_order,
.woocommerce-page #payment #place_order,
.woocommerce form.checkout_coupon .button,
.woocommerce form.login .button, .woocommerce form.register .button,
.woocommerce-cart .cart-collaterals .cart_totals table button,
.woocommerce table.my_account_orders .order-actions .button,
.woocommerce-account input.button,
.services-gallery-wrap .tz_ser_prev,
.services-gallery-wrap .tz_ser_next,
.woocommerce .return-to-shop .wc-backward,
.woocommerce .return-to-shop .wc-backward:focus,
.woocommerce #reviews #review_form #submit:focus,
.noo-contact-wrap,
.noo-contact-wrap .noo-contact-line .line-f,
.noo-contact-wrap .noo-contact-line .line-l,
.woocommerce .cart-collaterals .cart_totals .wc-proceed-to-checkout a,
.woocommerce-page .cart-collaterals .cart_totals .wc-proceed-to-checkout a,
.woocommerce .cart-collaterals .cart_totals .noo-button-update-cart:hover,
.woocommerce-page .cart-collaterals .cart_totals .noo-button-update-cart:hover,
.woocommerce-cart table.shop_table .noo_coupon_code .coupon input[type='submit']:hover,
.woocommerce .widget_price_filter .ui-slider .ui-slider-handle,
.woocommerce div.product .noo-shop-item .noo-item-meta .button,
.noo-shop-header .yith-wcwl-wishlistaddedbrowse a,
.noo-shop-header .yith-wcwl-wishlistexistsbrowse a,
.noo-shop-header .noo-wish-list .add_to_wishlist,
.woocommerce span.onsale,
.single-product .quantity .qty,
.single-tribe_events #tribe-events-content form.cart .woocommerce #respond input#submit.alt,
.single-tribe_events #tribe-events-content form.cart .woocommerce a.button.alt,
.single-tribe_events #tribe-events-content form.cart .woocommerce button.button.alt,
.single-tribe_events #tribe-events-content form.cart .woocommerce input.button.alt,
.woocommerce table.wishlist_table .product-add-to-cart a.button.alt:hover,
.menu-grid-item .menu-item .menu-grid-thum .price,
.noo-menu.noo_megamenu ul.sub-menu li a.sf-with-ul:after,
.noo-menu.noo_megamenu .noo-menu.noo_widget_area .widget-title:after
{
	background: <?php echo esc_html($noo_site_link_hover_color); ?>;
}
.single-product div.product form.cart .button:focus,
.single-product .entry-summary .yith-wcwl-add-to-wishlist .yith-wcwl-add-button.show a:focus{
	background-color: <?php echo esc_html($noo_site_link_hover_color); ?> !important; 
}
@media (max-width: 767px){
	.tribe-events-sub-nav li a:hover, .tribe-events-sub-nav li a:visited {
	    color: <?php echo esc_html($noo_site_link_hover_color); ?>;
	}
}
@media (max-width: 991px){
	#tribe-events-footer .btn-view-all a:hover {
	    color: <?php echo esc_html($noo_site_link_hover_color); ?>;
	}
}
.single-product div.product form.cart .button:hover,
.woocommerce-page div.product .woocommerce-tabs .panel #review_form #respond form #submit:hover,
.woocommerce-checkout form.checkout_coupon input[type='submit']:hover
{
	background: <?php echo esc_html($noo_site_link_hover_color); ?> !important;
}
.woocommerce .cart-collaterals .cart_totals table tr td .shipping-calculator-form button:hover,
.woocommerce-page .cart-collaterals .cart_totals table tr td .shipping-calculator-form button:hover{
	background: <?php echo esc_html($noo_site_link_hover_color); ?> !important;
}
.widget_product_search form input[type='submit'],
.widget_product_search form input[type='submit']:focus
{
    background-color:<?php echo esc_html($noo_site_link_hover_color); ?>;
}
.noo-contact-wrap .noo-contact-line .line-f:after,
.noo-contact-wrap .noo-contact-line .line-l:after{
border-right-color: <?php echo esc_html($noo_site_link_hover_color); ?>;
}
.widget_product_search form input[type='submit']:hover,
.woocommerce #reviews #review_form #submit:hover,
.woocommerce-cart .wc-proceed-to-checkout a:hover,
.woocommerce table.shop_table input.button:hover,
.quick_readmore:hover,
.woocommerce form.checkout_coupon .button:hover,
.woocommerce form.login .button, .woocommerce form.register .button:hover,
.woocommerce-cart .cart-collaterals .cart_totals table button:hover,
.woocommerce table.shop_table input.button:focus,
.woocommerce table.my_account_orders .order-actions .button:hover,
.woocommerce-account input.button:hover,
.services-gallery-wrap .tz_ser_prev:hover,
.services-gallery-wrap .tz_ser_next:hover,
.woocommerce .return-to-shop .wc-backward:hover
{
    background-color:<?php echo esc_html($noo_site_link_color_lighten_10); ?>
}

/* WordPress Element */
/* ====================== */
/*Portfolio link*/
body .masonry-filters ul li a.selected,
body .noo-portfolio-buttom a,
.woocommerce ul.product_list_widget li span.amount,
.noo-entry-summary .price span,
.comment-wrap .comment-block .comment-reply-link,
.comment-wrap .comment-block .comment-reply-link i
{
	color: <?php echo esc_html($noo_site_link_hover_color); ?>;
}
/* Comment */
h2.comments-title span,
.comment-reply-link,
.comment-author a:hover,
.woocommerce-account .noo-title-login i,
.woocommerce-account .noo-title-register i{
	color: <?php echo esc_html($noo_site_link_hover_color); ?>;
}

/* Post */
.content-meta > span > a,
.hentry.format-quote a:hover,
.hentry.format-link a:hover,
.single .hentry.format-quote .content-title:hover,
.single .hentry.format-link .content-title:hover,
.single .hentry.format-quote a:hover,
.single .hentry.format-link a:hover,
.sticky h2.content-title:before,
.post-navigation .prev-post:hover i,
.post-navigation .next-post:hover i,
.noo-custom-navigation span.current,
.noo-custom-navigation a.current,
.noo-custom-navigation span:hover,
.noo-custom-navigation a:hover,
.noo-custom-navigation span:focus,
.noo-custom-navigation a:focus,
.woocommerce .woocommerce-info:before{
	color: <?php echo esc_html($noo_site_link_hover_color); ?>;
}

body .read-more,
body .comment-form #submit
{
	background: <?php echo esc_html($noo_site_link_hover_color); ?>;
}
body .read-more:hover,
body .comment-form #submit:hover,
body .footer-bottom .mailchimp-widget button:hover,
body .footer-bottom .mailchimp-widget button:focus,
body .pagination a.page-numbers:hover,
body .wpcf7-form .wpcf7-submit:hover,
body .wpcf7-form .wpcf7-submit:focus,
.woocommerce .widget_price_filter .price_slider_amount .button:hover
{
	background: <?php echo esc_html($noo_site_link_color_lighten_10); ?>;
}
body .pagination a.page-numbers:hover:{
    border: <?php echo esc_html($noo_site_link_color_lighten_10); ?>;
}
.entry-tags a:hover,
.noo-shop-meta a:hover,
.noo-shop-meta .nooquick-view:hover{
	color: <?php echo esc_html($noo_site_link_color_lighten_10); ?>;
}

.read-more span,
body .pagination .page-numbers.current,
body .pagination a.page-numbers:hover,
body .pagination a.page-numbers:focus,
.woocommerce div.product form.cart .button,
.woocommerce .woocommerce-info,
.widget_product_search form input[type='submit']:focus,
.woocommerce .cart-collaterals .cart_totals .noo-button-update-cart,
.woocommerce-page .cart-collaterals .cart_totals .noo-button-update-cart{
	border-color: <?php echo esc_html($noo_site_link_hover_color); ?>;
}

.content-thumb:before,
.entry-tags a:hover {
	background: <?php echo esc_html($noo_site_link_hover_color); ?>;
}

body .pagination .page-numbers.current{
	//color: <?php echo esc_html($noo_site_link_hover_color); ?>;
	background: <?php echo esc_html($noo_site_link_hover_color); ?>;
}

.content-social a:hover {
	color: <?php echo esc_html($noo_site_link_hover_color); ?>;
	border-color: <?php echo esc_html($noo_site_link_hover_color); ?>;
}
.noo_social .social-all a:hover{
	background: <?php echo esc_html($noo_site_link_hover_color); ?>;
}

/* Widget */
.widget-title:after,
.wigetized .widget .widget-title,
.wigetized .widget a:hover,
.wigetized .widget ul li a:hover,
.wigetized .widget ol li a:hover,
.widget.widget_recent_entries li a:hover,
.widget_calendar #wp-calendar > tbody > tr > td > a,
.noo-recent-news .posts-loop-title h3:after,
.h3-title:after {
	color: <?php echo esc_html($noo_site_link_hover_color); ?>;
}


/* Shortcode */
/* ====================== */

.btn-primary,
.wpcf7-submit,
.form-submit input[type="submit"],
.widget_newsletterwidget .newsletter-submit {
	border-color: <?php echo esc_html($noo_site_link_hover_color); ?>;
}
.noo-form .book-form-submit .wpcf7-submit{
	background: <?php echo esc_html($noo_site_link_hover_color); ?>;
}
.noo-form .book-form-submit .wpcf7-submit:hover{
	background: <?php echo esc_html($noo_site_link_color_darken_15); ?>;
}
.button:hover,
.btn:hover,
.btn:focus,
.btn-primary:hover,
.btn-primary:focus,
.btn-primary:active,
.btn-primary.active,
.form-submit input[type="submit"]:hover,
.form-submit input[type="submit"]:focus,
.wpcf7-submit:hover,
.wpcf7-submit:focus {
	background-color: <?php echo esc_html($noo_site_link_hover_color); ?>;
	border-color: <?php echo esc_html($noo_site_link_hover_color); ?>;
}

.btn-primary.pressable {
	-webkit-box-shadow: 0 4px 0 0 <?php echo esc_html($noo_site_link_color_darken_15); ?>,0 4px 9px rgba(0,0,0,0.75) !important;
	box-shadow: 0 4px 0 0  <?php echo esc_html($noo_site_link_color_darken_15); ?>,0 4px 9px rgba(0,0,0,0.75) !important;
}

.btn-link,
.btn.btn-white:hover,
.wpcf7-submit.btn-white:hover,
.widget_newsletterwidget .newsletter-submit.btn-white:hover {
	color: <?php echo esc_html($noo_site_link_color); ?>;
}


.btn-link:hover,
.btn-link:focus {
	color: <?php echo esc_html($noo_site_link_hover_color); ?>;
}

/*form*/
textarea,
input[type="text"],
input[type="password"],
input[type="datetime"],
input[type="datetime-local"],
input[type="date"],
input[type="month"],
input[type="time"],
input[type="week"],
input[type="number"],
input[type="email"],
input[type="url"],
input[type="search"],
input[type="tel"],
input[type="color"]{
    outline-color: <?php echo esc_html($noo_site_link_hover_color); ?>;
}
/* Noo Form reservation */
.ui-widget-header{
	border-color: <?php echo esc_html($noo_site_link_hover_color); ?> !important;
}
.ui-timepicker-table tr td.ui-timepicker-hour-cell:hover,
.ui-timepicker-table tr td.ui-timepicker-minute-cell:hover,
.ui-widget-header,
.ui-datepicker-calendar tr td:hover,
.ui-datepicker .ui-datepicker-header{
	background-color: <?php echo esc_html($noo_site_link_hover_color); ?> !important;
}
.ui-timepicker-table tr td a.ui-state-active,
.ui-datepicker-calendar tr td a.ui-state-active{
	color: <?php echo esc_html($noo_site_link_hover_color); ?> !important;
}

/* Noo About */
.noo-images{
	border-color: <?php echo esc_html($noo_site_link_hover_color); ?>;
}
.noo-images .logo-images{
	background-color: <?php echo esc_html($noo_site_link_hover_color); ?>;
}
/* Mail Chip */
.mc-subscribe-form .mailchip-sm{
	background-color: <?php echo esc_html($noo_site_link_hover_color); ?>;
}
/* Event */
#tribe-events .tribe-events-button,
#tribe-bar-form .tribe-bar-submit input[type=submit],
.tribe-events-calendar td.tribe-events-present div[id*=tribe-events-daynum-]{
	background-color: <?php echo esc_html($noo_site_link_hover_color); ?>;
}
#tribe-events-content .tribe-events-tooltip h4{
	color: <?php echo esc_html($noo_site_link_hover_color); ?>;
}
/* Recent Tweet */
.recent-tweets li .tw-content a{
	color: <?php echo esc_html($noo_site_link_hover_color); ?>;
}
/* Blog */
.blog-content .noo-more a{
	background-color: <?php echo esc_html($noo_site_link_hover_color); ?>;
}
.blog-content .entry-meta > span:hover,
.blog-content .entry-meta > span:hover a,
.widget_categories ul li:hover span,
.widget_categories ul li:hover,
.widget_categories ul li a:hover,
.blog-content .entry-title a:hover{
	color: <?php echo esc_html($noo_site_link_hover_color); ?>;
}
