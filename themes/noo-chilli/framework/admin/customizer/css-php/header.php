<?php
// Variables
$default_primary_color = noo_default_primary_color();
$noo_typo_headings_font = $noo_typo_use_custom_fonts ? noo_get_option( 'noo_typo_headings_font', noo_default_headings_font_family() ) : noo_default_headings_font_family();

$default_header_bg_color = noo_default_header_bg();
$default_nav_font_size = '14'; // noo_default_font_size();
$default_font_color = noo_default_text_color();
$default_font = noo_default_font_family();
$default_logo_font_color = noo_default_logo_color();

$noo_header_bg_color = noo_get_option( 'noo_header_bg_color', $default_header_bg_color );

$noo_header_nav_position = noo_get_option( 'noo_header_nav_position', 'fixed_top' );

$noo_header_nav_icon_cart   = noo_get_option( 'noo_header_nav_icon_cart', false );

$noo_header_custom_nav_font = noo_get_option( 'noo_header_custom_nav_font', false );

$noo_header_nav_font = $noo_header_custom_nav_font ? noo_get_option( 'noo_header_nav_font', $noo_typo_headings_font ) : $noo_typo_headings_font;
$noo_header_nav_font_style = $noo_header_custom_nav_font ? noo_get_option( 'noo_header_nav_font_style', 'normal' ) : 'normal';
$noo_header_nav_font_weight = $noo_header_custom_nav_font ? noo_get_option( 'noo_header_nav_font_weight', '400' ) : '400';
$noo_header_nav_font_subset = $noo_header_custom_nav_font ? noo_get_option( 'noo_header_nav_font_subset', 'latin' ) : 'latin';
$noo_header_nav_font_size = noo_get_option( 'noo_header_nav_font_size', $default_nav_font_size );
$noo_header_nav_uppercase = noo_get_option( 'noo_header_nav_uppercase', false );

$noo_header_use_image_logo = noo_get_option( 'noo_header_use_image_logo', false );

$noo_header_logo_font = noo_get_option( 'noo_header_logo_font', noo_default_logo_font_family() );
$noo_header_logo_font_size = noo_get_option( 'noo_header_logo_font_size', '30' );
$noo_header_logo_font_color = noo_get_option( 'noo_header_logo_font_color', noo_default_logo_color() );
$noo_header_logo_font_style = noo_get_option( 'noo_header_logo_font_style', 'normal' );
$noo_header_logo_font_weight = noo_get_option( 'noo_header_logo_font_weight', '700' );
$noo_header_logo_font_subset = noo_get_option( 'noo_header_logo_font_subset', 'latin' );
$noo_header_logo_uppercase = noo_get_option( 'noo_header_logo_uppercase', false );

$noo_header_logo_image_height = noo_get_option( 'noo_header_logo_image_height', '90' );

$noo_header_nav_height = noo_get_option( 'noo_header_nav_height', '85' );
$noo_header_nav_link_spacing = noo_get_option( 'noo_header_nav_link_spacing', '22' );

$noo_header_nav_toggle_size = noo_get_option( 'noo_header_nav_toggle_size', '25' );

$noo_header_top_bar = noo_get_option( 'noo_header_top_bar', true );
$noo_header_nav_bg_color = noo_get_option( 'noo_header_nav_bg_color', '#000000' );
?>

/* Header */
/* ====================== */


/* Navigation Typography */
/* ====================== */

/* NavBar: Typo */
.noo-menu li > a,
body .navbar-nav li > a {
font-family: "<?php echo esc_html($noo_header_nav_font); ?>", sans-serif;
font-size: <?php echo esc_html($noo_header_nav_font_size) . 'px'; ?>;
}
body .noo-social li a,
body .noo-search li span{
font-size: <?php echo esc_html($noo_header_nav_font_size) . 'px'; ?>;
}
.noo-menu > li a.cart-button span em{
font-size: <?php echo esc_html($noo_header_nav_font_size - 4) . 'px'; ?>;    
}
.noo-menu li > a,
body .navbar-nav > li > a {
font-style: <?php echo esc_html($noo_header_nav_font_style); ?>;
font-weight: <?php echo esc_html($noo_header_nav_font_weight); ?>;
<?php if ( !empty( $noo_header_nav_uppercase ) ) : ?>
    text-transform: uppercase;
<?php else : ?>
    text-transform: none;
<?php endif; ?>
}

<?php if ( $noo_header_nav_position == 'fixed_top' || $noo_header_nav_position == 'static_tops' ) : ?>
    @media (min-width: 992px) {
    body .navbar-nav li > a{
    padding-left: <?php echo esc_html($noo_header_nav_link_spacing) . 'px'; ?>;
    padding-right: <?php echo esc_html($noo_header_nav_link_spacing) . 'px'; ?>;
    }
    .navbar:not(.navbar-shrink) .navbar-nav > li > a,
    .noo-social li a,
    .noo-search li span{
    line-height: <?php echo esc_html($noo_header_nav_height) . 'px'; ?>;
    }
    }

    .navbar-toggle {
    height: <?php echo esc_html($noo_header_nav_height) . 'px'; ?>;
    }
    .navbar:not(.navbar-shrink) .navbar-brand {
    line-height: <?php echo esc_html($noo_header_nav_height) . 'px'; ?>;
    height: <?php echo esc_html($noo_header_nav_height) . 'px'; ?>;
    }
<?php endif; ?>

<?php if ( $noo_header_nav_bg_color ) : ?>
.rest-nav,    
.noo-box-header{
    background: <?php echo esc_html($noo_header_nav_bg_color); ?>;
}    
<?php endif; ?>

/* Top Bar */
/* ====================== */
<?php if ( ! $noo_header_top_bar ) : ?>
.noo-box-header{
    top: 0px;
}
<?php endif; ?>
/* Logo */
/* ====================== */
<?php if ( $noo_header_logo_font_color ) : ?>
.no-menu-assign{
    color: <?php echo esc_html($noo_header_logo_font_color); ?>;
}
<?php endif; ?>
<?php if( ! $noo_header_use_image_logo ) : ?>
    .navbar-brand {
    color: <?php echo esc_html($noo_header_logo_font_color); ?>;
    font-family: "<?php echo esc_html($noo_header_logo_font); ?>", sans-serif;
    font-size: <?php echo esc_html($noo_header_logo_font_size) . 'px'; ?>;
    font-style: <?php echo esc_html($noo_header_logo_font_style); ?>;
    font-weight: <?php echo esc_html($noo_header_logo_font_weight); ?>;
    <?php if ( !empty( $noo_header_logo_uppercase ) ) : ?>
        text-transform: uppercase;
    <?php else: ?>
        text-transform: none;
    <?php endif; ?>
    }
<?php else : ?>
    .navbar-brand .noo-logo-img,
    .navbar-brand .noo-logo-retina-img {
    max-height: <?php echo esc_html($noo_header_logo_image_height) . 'px'; ?>;
    }
<?php endif; ?>

/* Mobile Icons */
/* ====================== */
.navbar-toggle, .mobile-minicart-icon {
font-size: <?php echo esc_html($noo_header_nav_toggle_size) . 'px'; ?>;
}