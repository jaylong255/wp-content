jQuery(document).ready(function(){
    jQuery('.add-hours').live('click',function(){
        var $hours = jQuery(this);
        var $id = $hours.attr('data-num');
        var $new_id = parseInt($id) + 1;
        jQuery.ajax({
            type: 'POST',
            url: openhours.url,
            data:({
                action: 'add_tab',
                count: $new_id
            }),
            success: function(data){
                if(data){
                    $hours.attr('data-num',$new_id);
                    jQuery('.open-hours').append(data);
                }
            }
        })
    });
    jQuery('.openhours-remove').live('click',function(){
       jQuery(this).parent().remove();
    });
});