function delete_noo_notica_custom_field(el){
    jQuery(el).closest('tr').remove();
    return false;
}
(function($){
    $(document).ready(function(){

        $(".form-table").sortable({
            'items': 'tbody tr',
            'axis': 'y'
        });

        $('.add').click(function(){
            var table = $('.form-table'),
                n = 0,
                num = table.data('num');

            n = num + 1;
            var tmpl = noopropertyL10n.custom_field_tmpl.replace( /__i__|%i%/g, n );
            table.append(tmpl);
            table.data('num',n);

            $(".form-table").sortable({
                'items': 'tbody tr',
                'axis': 'y'
            });
        });
//        if(jQuery('.vote_select').length > 0){
//        var $offset =   jQuery('.vote_select').offset();
//        var $left_offset = $offset.left;
//        jQuery('.vote_select').mousemove(function(event){
//            var $value = event.pageX - $left_offset + 1;
//            jQuery(this).find('.vote_unit').css({
//                width: $value+"px"
//            });
//            jQuery(this).parent().find('.noo_item_vote').val($value);
//
//
//        });
//        }

    });


})(jQuery);
