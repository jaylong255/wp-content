jQuery(document).ready(function($) {
	
	// --- Check select
		$('#noo_tools').on('click', '.item_tools', function(event) {
			event.preventDefault();
			
			var show = $(this).data('show');
			$('.'+ show).toggle('fast');

		});

	// --- Hover image
		// --- hiden default
			$('#button-1').hide();
		
		// --- Event hover
			$('.noo_hide').on('mouseover', '.item', function(event) {
				event.preventDefault();
				var demo = $(this).data('demo');

				$(this).find('.button-install').show().css({
					background: '#000',
					padding: '5px 10px',
					color: '#fff',
					cursor: 'pointer'
				});

				$(this).find('img').css({
					opacity: '0.7',
					filter: 'alpha(opacity=70)'
				});

				$(this).find('#img-' + demo).css('background', 'rgb(25, 255, 255, 0.8)');
			});

			$('.noo_hide').on('mouseout', '.item', function(event) {
				event.preventDefault();
				$(this).find('.button-install').hide();
				$(this).find('img').css({
					opacity: '1',
					filter: 'alpha(opacity=100)'
				});
			});

	// --- Event click install demo
		$('.theme').on('click', '.install-demo', function(event) {
			event.preventDefault();
			var answer = confirm (nooSetupDemo.notice);
			if (answer) {
				$('#process_import').html( '<img src="'+ nooSetupDemo.img_ajax_load +'" />' + nooSetupDemo.warning ).css('text-align', 'center');
			    var name = $(this).data('name');
			    var data = {
			    	action : 'process_data',
			    	security : nooSetupDemo.ajax_nonce,
			    	name : name
			    };
			    $.post(nooSetupDemo.ajax_url, data, function(response) {
			    	alert( response );
			    	$('#process_import').html('');
			    });
			}
			else {
			    
			}
		});

});