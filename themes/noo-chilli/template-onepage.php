<?php
    /**
     * Template Name: Template One Page
     */
?>
<?php get_header(); ?>
<?php echo noo_custom_page_heading(); ?>
<?php
    if( have_posts() ):
        while( have_posts() ): the_post();

            the_content();

        endwhile;
    endif;
?>
<?php get_footer(); ?>