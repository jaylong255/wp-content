<?php get_header(); ?>

<div class="container-wrap">
	
	<div class="container main-content">
		
		<div class="row">
			
			<div class="col span_12">
				
				<div id="error-404">
					<h1><?php echo esc_html__('404', 'noo-chilli' ) ; ?></h1>
					<h2><?php echo esc_html__('Not Found.', 'noo-chilli' ); ?></h2>
				</div>
				
			</div><!--/span_12-->
			
		</div><!--/row-->
		
	</div><!--/container-->

</div>
<?php get_footer(); ?>

