<?php
/**
 * Default Events Template
 * This file is the basic wrapper template for all the views if 'Default Events Template'
 * is selected in Events -> Settings -> Template -> Events Template.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/default-template.php
 *
 * @package TribeEventsCalendar
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

get_header(); ?>
<?php
$status = noo_get_option( 'noo_page_heading', true );
$bk_img = '' ;
$headerimg = noo_get_option('noo_event_heading_image');
if ( isset($headerimg) && $headerimg !='' ){
    $bk_img = $headerimg;
}

?>
<?php if( $status == true && isset($bk_img) && $bk_img !='' ) : ?>
<div class="noo_header_bk noo_header_has_absolute" style="background-image: url(<?php echo esc_url($bk_img); ?>)">
    <div class="noo-title-wrap invert">
		<div class="noo-title-block">
			<h1 class="noo-title header-title noo-title-medium"><?php echo esc_html(noo_get_option('noo_event_heading_title', 'Events')); ?></h1>
		</div>
        <p class="noo-breadcrumb"><?php if ( function_exists( 'bcn_display' ) ) bcn_display(); ?></p>
	</div>
</div>
<?php endif; ?>
<div class="the-events-wrap">
	<div class="container">
		<?php tribe_events_before_html(); ?>
		<?php tribe_get_view(); ?>
		<?php tribe_events_after_html(); ?>
	</div> <!-- #tribe-events-pg-template -->
</div>
<?php get_footer(); ?>