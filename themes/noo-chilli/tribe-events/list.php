<?php
/**
 * List View Template
 * The wrapper template for a list of events. This includes the Past Events and Upcoming Events views
 * as well as those same views filtered to a specific category.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/list.php
 *
 * @package TribeEventsCalendar
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
} ?>


<?php do_action( 'tribe_events_before_template' ); ?>

<!-- Tribe Bar -->
<?php  tribe_get_template_part( 'modules/bar' ); ?>

<?php
$noo_event_layout = noo_get_option('noo_event_layout');
$noo_event_sidebar = noo_get_option('noo_event_sidebar');
$class_layout = 'col-md-8 pull-left reser-padding-right';
if( $noo_event_layout == 'left_sidebar' ){
    $class_layout = 'col-md-8 reser-padding-right pull-right';
}elseif($noo_event_layout == 'fullwidth'){
    $class_layout = 'col-md-12 pull-right';
}
?>
<div class="row">
    <div class="<?php echo esc_attr($class_layout); ?>">
        <!-- Main Events Content -->
        <?php tribe_get_template_part( 'list/content' ); ?>

        <div class="tribe-clear"></div>
    </div>
    <?php if ( $noo_event_layout !='fullwidth' ) : ?>
    <div class="col-md-4">
        <div class="noo-sidebar-wrap">
            <?php // Dynamic Sidebar
            if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( $noo_event_sidebar ) ) : ?>
                <!-- Sidebar fallback content -->

            <?php endif; // End Dynamic Sidebar sidebar-main ?>
        </div>
    </div>
    <?php endif; ?>
</div>


    <?php do_action( 'tribe_events_after_template' ) ?>
