<?php
/**
 * Single Event Template
 * A single event. This displays the event title, description, meta, and
 * optionally, the Google map for the event.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/single-event.php
 *
 * @package TribeEventsCalendar
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$event_id = get_the_ID();

$noo_event_layout = noo_get_option('noo_event_layout');
$noo_event_sidebar = noo_get_option('noo_event_sidebar');
$class_layout = 'col-md-8 pull-left reser-padding-right';
if( $noo_event_layout == 'left_sidebar' ){
    $class_layout = 'col-md-8 reser-padding-right pull-right';
}elseif($noo_event_layout == 'fullwidth'){
    $class_layout = 'col-sm-8 col-sm-offset-2';
}
?>

<div id="tribe-events-content" class="tribe-events-single vevent hentry">

    <div class="row">
        <div class="<?php echo esc_attr($class_layout); ?>">


            <!-- Notices -->
            <?php tribe_the_notices() ?>

            <?php while ( have_posts() ) :  the_post(); ?>
                <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <!-- Event featured image, but exclude link -->
                   <div class="single-event-header">
                       <?php echo tribe_event_featured_image( $event_id, 'full', false ); ?>
                       <div class="events-meta noo-title-block">
                            <div class="events-meta-bk">
                                <?php the_title( '<h2 class="tribe-events-single-event-title summary entry-title">', '</h2>' ); ?>

                                <div class="tribe-events-schedule updated published tribe-clearfix">
                                    <?php  echo tribe_events_event_schedule_details( $event_id, '<h3><i class="fa fa-clock-o"></i>', '</h3>' ); ?>
                                    <?php if ( tribe_get_cost() ) : ?>
                                        <span class="tribe-events-cost"><i class="fa fa-money"></i><?php echo tribe_get_cost( null, true ) ?></span>
                                    <?php endif; ?>
                                </div>
                            </div>
                       </div>
                   </div>

                    <!-- Event content -->
                    <?php do_action( 'tribe_events_single_event_before_the_content' ) ?>
                    <div class="tribe-events-single-event-description tribe-events-content entry-content description">
                        <?php the_content(); ?>
                    </div>
                    <!-- .tribe-events-single-event-description -->
                    <?php do_action( 'tribe_events_single_event_after_the_content' ) ?>

                    <!-- Event meta -->
                    <?php do_action( 'tribe_events_single_event_before_the_meta' ) ?>
                    <?php
                    /**
                     * The tribe_events_single_event_meta() function has been deprecated and has been
                     * left in place only to help customers with existing meta factory customizations
                     * to transition: if you are one of those users, please review the new meta templates
                     * and make the switch!
                     */
                    if ( ! apply_filters( 'tribe_events_single_event_meta_legacy_mode', false ) ) {
                         tribe_get_template_part( 'modules/meta' );
                    } else {
                        echo tribe_events_single_event_meta();
                    }
                    ?>
                    <?php do_action( 'tribe_events_single_event_after_the_meta' ) ?>
                </div> <!-- #post-x -->
                <?php if ( get_post_type() == Tribe__Events__Main::POSTTYPE && tribe_get_option( 'showComments', false ) ) : ?>
                    <div class="single-comment">
                        <?php if ( comments_open() ) : ?>
                            <?php comments_template( '', true ); ?>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
            <?php endwhile; ?>

            <!-- Event footer -->
            <div id="tribe-events-footer">
                <!-- Navigation -->
                <!-- Navigation -->
                <h3 class="tribe-events-visuallyhidden"><?php _e( 'Event Navigation', 'tribe-events-calendar' ) ?></h3>
                <ul class="tribe-events-sub-nav">
                    <li class="tribe-events-nav-previous"><div class="btn-view-all"><?php tribe_the_prev_event_link( '<span>&laquo; %title%</span>' ) ?></div></li>
                    <li class="tribe-events-nav-next"><div class="btn-view-all"><?php tribe_the_next_event_link( '<span>%title% &raquo;</span>' ) ?></div></li>
                </ul>
                <!-- .tribe-events-sub-nav -->
            </div>
            <!-- #tribe-events-footer -->

        </div>
        <?php if ( $noo_event_layout !='fullwidth' ) : ?>
        <div class="col-md-4">
            <div class="noo-sidebar-wrap">
                <?php // Dynamic Sidebar
                if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'sidebar-main' ) ) : ?>
                    <!-- Sidebar fallback content -->

                <?php endif; // End Dynamic Sidebar sidebar-main ?>
            </div>
        </div>
        <?php endif; ?>
    </div>

</div><!-- #tribe-events-content -->
