<?php
    /**
     * Template Name: Template Full Width
     */
?>
<?php get_header(); ?>
<?php echo noo_custom_page_heading(); ?>
<div class="container-fullwidth">
<?php
    if( have_posts() ):
        while( have_posts() ): the_post();

            the_content();

        endwhile;
    endif;
?>
</div>
<?php get_footer(); ?>