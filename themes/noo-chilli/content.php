<?php
$noo_meta = noo_get_option('noo_blog_show_post_meta',1);
$noo_img = noo_get_option('noo_blog_show_post_image',1);

if(is_single()){
    $noo_single_meta        = noo_get_option('noo_blog_post_show_post_meta',1);
    $noo_single_thumbnail   = noo_get_option('noo_blog_post_show_post_thumbnail',1);
    $noo_meta = $noo_single_meta;
    $noo_img  = $noo_single_thumbnail;
}
?>
<article id="post-<?php the_ID(); ?>" <?php post_class('article-item'); ?>>
    <div class="blog-head">
        <?php if( has_post_thumbnail() && $noo_img == 1): ?>
            <?php if( !is_single() ): ?>
                <div class="entry-thumb">
                    <?php the_post_thumbnail(); ?>
                </div>
            <?php else: ?>
                    <?php if( get_post_format() == 'gallery' ):
                        $gallery     = noo_get_post_meta(get_the_ID(),'_noo_wp_post_gallery');
                    ?>
                        <?php if( isset($gallery) && !empty($gallery) ): ?>
                            <ul class="single-gallery">
                                <?php
                                $gallery_new = explode(',',$gallery);
                                foreach($gallery_new as $gallery_id): ?>
                                    <li>
                                        <?php echo wp_get_attachment_image(esc_attr($gallery_id),'full'); ?>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                    <?php else: ?>
                        <div class="entry-thumb">
                            <?php the_post_thumbnail(); ?>
                        </div>
                    <?php endif; ?>
            <?php endif; ?>
        <?php endif; ?>
    </div>
    <div class="blog-content style-new">
    <?php
    if ( is_single() ) :
        the_title( '<h2 class="entry-title">', '</h2>' );
    else :
        ?>
    <?php if( $noo_meta == 1 ): ?>
    <span class="news-date"><?php echo get_the_date(); ?></span>
    <?php endif; ?>
    <h2 class="entry-title">
        <a href="<?php the_permalink() ?>"><?php the_title() ?></a>
            <?php
            if( is_search() ):
                ?>
                <span class="search-post-type">
                    <?php
                    switch(get_post_type()):
                        case 'page':
                            echo esc_html__('Page', 'noo-chilli' );
                            break;
                        case 'post':
                            echo esc_html__('Blog Post', 'noo-chilli' );
                            break;
                        case 'food_menu':
                            echo esc_html__('Food menu', 'noo-chilli' );
                            break;
                        case 'product':
                            echo esc_html__('Product', 'noo-chilli' );
                            break;
                        default:
                            echo esc_html__('Blog Post', 'noo-chilli' );
                            break;
                    endswitch;
                    ?>
                </span>
            <?php
            endif;
            ?>
        </h2>
    <?php
    endif;
    ?>
    <?php if( $noo_meta == 1 ): ?>
    <div class="entry-meta">
         <span class="noo-cat-link">
             <i class="fa fa-bookmark"></i>
            <?php the_category(',') ?>
        </span>
        <?php if( !is_single() ): ?>
        <span class="noo-tags-link">
             <i class="fa fa-tags"></i>
            <?php the_tags(''); ?>
        </span>
        <?php endif; ?>
    </div>
    <?php endif; ?>
    <div class="entry-content">
        <?php
        if(!is_search()):
            the_content();

            wp_link_pages( array(
                'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'noo-chilli' ) . '</span>',
                'after'       => '</div>',
                'link_before' => '<span>',
                'link_after'  => '</span>',
            ) );
        else:
            the_excerpt();
        endif;
        ?>
    </div><!-- .entry-content -->
    <?php /*if( $noo_meta == 1 ): ?>
        <span class="news-author-cm"><?php echo esc_html__( 'By ', 'noo-chilli' ) . get_the_author() . ', ' . get_comments_number() . esc_html__( ' Comments', 'noo-chilli' ); ?></span>
    <?php endif;*/ ?>
    <?php if( is_single() && get_the_tags() != false ): ?>
    <span class="noo-tags-link">
         <i class="fa fa-tags"></i>
        <?php the_tags(''); ?>
    </span>
    <?php endif; ?>
    </div>
</article><!-- #post-## -->
