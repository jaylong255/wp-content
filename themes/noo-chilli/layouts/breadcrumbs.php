
<?php 
$ltr = ! is_rtl();
?>
<?php if ( ! is_front_page() && noo_get_option( 'noo_breadcrumbs', true ) ) : ?>
	<div class="breadcrumb-wrap">
		<?php noo_the_breadcrumbs(); ?>
	</div>
<?php endif; ?>