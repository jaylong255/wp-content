
<?php

$blog_name		= get_bloginfo( 'name' );
$blog_desc		= get_bloginfo( 'description' );
$image_logo		= '';
$fixed_logo 	= '';
if ( noo_get_option( 'noo_header_use_image_logo', false ) ) {
    $image_logo		= 'noo-logo-normal';
    if ( noo_get_image_option( 'noo_header_logo_image', '' ) !=  '' ) {
        $image_logo = noo_get_image_option( 'noo_header_logo_image', '' );
    }

}

$topbar = noo_get_option( 'noo_header_top_bar', true );
if ( $topbar ) {
    $welcome_text = noo_get_option( 'noo_top_bar_welcome_text', '' );
    $hotline      = noo_get_option( 'noo_top_bar_hotline', '' );
    $email        = noo_get_option( 'noo_top_bar_email', '' );
    $show_search  = noo_get_option( 'noo_top_bar_show_search', true );
}

$nav_class = '';
$nav_class .= noo_get_option( 'noo_header_nav_style', 'relative' ) == 'absolute' ? ' noo-menu-absolute' : '';
$nav_class .= noo_get_option( 'noo_header_nav_position', 'fixed_top' ) == 'static_top' ? ' noo-static-top' : '';

if ( $topbar )
    $nav_class .= ' have-top-bar';

if ( is_page_template('template-onepage.php') )
    $nav_class = 'noo-onepage';
?>

<?php if ( isset($show_search) && $show_search ) : ?>
<div class="top-search">
    <div class="container">
        <?php get_search_form(); ?>
    </div>
</div>
<?php endif; ?>

<header class="noo-header">
    <?php if ( $topbar && !is_page_template('template-onepage.php') ) : ?>
    <div class="top_bar">
        <div class="container">
            <p class="pull-left"><?php echo esc_html( $welcome_text ); ?></p>
            <ul class="pull-right noo-address">
                <?php if ( $hotline != '' ) : ?>
                <li>
                    <i class="fa fa-phone"></i>
                    <a href="#"><?php echo esc_html( $hotline ); ?></a>
                </li>
                <?php endif; ?>
                <?php if ( $email != '' ) : ?>
                <li>
                    <i class="fa fa-envelope"></i>
                    <a href="mailto:<?php echo esc_attr( $email ); ?>?Subject=<?php echo esc_attr( $blog_name); ?>"><?php echo esc_html( $email ); ?></a>
                </li>
                <?php endif; ?>
                <?php if ( $show_search ) : ?>
                <li>
                    <i class="fa fa-search tz-icon-search"></i>
                </li>
                <?php endif; ?>

            </ul>
        </div>
    </div>
    <?php endif; ?>
    
    <div class="noo-box-header <?php echo esc_attr( $nav_class ); ?>">
        <div class="container container-relative">
            <div class="navbar-wrapper">
                <div class="navbar navbar-default" role="navigation">
                    <div class="navbar-header">
                        <?php if ( is_front_page() ) : echo '<h1 class="sr-only">' . esc_html($blog_name) . '</h1>'; endif; ?>
                        <button data-target=".nav-collapse" class="btn-navbar noo_icon_menu" type="button">
                            <i class="fa fa-bars"></i>
                        </button>
                        <a href="<?php echo home_url( '/' ); ?>" class="navbar-brand" title="<?php echo esc_attr($blog_desc); ?>">
                            <?php echo ( $image_logo == '' ) ? esc_html($blog_name) : '<img class="noo-logo-img noo-logo-normal" src="' . esc_url($image_logo) . '" alt="' . esc_attr($blog_desc) . '">'; ?>
                        </a>
                    </div> <!-- / .nav-header -->
                    <div class="bs-navbar-collapse collapse">
                        <nav id="noo-nav" class="pull-right navbar-primary">
                            <?php

                            $menu =  is_page_template('template-onepage.php') ? 'onepage' : 'primary';

                            if ( has_nav_menu( $menu ) ) :
                                wp_nav_menu( array(
                                    'theme_location' => $menu,
                                    'container'      => false,
                                    'menu_class'     => 'navbar-nav nav-collapse noo-menu sf-menu',
                                    'walker'         => new noo_megamenu_walker
                                ) );
                            else :
                                echo '<ul class="navbar-nav nav"><li><a class="no-menu-assign" href="' . admin_url( 'nav-menus.php' ) . '">' . esc_html__( 'No menu assigned!', 'noo-chilli' ) . '</a></li></ul>';
                            endif;
                            
                            ?>
                        </nav> <!-- /.navbar-collapse -->
                        <div class="clear"></div>
                    </div>
                </div> <!-- / .navbar-default -->
            </div>
        </div>
    </div> <!-- / .noo-box-header -->
    <div class="rest-nav"></div>
</header>