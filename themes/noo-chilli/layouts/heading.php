<?php

$blog_name		= get_bloginfo( 'name' );
$blog_desc		= get_bloginfo( 'description' );
$image_logo		= '';
$retina_logo	= '';
$page_logo		= '';


?>

<header class="noo-header noo-header-fix">
    <div class="container">
        <div class="navbar-header pull-left">
            <button data-target=".noo-navbar-collapse" data-toggle="collapse" type="button" class="navbar-toggle collapsed">
                <i class="fa fa-bars"></i>
            </button>
            <h3 class="noo-logo">
                <a href="<?php echo home_url(); ?>">
                    <?php
                    if ( noo_get_option( 'noo_header_use_image_logo', false ) ) {

                        $image_logo = noo_get_image_option( 'noo_header_logo_image', '' );
                        $retina_logo = noo_get_image_option( 'noo_header_logo_retina_image', $image_logo );
                        ?>
                        <img class="noo-logo-img" src="<?php echo esc_attr($image_logo) ?>" alt="<?php echo esc_attr($blog_name); ?>">
                    <?php
                    }else{
                        echo  esc_html(noo_get_option('blogname'));
                    }
                    ?>

                </a>
            </h3>
        </div>
        <nav class="collapse navbar-collapse noo-navbar-collapse">
            <?php
            if ( has_nav_menu( 'primary' ) ) :
                wp_nav_menu( array(
                    'theme_location' => 'primary',
                    'container'      => false,
                    'menu_class'     => 'pull-left noo-menu navbar-nav sf-menu'
                ) );
            else :
                echo '<ul class="navbar-nav nav"><li><a href="' . home_url( '/' ) . 'wp-admin/nav-menus.php">' . esc_html__( 'No menu assigned!', 'noo-chilli' ) . '</a></li></ul>';
            endif;
            ?>
            <?php noo_social_icons(); ?>
        </nav>
    </div>
</header>