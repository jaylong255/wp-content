<?php

class Noo_Tweets extends WP_Widget {

    public function __construct() {
        parent::__construct(
            'dh_tweets',  // Base ID
            'Recent Tweets',  // Name
            array( 'classname' => 'tweets-widget', 'description' => esc_html__( 'Display recent tweets', 'noo-chilli' ) ) );
    }

    public function widget( $args, $instance ) {
        extract( $args );
        if ( ! empty( $instance['title'] ) ) {
            $title = apply_filters( 'widget_title', $instance['title'] );
        }
        echo $before_widget;
        if ( ! empty( $title ) ) {
            echo $before_title . $title . $after_title;
        }

        // check settings and die if not set
        if ( empty( $instance['consumerkey'] ) || empty( $instance['consumersecret'] ) || empty(
            $instance['accesstoken'] ) || empty( $instance['accesstokensecret'] ) || empty( $instance['cachetime'] ) ||
            empty( $instance['username'] ) ) {
            echo '<strong>' . esc_html__( 'Please fill all widget settings!', 'noo-chilli' ) . '</strong>' . $after_widget;
            return;
        }

        $dh_widget_recent_tweets_cache_time = get_option( 'dh_widget_recent_tweets_cache_time' );
        $diff = time() - $dh_widget_recent_tweets_cache_time;

        $crt = (int) $instance['cachetime'] * 3600;

        if($diff >= $crt || empty($dh_widget_recent_tweets_cache_time)){

            if( ! class_exists('TwitterOAuth' ) ) {
                echo '<strong>' . esc_html__( 'Couldn\'t find twitteroauth.php!', 'noo-chilli' ) . '</strong>' . $after_widget;
                return;
            }

            function getConnectionWithAccessToken( $cons_key, $cons_secret, $oauth_token, $oauth_token_secret ) {
                $connection = new TwitterOAuth( $cons_key, $cons_secret, $oauth_token, $oauth_token_secret );
                return $connection;
            }

            $connection = getConnectionWithAccessToken(
                $instance['consumerkey'],
                $instance['consumersecret'],
                $instance['accesstoken'],
                $instance['accesstokensecret'] );
            $tweets = $connection->get(
                "https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=" . $instance['username'] .
                "&count=10&exclude_replies=" . $instance['excludereplies'] );

            if ( ! empty( $tweets->errors ) ) {
                if ( $tweets->errors[0]->message == 'Invalid or expired token' ) {
                    echo '<strong>' . $tweets->errors[0]->message . '!</strong><br/>' . esc_html__(
                            'You\'ll need to regenerate it <a href="https://dev.twitter.com/apps" target="_blank">here</a>!',
                            'noo-chilli' ) . $after_widget;
                } else {
                    echo '<strong>' . $tweets->errors[0]->message . '</strong>' . $after_widget;
                }
                return;
            }

            $tweets_array = array();
            for ( $i = 0; $i <= count( $tweets ); $i++ ) {
                if ( ! empty( $tweets[$i] ) ) {
                    $tweets_array[$i]['created_at'] = $tweets[$i]->created_at;
                    $tweets_array[$i]['name']	=	$tweets[$i]->user->name;
                    $tweets_array[$i]['screen_name'] = $tweets[$i]->user->screen_name;
                    $tweets_array[$i]['profile_image_url'] = $tweets[$i]->user->profile_image_url;
                    // clean tweet text
                    $tweets_array[$i]['text'] = preg_replace( '/[\x{10000}-\x{10FFFF}]/u', '', $tweets[$i]->text );

                    if ( ! empty( $tweets[$i]->id_str ) ) {
                        $tweets_array[$i]['status_id'] = $tweets[$i]->id_str;
                    }
                }
            }
            update_option( 'dh_widget_recent_tweets', serialize( $tweets_array ) );
            update_option( 'dh_widget_recent_tweets_cache_time', time() );
        }

        $dh_widget_recent_tweets = maybe_unserialize( get_option( 'dh_widget_recent_tweets' ) );
        if ( ! empty( $dh_widget_recent_tweets ) ) {
            echo '<div class="recent-tweets"><ul>';
            $i = '1';
            foreach ( $dh_widget_recent_tweets as $tweet ) {

                if ( ! empty( $tweet['text'] ) ) {
                    if ( empty( $tweet['status_id'] ) ) {
                        $tweet['status_id'] = '';
                    }
                    if ( empty( $tweet['created_at'] ) ) {
                        $tweet['created_at'] = '';
                    }

                    echo '<li><div class="twitter_user"><a class="twitter_profile" target="_blank" href="http://twitter.com/' .
                        $instance['username'] . '/statuses/' . $tweet['status_id'] . '"><img src="'.$tweet['profile_image_url'].'" alt="'. $tweet['name'] .'">'. $tweet['name'] .'</a><span class="twitter_username">@'.$tweet['screen_name'].'</span></div><span class="tw-content">' . $this->_convert_links( $tweet['text'] ) .
                        '</span></li>';
                    if ( $i == $instance['tweetstoshow'] ) {
                        break;
                    }
                    $i++;
                }
            }

            echo '</ul></div>';
        }

        echo $after_widget;
    }

    protected function _convert_links( $status, $targetBlank = true, $linkMaxLen = 50 ) {
        // the target
        $target = $targetBlank ? " target=\"_blank\" " : "";

        // convert link to url
        $status = preg_replace(
            "/((http:\/\/|https:\/\/)[^ )]+)/i",
            "<a href=\"$1\" title=\"$1\" $target >$1</a>",
            $status );

        // convert @ to follow
        $status = preg_replace(
            "/(@([_a-z0-9\-]+))/i",
            "<a href=\"http://twitter.com/$2\" class=\"quick_anchor\" title=\"Follow $2\" $target >$1</a>",
            $status );

        // convert # to search
        $status = preg_replace(
            "/(#([_a-z0-9\-]+))/i",
            "<a href=\"https://twitter.com/search?q=$2\" class=\"quick_hashtag\" title=\"Search $1\" $target >$1</a>",
            $status );

        // return the status
        return $status;
    }

    protected function _relative_time( $a = '' ) {
        // get current timestampt
        $b = strtotime( "now" );
        // get timestamp when tweet created
        $c = strtotime( $a );
        // get difference
        $d = $b - $c;
        // calculate different time values
        $minute = 60;
        $hour = $minute * 60;
        $day = $hour * 24;
        $week = $day * 7;

        if ( is_numeric( $d ) && $d > 0 ) {
            // if less then 3 seconds
            if ( $d < 3 )
                return "right now";
            // if less then minute
            if ( $d < $minute )
                return sprintf( esc_html__( "%s seconds ago", 'noo-chilli' ), floor( $d ) );
            // if less then 2 minutes
            if ( $d < $minute * 2 )
                return esc_html__( "about 1 minute ago", 'noo-chilli' );
            // if less then hour
            if ( $d < $hour )
                return sprintf( esc_html__( '%s minutes ago', 'noo-chilli' ), floor( $d / $minute ) );
            // if less then 2 hours
            if ( $d < $hour * 2 )
                return esc_html__( "about 1 hour ago", 'noo-chilli' );
            // if less then day
            if ( $d < $day )
                return sprintf( esc_html__( "%s hours ago", 'noo-chilli' ), floor( $d / $hour ) );
            // if more then day, but less then 2 days
            if ( $d > $day && $d < $day * 2 )
                return esc_html__( "yesterday", 'noo-chilli' );
            // if less then year
            if ( $d < $day * 365 )
                return sprintf( esc_html__( '%s days ago', 'noo-chilli' ), floor( $d / $day ) );
            // else return more than a year
            return esc_html__( "over a year ago", 'noo-chilli' );
        }
    }

    public function form( $instance ) {
        $defaults = array(
            'title' => '',
            'consumerkey' => '',
            'consumersecret' => '',
            'accesstoken' => '',
            'accesstokensecret' => '',
            'cachetime' => '',
            'username' => '',
            'tweetstoshow' => '' );
        $instance = wp_parse_args( (array) $instance, $defaults );

        echo '
		<p>
			<label>' . esc_html__( 'Title', 'noo-chilli' ) . ':</label>
			<input type="text" name="' .
            $this->get_field_name( 'title' ) . '" id="' . $this->get_field_id( 'title' ) . '" value="' .
            esc_attr( $instance['title'] ) . '" class="widefat" />
		</p>
		<p>
			<label>' . esc_html__( 'Consumer Key', 'noo-chilli' ) . ':</label>
			<input type="text" name="' .
            $this->get_field_name( 'consumerkey' ) . '" id="' . $this->get_field_id( 'consumerkey' ) . '" value="' .
            esc_attr( $instance['consumerkey'] ) . '" class="widefat" />
		</p>
		<p>
			<label>' . esc_html__( 'Consumer Secret', 'noo-chilli' ) . ':</label>
			<input type="text" name="' .
            $this->get_field_name( 'consumersecret' ) . '" id="' . $this->get_field_id( 'consumersecret' ) . '" value="' .
            esc_attr( $instance['consumersecret'] ) . '" class="widefat" />
		</p>
		<p>
			<label>' . esc_html__( 'Access Token', 'noo-chilli' ) . ':</label>
			<input type="text" name="' .
            $this->get_field_name( 'accesstoken' ) . '" id="' . $this->get_field_id( 'accesstoken' ) . '" value="' .
            esc_attr( $instance['accesstoken'] ) . '" class="widefat" />
		</p>
		<p>
			<label>' . esc_html__( 'Access Token Secret', 'noo-chilli' ) . ':</label>
			<input type="text" name="' .
            $this->get_field_name( 'accesstokensecret' ) . '" id="' . $this->get_field_id( 'accesstokensecret' ) .
            '" value="' . esc_attr( $instance['accesstokensecret'] ) . '" class="widefat" />
		</p>
		<p>
			<label>' .
            esc_html__( 'Cache Tweets in every', 'noo-chilli' ) . ':</label>
			<input type="text" name="' .
            $this->get_field_name( 'cachetime' ) . '" id="' . $this->get_field_id( 'cachetime' ) . '" value="' .
            esc_attr( $instance['cachetime'] ) . '" class="small-text" />' . esc_html__( 'hours', 'noo-chilli' ) . '
		</p>
		<p>
			<label>' . esc_html__( 'Twitter Username', 'noo-chilli' ) . ':</label>
			<input type="text" name="' .
            $this->get_field_name( 'username' ) . '" id="' . $this->get_field_id( 'username' ) . '" value="' .
            esc_attr( $instance['username'] ) . '" class="widefat" />
		</p>
		<p>
			<label>' . esc_html__( 'Tweets to display', 'noo-chilli' ) . ':</label>
			<select type="text" name="' .
            $this->get_field_name( 'tweetstoshow' ) . '" id="' . $this->get_field_id( 'tweetstoshow' ) . '">';
        $i = 1;
        for ( $i; $i <= 10; $i++ ) {
            echo '<option value="' . $i . '"';
            if ( $instance['tweetstoshow'] == $i ) {
                echo ' selected="selected"';
            }
            echo '>' . $i . '</option>';
        }
        echo '
			</select>
		</p>
		<p>
			<label>' . esc_html__( 'Exclude replies', 'noo-chilli' ) . ':</label>
			<input type="checkbox" name="' .
            $this->get_field_name( 'excludereplies' ) . '" id="' . $this->get_field_id( 'excludereplies' ) .
            '" value="true"';
        if ( ! empty( $instance['excludereplies'] ) && esc_attr( $instance['excludereplies'] ) == 'true' ) {
            echo ' checked="checked"';
        }
        echo '/></p>';
    }

    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = strip_tags( $new_instance['title'] );
        $instance['consumerkey'] = strip_tags( $new_instance['consumerkey'] );
        $instance['consumersecret'] = strip_tags( $new_instance['consumersecret'] );
        $instance['accesstoken'] = strip_tags( $new_instance['accesstoken'] );
        $instance['accesstokensecret'] = strip_tags( $new_instance['accesstokensecret'] );
        $instance['cachetime'] = strip_tags( $new_instance['cachetime'] );
        $instance['username'] = strip_tags( $new_instance['username'] );
        $instance['tweetstoshow'] = strip_tags( $new_instance['tweetstoshow'] );
        $instance['excludereplies'] = strip_tags( $new_instance['excludereplies'] );

        if ( $old_instance['username'] != $new_instance['username'] ) {
            delete_option( 'dh_widget_recent_tweets_cache_time' );
        }

        return $instance;
    }
}

register_widget( 'Noo_Tweets' );
class WP_Widget_Recent_News extends WP_Widget {

	public function __construct() {
		$widget_ops = array('classname' => 'widget_recent_news', 'description' => esc_html__( "Your site&#8217;s most recent Posts.", 'noo-chilli' ) );
		parent::__construct('recent-news', esc_html__('Recent News', 'noo-chilli' ), $widget_ops);
		$this->alt_option_name = 'widget_recent_news';

		add_action( 'save_post', array($this, 'flush_widget_cache') );
		add_action( 'deleted_post', array($this, 'flush_widget_cache') );
		add_action( 'switch_theme', array($this, 'flush_widget_cache') );
	}

	public function widget($args, $instance) {
		$cache = array();
		if ( ! $this->is_preview() ) {
			$cache = wp_cache_get( 'widget_recent_news', 'widget' );
		}

		if ( ! is_array( $cache ) ) {
			$cache = array();
		}

		if ( ! isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo $cache[ $args['widget_id'] ];
			return;
		}

		ob_start();

		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : esc_html__( 'Recent News', 'noo-chilli' );

		/** This filter is documented in wp-includes/default-widgets.php */
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		$number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 5;
		if ( ! $number )
			$number = 5;
		$show_date = isset( $instance['show_date'] ) ? $instance['show_date'] : false;

		/**
		 * Filter the arguments for the Recent Posts widget.
		 *
		 * @since 3.4.0
		 *
		 * @see WP_Query::get_posts()
		 *
		 * @param array $args An array of arguments used to retrieve the recent posts.
		 */
		$r = new WP_Query( apply_filters( 'widget_news_args', array(
			'posts_per_page'      => $number,
			'no_found_rows'       => true,
			'post_status'         => 'publish',
			'ignore_sticky_posts' => true
		) ) );

		if ($r->have_posts()) :
?>
		<?php echo $args['before_widget']; ?>
		<?php if ( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		} ?>
		<ul>
		<?php while ( $r->have_posts() ) : $r->the_post(); ?>
			<li>
			 <?php if ( has_post_thumbnail() ):
	            the_post_thumbnail(array(70, 70));
	        else: ?>
	            <img src="<?php echo NOO_ASSETS_URI.'/images/no-image-widget.jpg' ; ?>" alt="<?php the_title(); ?>" />
	        <?php endif;  ?>
            <div class="recent-right">
				<a href="<?php the_permalink(); ?>"><?php get_the_title() ? the_title() : the_ID(); ?></a>
                <?php if ( $show_date ) : ?>
                    <span class="post-date"><?php echo get_the_date(); ?></span>
                <?php endif; ?>
            </div>
			</li>
		<?php endwhile; ?>
		</ul>
		<?php echo $args['after_widget']; ?>
<?php
		// Reset the global $the_post as this query will have stomped on it
		wp_reset_postdata();

		endif;

		if ( ! $this->is_preview() ) {
			$cache[ $args['widget_id'] ] = ob_get_flush();
			wp_cache_set( 'widget_recent_news', $cache, 'widget' );
		} else {
			ob_end_flush();
		}
	}

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['number'] = (int) $new_instance['number'];
		$instance['show_date'] = isset( $new_instance['show_date'] ) ? (bool) $new_instance['show_date'] : false;
		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset($alloptions['widget_recent_entries']) )
			delete_option('widget_recent_entries');

		return $instance;
	}

	public function flush_widget_cache() {
		wp_cache_delete('widget_recent_news', 'widget');
	}

	public function form( $instance ) {
		$title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$number    = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
		$show_date = isset( $instance['show_date'] ) ? (bool) $instance['show_date'] : false;
?>
		<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'noo-chilli' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></p>

		<p><label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of posts to show:', 'noo-chilli' ); ?></label>
		<input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo esc_attr($number); ?>" size="3" /></p>

		<p><input class="checkbox" type="checkbox" <?php checked( $show_date ); ?> id="<?php echo $this->get_field_id( 'show_date' ); ?>" name="<?php echo $this->get_field_name( 'show_date' ); ?>" />
		<label for="<?php echo $this->get_field_id( 'show_date' ); ?>"><?php _e( 'Display post date?', 'noo-chilli' ); ?></label></p>
<?php
	}
}

register_widget('WP_Widget_Recent_News');


if( !class_exists('Noo_Constru_About') ):
    class Noo_Constru_About extends WP_Widget{

        public function __construct(){
            $widget_option = array('classname'  =>  'widget_about','description' => esc_html__('Display about', 'noo-chilli' ));
            parent::__construct('constru-about',esc_html__('Noo About', 'noo-chilli' ), $widget_option);
            add_action('admin_enqueue_scripts', array($this, 'register_js'));
        }

        public function register_js(){

            wp_enqueue_media();
            wp_register_script('upload_img', NOO_FRAMEWORK_ADMIN_URI . '/assets/js/upload_img.js', false, false, $in_footer=true);
            wp_enqueue_script('upload_img');

        }

        // method for front-end
        public function widget( $args, $instance ){
            extract( $args );
            extract( $instance );
            echo $before_widget;
            ?>
            <div class="noo_about_widget">
                <a href="<?php echo esc_url($link); ?>">
                    <img src="<?php echo esc_url($image); ?>" alt="<?php bloginfo('title'); ?>">
                </a>
                <h3 class="about-title"><?php echo esc_html($title); ?></h3>
                <p>
                    <?php echo esc_html($description); ?>
                </p>
            </div>
            <?php
            echo $after_widget;
        }

        // method widget form
        public function form( $instance ){

            $instance = wp_parse_args( $instance, array(
                'title'  =>  'Upload logo',
                'link'   =>  '',
                'image'  =>  '',
                'description'  =>  ''
            ) );
            extract($instance);
            ?>
            <p>
                <label for="<?php echo $this -> get_field_id('title'); ?>"><?php echo _e('Title:', 'noo-chilli' ); ?></label>
                <input type="text" id="<?php echo $this -> get_field_id('title'); ?>" name="<?php echo $this -> get_field_name('title') ?>" class="widefat" value="<?php echo esc_attr($title); ?>">
            </p>
            <p>
                <label for="<?php echo $this -> get_field_id('link'); ?>"><?php echo _e('Link Image:', 'noo-chilli' ); ?></label>
                <input type="text" id="<?php echo $this -> get_field_id('link'); ?>" name="<?php echo $this -> get_field_name('link') ?>" class="widefat" value="<?php echo esc_attr($link); ?>">
            </p>
            <p>
                <label for="<?php echo $this -> get_field_id('image'); ?>"><?php echo _e('Image', 'noo-chilli' ) ; ?></label>
                <input class="widefat" type="text" name="<?php echo $this -> get_field_name('image'); ?>" id="<?php echo $this -> get_field_id('image') ; ?>" value="<?php echo esc_attr($image); ?>">
                <a href="#" class="noo_upload_button button" rel="image"><?php echo _e('Upload', 'noo-chilli' ) ?></a>
            </p>
            <p>
                <label for="<?php echo $this -> get_field_id('description'); ?>"><?php echo _e('Description', 'noo-chilli' ) ; ?></label>
                <textarea name="<?php echo $this -> get_field_name('description'); ?>" id="<?php echo $this -> get_field_id('description') ; ?>" cols="10" rows="5" class="widefat"><?php echo esc_attr($description); ?></textarea>
            </p>


        <?php
        }

        // method update
        public function update( $new_instance, $old_instance ){
            $instance                 =   $old_instance;
            $instance['title']        =   $new_instance['title'];
            $instance['link']         =   $new_instance['link'];
            $instance['image']        =   $new_instance['image'];
            $instance['description']  =   $new_instance['description'];
            return $instance;
        }

    }
    register_widget('Noo_Constru_About');
endif; // endif check class

/*widget social*/

class Noo_Social  extends WP_Widget {

    /* *
    * Register widget with WordPress.
    * parent user function class father
    */
    function  __construct() {
        parent::__construct(
            'noo_social', // Base Id
            esc_html__('Noo Social', 'noo-chilli' ), // NAME
            array('description' => esc_html__('Display soical', 'noo-chilli' )) // args
        ) ;
    }

    /**
     * Front-end display of widget
     */
    public function widget( $args, $instance ) {
        extract($args);
        $title = apply_filters('widget_title', $instance['title']);
        echo $before_widget ;
        if ( $title ) :
            echo $before_title.$title.$after_title ;
        endif;
        $arg_social = array(
            array('id'       =>  'facebook'),
            array('id'       =>  'google-plus'),
            array('id'       =>  'twitter'),
            array('id'       =>  'youtube'),
            array('id'       =>  'skype'),
            array('id'       =>  'linkedin'),
            array('id'       =>  'dribbble'),
            array('id'       =>  'pinterest'),
            array('id'       =>  'flickr')

        ) ;
        ?>
        <div class="noo_social">
            <div class="noo-copyright">
                <?php echo noo_html_content_filter($instance['description']) ?>
            </div>
            <div class="social-all">
                <?php
                foreach($arg_social as $social):
                    if (!empty($instance[$social['id']])):
                        ?>
                        <a href="<?php echo esc_url($instance[$social['id']]); ?>" class="fa fa-<?php echo esc_attr($social['id']); ?>"></a>
                    <?php
                    endif;
                endforeach;
                ?>
            </div>
        </div>
        <?php
        echo $after_widget ;


    }

    /**
     * Back-end widget form
     */
    public function  form($instrance) {
        // wp_parse_args : set default values
        $instrance = wp_parse_args( $instrance, array(
            'title'       =>  'NooTheme',
            'description' =>  '',
            'facebook'    =>  '',
            'google-plus' =>  '',
            'twitter'     =>  '',
            'youtube'     =>  '',
            'skype'       =>  '',
            'linkedin'    =>  '',
            'dribbble'    =>  '',
            'pinterest'   =>  '',
            'flickr'      =>  ''

        ) );
        ?>
        <p>
            <label for="<?php echo $this ->  get_field_id('title'); ?>">
                <?php _e('Title', 'noo-chilli' ) ; ?>
            </label>
            <br>
            <input type="text" name="<?php echo $this -> get_field_name('title') ; ?>" id="<?php echo $this -> get_field_id('title'); ?>" class="widefat" value="<?php echo esc_html($instrance['title']); ?>">
        </p>
        <p>
            <label for="<?php echo $this -> get_field_id('description'); ?>"><?php _e('Description', 'noo-chilli' ) ; ?></label>
            <textarea name="<?php echo $this -> get_field_name('description'); ?>" id="<?php echo $this -> get_field_id('description') ; ?>" cols="10" rows="5" class="widefat"><?php echo esc_attr($instrance['description']); ?></textarea>
        </p>
        <p>
            <label for="<?php echo $this -> get_field_id('facebook') ?>" >
                <?php _e('Facebook', 'noo-chilli' ) ; ?>
            </label>
            <br>
            <input type="text" name="<?php echo $this -> get_field_name('facebook') ; ?>" id="<?php echo $this -> get_field_id('facebook'); ?>" class="widefat" value="<?php echo esc_attr($instrance['facebook']); ?>">
        </p>
        <p>
            <label for="<?php echo $this -> get_field_id('google-plus') ?>">
                <?php _e('Google', 'noo-chilli' ) ; ?>
            </label>
            <br>
            <input type="text" name="<?php echo $this -> get_field_name('google-plus') ; ?>" id="<?php echo $this -> get_field_id('google-plus'); ?>" class="widefat" value="<?php echo esc_attr($instrance['google-plus']); ?>">
        </p>
        <p>
            <label for="<?php echo $this -> get_field_id('twitter') ?>">
                <?php _e('Twitter', 'noo-chilli' ) ; ?>
            </label>
            <br>
            <input type="text" name="<?php echo $this -> get_field_name('twitter') ; ?>" id="<?php echo $this -> get_field_id('twitter'); ?>" class="widefat" value="<?php echo esc_attr($instrance['twitter']); ?>">
        </p>
        <p>
            <label for="<?php echo $this -> get_field_id('youtube') ?>">
                <?php _e('Youtube', 'noo-chilli' ) ; ?>
            </label>
            <br>
            <input type="text" name="<?php echo $this -> get_field_name('youtube') ; ?>" id="<?php echo $this -> get_field_id('youtube'); ?>" class="widefat" value="<?php echo esc_attr($instrance['youtube']); ?>">
        </p>
        <p>
            <label for="<?php echo $this -> get_field_id('skype'); ?>">
                <?php  _e('Skype', 'noo-chilli' ) ; ?>
            </label>
            <br>
            <input type="text" name="<?php echo $this -> get_field_name('skype') ; ?>" id="<?php echo $this -> get_field_id('skype'); ?>" class="widefat" value="<?php echo esc_attr($instrance['skype']); ?>">
        </p>
        <p>
            <label for="<?php echo $this -> get_field_id('linkedin') ?>">
                <?php _e('linkedin', 'noo-chilli' ) ; ?>
            </label>
            <br>
            <input type="text" name="<?php echo $this -> get_field_name('linkedin') ; ?>" id="<?php echo $this -> get_field_id('linkedin'); ?>" class="widefat" value="<?php echo esc_attr($instrance['linkedin']); ?>">
        </p>
        <p>
            <label for="<?php echo $this -> get_field_id('dribbble') ?>">
                <?php _e('Dribbble', 'noo-chilli' ) ; ?>
            </label>
            <br>
            <input type="text" name="<?php echo $this -> get_field_name('dribbble') ; ?>" id="<?php echo $this -> get_field_id('dribbble'); ?>" class="widefat" value="<?php echo esc_attr($instrance['dribbble']); ?>">
        </p>
        <p>
            <label for="<?php echo $this -> get_field_id('pinterest') ?>">
                <?php _e('Pinterest', 'noo-chilli' ) ; ?>
            </label>
            <br>
            <input type="text" name="<?php echo $this -> get_field_name('pinterest') ; ?>" id="<?php echo $this -> get_field_id('pinterest'); ?>" class="widefat" value="<?php echo esc_attr($instrance['pinterest']); ?>">
        </p>
        <p>
            <label for="<?php echo $this -> get_field_id('flickr') ?>">
                <?php _e('Flickr', 'noo-chilli' ) ; ?>
            </label>
            <br>
            <input type="text" name="<?php echo $this -> get_field_name('flickr') ; ?>" id="<?php echo $this -> get_field_id('flickr'); ?>" class="widefat" value="<?php echo esc_attr($instrance['flickr']); ?>">
        </p>

    <?php
    }

    /* *
     * Method update
     */
    function update( $new_instance, $old_instance ) {
        $instance = array() ;
        $instance['title']     = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['description']     = $new_instance['description'] ;
        $instance['facebook']  = ( ! empty( $new_instance['facebook'] ) ) ? strip_tags( $new_instance['facebook'] ) : ''  ;
        $instance['google-plus']    = ( ! empty( $new_instance['google-plus'] ) ) ? strip_tags( $new_instance['google-plus'] ) : ''  ;
        $instance['twitter']   = ( ! empty( $new_instance['twitter'] ) ) ? strip_tags( $new_instance['twitter'] ) : ''  ;
        $instance['youtube']   = ( ! empty( $new_instance['youtube'] ) ) ? strip_tags( $new_instance['youtube'] ) : ''  ;
        $instance['skype']     = ( ! empty( $new_instance['skype'] ) ) ? strip_tags( $new_instance['skype'] ) : ''  ;
        $instance['linkedin']  = ( ! empty( $new_instance['linkedin'] ) ) ? strip_tags( $new_instance['linkedin'] ) : ''  ;
        $instance['dribbble']  = ( ! empty( $new_instance['dribbble'] ) ) ? strip_tags( $new_instance['dribbble'] ) : ''  ;
        $instance['pinterest'] = ( ! empty( $new_instance['pinterest'] ) ) ? strip_tags( $new_instance['pinterest'] ) : ''  ;
        $instance['flickr']    = ( ! empty( $new_instance['flickr'] ) ) ? strip_tags( $new_instance['flickr'] ) : ''  ;
        return $instance ;
    }

}
register_widget('Noo_Social');


if( !class_exists('Noo_Location') ):

    class Noo_Location extends WP_Widget{

        public function __construct(){
            parent::__construct(
                'noo_location',
                'Noo Location',
                array('description' => 'Noo Location')
            );
        }
        public function widget($args, $instance) {
            // Get menu
            $location = get_categories( array( 'taxonomy'  => 'portfolio_location' ) );



            if ( !$location )
                return;

            /** This filter is documented in wp-includes/default-widgets.php */
            $instance['title'] = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );

            echo $args['before_widget'];

            if ( !empty($instance['title']) )
                echo $args['before_title'] . $instance['title'] . $args['after_title'];


            if( isset($location) && !empty($location) ):
                echo '<div class="constru-wrap-menu">';
                $i = 0;
                foreach ( (array) $location as $location_item ) {
                    if( $i++ % 5 == 0 ): ?>
                    <ul class="noo-custom-menu">
                    <?php endif; ?>
                            <li><a href="<?php echo esc_url(get_term_link($location_item)) ?>"><?php echo esc_html($location_item->name); ?></a></li>
                    <?php if( $i % 5 == 0 || $i == count($location)  ): ?>
                    </ul>
                    <?php endif;
                }
                echo '</div>';
            endif;



            echo $args['after_widget'];
        }

        public function update( $new_instance, $old_instance ) {
            $instance = array();
            if ( ! empty( $new_instance['title'] ) ) {
                $instance['title'] = strip_tags( stripslashes($new_instance['title']) );
            }
            return $instance;
        }

        public function form( $instance ) {
            $title = isset( $instance['title'] ) ? $instance['title'] : ''; ?>
            <p>
                <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'noo-chilli' ) ?></label>
                <input type="text" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo esc_attr($title); ?>" />
            </p>
        <?php
        }

    }
    register_widget('Noo_Location');

endif;
if( !class_exists('Noo_Events') ):

    class Noo_Events extends WP_Widget{

        public function __construct(){
            parent::__construct(
                'noo_events',
                'Noo Events',
                array('description' => 'Noo Events')
            );
        }
        public function widget($args, $instance) {

            extract( $args );
            /** This filter is documented in wp-includes/default-widgets.php */
            $instance['title'] = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );

            echo $before_widget;

            if ( !empty($instance['title']) )
                echo $args['before_title'] . $instance['title'] . $args['after_title'];
            $args = array(
                'post_type'         => 'tribe_events',
                'orderby'           =>  'date',
                'order'             =>  'desc',
                'posts_per_page'    =>  $instance['limit']
            );
            $query = new WP_Query( $args );
            if( $query->have_posts() ):
                echo'<ul class="noo_widget_events">';
                while( $query->have_posts() ):
                    $query->the_post();
                    ?>
                        <li>
                            <?php the_post_thumbnail('noo-thumbnail-square'); ?>
                            <div class="event-widget-info">
                                <h4><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h4>
                                <div class="date">
                                    <?php if( function_exists('tribe_events_event_schedule_details') ):  echo tribe_events_event_schedule_details( get_the_ID()); endif; ?>
                                </div>
                            </div>
                        </li>
                    <?php
                endwhile;
                echo'</ul>';
            endif;
            wp_reset_postdata();


            echo $after_widget;
        }

        public function form( $instance ){
            $instance = wp_parse_args( $instance, array(
                'title'     =>  'Noo Events',
                'limit'   =>  ''
            ) );
            extract($instance);
            ?>
            <p>
                <label for="<?php echo $this -> get_field_id('title'); ?>"><?php echo _e('Title:', 'noo-chilli' ); ?></label>
                <input type="text" id="<?php echo $this -> get_field_id('title'); ?>" name="<?php echo $this -> get_field_name('title') ?>" class="widefat" value="<?php echo esc_attr($title); ?>">
            </p>

            <p>
                <label for="<?php echo $this -> get_field_id('limit'); ?>"><?php echo _e('Limit:', 'noo-chilli' ); ?></label>
                <input type="text" id="<?php echo $this -> get_field_id('limit'); ?>" name="<?php echo $this -> get_field_name('limit') ?>" class="widefat" value="<?php echo esc_attr($limit); ?>">
            </p>

        <?php
        }
        // method update
        public function update( $new_instance, $old_instance ){
            $instance                 =   $old_instance;
            $instance['title']        =   $new_instance['title'];
            $instance['limit']      =   $new_instance['limit'];
            return $instance;
        }

    }
    register_widget('Noo_Events');

endif;

if( !class_exists('Noo_Infomation') ):
    class Noo_Infomation extends  WP_Widget{

        public function __construct(){
            parent::__construct(
                'noo_infomation',
                'Noo Infomation',
                array('description', esc_html__('Noo Infomation', 'noo-chilli' ))
            );

        }

        public function widget($args, $instance){
            extract( $args );
            extract( $instance );
            if ( ! empty( $instance['title'] ) ) {
                $title = apply_filters( 'widget_title', $instance['title'] );
            }
            echo $before_widget;
            if ( ! empty( $title ) ) {
                echo $before_title . $title . $after_title;
            }
            ?>
            <ul class="noo-infomation">
                <li>
                    <span class="fa fa-map-marker infomation-left"></span>
                    <address>
                        <?php echo esc_html($address); ?>
                    </address>
                </li>
                <li>
                    <span class="fa fa-phone infomation-left"></span>
                    <span><?php echo esc_html($phone); ?></span>
                </li>
                <li>
                    <span class="fa fa-envelope infomation-left"></span>
                    <span><?php echo esc_html($mail); ?></span>
                </li>
            </ul>
            <?php
            echo $after_widget;
        }

        public function form( $instance ){
            $instance = wp_parse_args( $instance, array(
                'title'     =>  'Noo Infomation',
                'address'   =>  '',
                'phone'     =>  '',
                'mail'      =>  ''
            ) );
            extract($instance);
            ?>
            <p>
                <label for="<?php echo $this -> get_field_id('title'); ?>"><?php echo _e('Title:', 'noo-chilli' ); ?></label>
                <input type="text" id="<?php echo $this -> get_field_id('title'); ?>" name="<?php echo $this -> get_field_name('title') ?>" class="widefat" value="<?php echo esc_attr($title); ?>">
            </p>
            <p>
                <label for="<?php echo $this -> get_field_id('address'); ?>"><?php echo _e('Address', 'noo-chilli' ) ; ?></label>
                <textarea name="<?php echo $this -> get_field_name('address'); ?>" id="<?php echo $this -> get_field_id('address') ; ?>" cols="10" rows="5" class="widefat"><?php echo esc_attr($address); ?></textarea>
            </p>
            <p>
                <label for="<?php echo $this -> get_field_id('phone'); ?>"><?php echo _e('Phone:', 'noo-chilli' ); ?></label>
                <input type="text" id="<?php echo $this -> get_field_id('phone'); ?>" name="<?php echo $this -> get_field_name('phone') ?>" class="widefat" value="<?php echo esc_attr($phone); ?>">
            </p>
            <p>
                <label for="<?php echo $this -> get_field_id('mail'); ?>"><?php echo _e('Mail', 'noo-chilli' ) ; ?></label>
                <input class="widefat" type="text" name="<?php echo $this -> get_field_name('mail'); ?>" id="<?php echo $this -> get_field_id('mail') ; ?>" value="<?php echo esc_attr($mail); ?>">
            </p>

            <?php
        }
        // method update
        public function update( $new_instance, $old_instance ){
            $instance                 =   $old_instance;
            $instance['title']        =   $new_instance['title'];
            $instance['address']      =   $new_instance['address'];
            $instance['phone']        =   $new_instance['phone'];
            $instance['mail']         =   $new_instance['mail'];
            return $instance;
        }
    }
    register_widget('Noo_Infomation');
endif;

if( !class_exists('Noo_Openhours') ){
    class Noo_Openhours extends  WP_Widget{

        public function __construct(){
            parent::__construct(
                'noo_openhours',
                'Noo Open Hours',
                array('description'=>esc_html__('Display Open Hours', 'noo-chilli' ))
            );
            add_action('wp_ajax_add_tab', array($this, 'add_tab'));
        }

        public function widget($args, $instance){
            extract( $args );
            extract( $instance );
            if ( ! empty( $instance['title'] ) ) {
                $title = apply_filters( 'widget_title', $instance['title'] );
            }
            echo $before_widget;
            if ( ! empty( $title ) ) {
                echo $before_title . $title . $after_title;
            }
            ?>
            <ul class="noo-openhours">
                <?php
                    if( isset($tabs) && !empty($tabs)  ):
                        foreach($tabs as $tab):
                ?>
                   <li>
                       <span>
                           <?php echo esc_attr($tab['title']);  ?>
                       </span>
                       <span>
                           <?php echo esc_attr($tab['content']);  ?>
                       </span>
                   </li>
                    <?php endforeach; ?>
                <?php endif; ?>
            </ul>
            <?php
            echo $after_widget;
        }

        public function form( $instance ){
            extract(wp_parse_args($instance, array(
                'title' =>  esc_html__('Open Hours', 'noo-chilli' ),
                'tabs'  => array(
                    1 => array(
                        'title'  =>  '',
                        'content' =>  ''
                    )
                )
            )));
            ?>
            <p>
                <label for="<?php echo $this -> get_field_id('title'); ?>"><?php echo _e('Title:', 'noo-chilli' ); ?></label>
                <input type="text" id="<?php echo $this -> get_field_id('title'); ?>" name="<?php echo $this -> get_field_name('title') ?>" class="widefat" value="<?php echo esc_attr($title); ?>">
            </p>
            <div class="open-hours">
                <?php

                    $tabs = is_array($tabs) ? $tabs : $instance['tabs'];
                    $count = 0;
                    foreach($tabs as $tab) {
                        $this->tab($tab, $count);
                        $count++;
                    }
                ?>
            </div>
            <p><span class="add-hours button-primary" data-num="<?php echo esc_attr(count($tabs)); ?>"><?php _e('ADD Field', 'noo-chilli' ); ?></span></p>
            <?php
        }
        function tab($tab = array(), $count = 0) {
            ?>
                <div class="openhousrs-item">
                    <p class="tab-desc description">
                        <label for="<?php echo $this->get_field_id('tabs') ?>-<?php echo esc_attr($count) ?>-title"><?php _e('Title', 'noo-chilli' ); ?></label>
                        <input type="text" id="<?php echo $this->get_field_id('tabs') ?>-<?php echo esc_attr($count) ?>-title" class="widefat" name="<?php echo $this->get_field_name('tabs') ?>[<?php echo esc_attr($count) ?>][title]" value="<?php echo esc_attr($tab['title']) ?>" />

                    </p>
                    <p class="tab-desc description">
                        <label for="<?php echo $this->get_field_id('tabs') ?>-<?php echo esc_attr($count) ?>-content"><?php _e('Content', 'noo-chilli' ); ?></label>
                        <input type="text" id="<?php echo $this->get_field_id('tabs') ?>-<?php echo esc_attr($count) ?>-content" class="widefat" name="<?php echo $this->get_field_name('tabs') ?>[<?php echo esc_attr($count) ?>][content]" value="<?php echo esc_attr($tab['content']) ?>" />
                    </p>
                    <button class="openhours-remove">X</button>
                </div>
            <?php
        }
        function update($new_instance, $old_instance) {
            $new_instance = $new_instance;
            return $new_instance;
        }

        /* AJAX add tab */
        function add_tab() {
            $count = isset($_POST['count']) ? absint($_POST['count']) : false;

            //default key/value for the tab
            $tab = array(
                'title' => '',
                'content' => ''
            );

            if($count) {
                $this->tab($tab, $count);
            } else {
                die(-1);
            }

            die();
        }

    }
    register_widget('Noo_Openhours');
}


class Noo_MailChimp extends WP_Widget {

    public function __construct() {
        parent::__construct(
            'noo_mailchimp_widget',  // Base ID
            'Noo MailChimps',  // Name
            array( 'classname' => 'mailchimp-widget', 'description' => esc_html__( 'Display simple MailChimp subscribe form.', 'noo-chilli' ) ) );
    }

    public function widget( $args, $instance ) {
        extract( $args );
        if ( ! empty( $instance['title'] ) ) {
            $title = apply_filters( 'widget_title', $instance['title'] );
        }
        echo $before_widget;
        if ( ! empty( $title ) ) {
            echo $before_title . $title . $after_title;
        }

        $mail_list;
        ?>
        <form class="mc-subscribe-form<?php echo (isset($_COOKIE['noo_subscribed']) ? ' submited':'')?>">
            <?php if( isset($_COOKIE['noo_subscribed']) ) : ?>
                <label class="noo-message alert" role="alert"><?php _e( 'You\'ve already subscribed.', 'noo-chilli' ); ?></label>
            <?php else: ?>
                <label for="mc_email"><span><?php echo esc_attr( $instance['subscribe_text'] ); ?></span></label>
                <div class="mc-content">
                    <input type="email" id="mc_email" name="mc_email" class="mc-email" value="" placeholder="<?php _e( 'Enter your email', 'noo-chilli' ); ?>" />
                    <button class="fa fa-angle-right mailchip-sm"></button>
                </div>
                <input type="hidden" name="mc_list_id" value="<?php echo esc_attr( $instance['mail_list'] ); ?>" />
                <input type="hidden" name="action" value="noo_mc_subscribe" />
                <?php wp_nonce_field('noo-subscribe','nonce'); ?>
            <?php endif; ?>
        </form>
        <?php
        echo $after_widget;
    }

    public function form( $instance ) {
        $defaults = array(
            'title' => '',
            'subscribe_text' => esc_html__( 'Subscribe to stay update', 'noo-chilli' ),
            'mail_list' => ''
        );
        $instance = wp_parse_args( (array) $instance, $defaults );

        global $noo_mailchimp;
        $api_key = noo_get_option('noo_mailchimp_api_key', '');
        $mail_list = !empty( $api_key ) ? $noo_mailchimp->get_mail_lists( $api_key ) : '';

        echo '
		<p>
			<label>' . esc_html__( 'Title', 'noo-chilli' ) . ':</label>
			<input type="text" name="' .
            $this->get_field_name( 'title' ) . '" id="' . $this->get_field_id( 'title' ) . '" value="' .
            esc_attr( $instance['title'] ) . '" class="widefat" />
		</p>
		<p>
			<label>' . esc_html__( 'Subscribe Text', 'noo-chilli' ) . ':</label>
			<input type="text" name="' .
            $this->get_field_name( 'subscribe_text' ) . '" id="' . $this->get_field_id( 'subscribe_text' ) . '" value="' .
            esc_attr( $instance['subscribe_text'] ) . '" class="widefat" />
		</p>';
        if(!empty($mail_list)) {
            echo '
		<p>
			<label>' . esc_html__( 'Subscribe Mail List', 'noo-chilli' ) . ':</label>
			<select name="' .
                $this->get_field_name( 'mail_list' ) . '" id="' . $this->get_field_id( 'mail_list' ) . '" class="widefat" >';
            foreach($mail_list as $id => $list_name) {
                echo '<option value="' . esc_attr($id) . '" ' . selected( $instance['mail_list'], $id, false ) . '>' . esc_html($list_name) . '</option>';
            }
            echo '	</select>
		</p>';
        } else {
            echo '<p>' . esc_html__( 'There\'s problem get your mail list, please check your MailChimp API Key settings in Customizer', 'noo-chilli' ) . '</p>';
        }
    }
}

function noo_register_widget() {
    if( noo_get_option( 'noo_mailchimp', true ) ) {
        $subscribe_mail_list = noo_get_option( 'noo_mailchimp_api_key', '' );
        if( ! empty( $subscribe_mail_list ) ) {
            register_widget( 'Noo_MailChimp' );
        }
    }
}
if( class_exists('MCAPI' ) ) {
    add_action( 'widgets_init', 'noo_register_widget' );
}

