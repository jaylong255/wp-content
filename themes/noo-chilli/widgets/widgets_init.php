<?php
/**
 * This file initialize widgets area used in this theme.
 *
 *
 * @package    NOO Framework
 * @subpackage Widget Initiation
 * @version    1.0.0
 * @author     Kan Nguyen <khanhnq@nootheme.com>
 * @copyright  Copyright (c) 2014, NooTheme
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       http://nootheme.com
 */

if ( ! function_exists( 'noo_widgets_init' ) ) :

	function noo_widgets_init() {
		
		// Default Sidebar (WP main sidebar)
		register_sidebar( 
			array(  // 1
				'name' => esc_html__( 'Main Sidebar', 'noo-chilli' ), 
				'id' => 'sidebar-main', 
				'description' => esc_html__( 'Default Blog Sidebar.', 'noo-chilli' ), 
				'before_widget' => '<div id="%1$s" class="widget %2$s">', 
				'after_widget' => '</div>', 
				'before_title' => '<h4 class="widget-title">', 
				'after_title' => '</h4>' ) );

        register_sidebar(
			array(  // 1
				'name' => esc_html__( 'Shop Sidebar', 'noo-chilli' ),
				'id' => 'shop-main',
				'description' => esc_html__( 'Default Shop Sidebar.', 'noo-chilli' ),
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget' => '</div>',
				'before_title' => '<h4 class="widget-title">',
				'after_title' => '</h4>' ) );


        register_sidebar(
            array(
                'name' => esc_html__( 'Noo  Footer Top', 'noo-chilli' ),
                'id' => 'noo-footer-top',
                'description' => esc_html__( 'Display Footer Top.', 'noo-chilli' ),
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget' => '</div>',
                'before_title' => '<h4 class="widget-title">',
                'after_title' => '</h4>' ) );



//        $noo_bottom = noo_get_option('noo_footer_bottom');
//        if( isset($noo_bottom) && $noo_bottom == 1 ):
            register_sidebar(
                array(
                    'name' => esc_html__( 'Noo  Footer Bottom', 'noo-chilli' ),
                    'id' => 'noo-footer-bottom',
                    'description' => esc_html__( 'Display Footer Bottom.', 'noo-chilli' ),
                    'before_widget' => '<div id="%1$s" class="widget %2$s">',
                    'after_widget' => '</div>',
                    'before_title' => '<h4 class="widget-title">',
                    'after_title' => '</h4>' ) );
//        endif;

		// Footer Columns (Widgetized)
		$num = ( noo_get_option( 'noo_footer_widgets' ) == '' ) ? 3 : noo_get_option( 'noo_footer_widgets' );
       // var_dump(noo_get_option( 'noo_footer_widgets' )); die();
		for ( $i = 1; $i <= $num; $i++ ) :
			register_sidebar( 
				array( 
					'name' => esc_html__( 'NOO - Footer Column #', 'noo-chilli' ) . esc_attr($i),
					'id' => 'noo-footer-' . esc_attr($i),
					'before_widget' => '<div id="%1$s" class="widget %2$s">', 
					'after_widget' => '</div>', 
					'before_title' => '<h4 class="widget-title">', 
					'after_title' => '</h4>' ) );
		endfor
		;
	}
	add_action( 'widgets_init', 'noo_widgets_init' );

endif;
