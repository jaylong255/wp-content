<?php
$num        = ( noo_get_option( 'noo_footer_widgets', '3' ) == '' ) ? '3' : noo_get_option( 'noo_footer_widgets', '3' );
$copyright  = noo_get_option('noo_bottom_bar_content','');
$image      = noo_get_option('noo_footer_top_logo');
$noo_bottom = noo_get_option('noo_footer_bottom');
?>
<footer class="noo-footer" <?php if( isset($image) && $image != '' ): ?>style="background-image: url('<?php echo esc_url($image); ?>')"<?php endif; ?>>
    <div class="footer-top">
        <div class="container">
            <div class="footer-top-content">
                <?php if( function_exists('dynamic_sidebar') && dynamic_sidebar('noo-footer-top') ): ?>
                <?php else : ?>
                    <aside class="widget">
                        <h4 class="widget-title">Footer Top</h4>
                        <a class="demo-widgets" href="<?php echo esc_url( admin_url( 'widgets.php' ) ); ?>"><?php echo esc_html__( 'Click here to add your widgets', 'noo-chilli' ); ?></a>
                    </aside>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <?php if ( $num != 0 ) : ?>
        <div class="noo-footer-bk">
            <div class="container">
                <div class="row">

                <?php

                $i = 0; while ( $i < $num ) : $i ++;
                    switch ( $num ) {
                        case 4 : $class = 'col-md-3 col-sm-6 footer-item';  break;
                        case 3 :
                            $class = 'col-md-4 col-sm-4 footer-item';
                            break;
                        case 2 : $class = 'col-md-6 col-sm-12 footer-item';  break;
                        case 1 : $class = 'col-md-12 footer-item'; break;
                    }
                    ?>
                    <div class="<?php echo esc_attr($class); ?>">
                        <?php
                            if( function_exists('dynamic_sidebar') && dynamic_sidebar( 'noo-footer-' . esc_attr($i) ) ):
                            else:
                        ?>
                            <aside class="widget">
                                <h4 class="widget-title">Footer <?php echo esc_attr($i); ?></h4>
                                <a class="demo-widgets" href="<?php echo esc_url( admin_url( 'widgets.php' ) ); ?>"><?php echo esc_html__( 'Click here to add your widgets', 'noo-chilli' ); ?></a>
                            </aside>
                        <?php endif; ?>
                    </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>

        <div class="footer-bottom">
            <div class="container">
                <?php if( function_exists('dynamic_sidebar') && dynamic_sidebar('noo-footer-bottom') ): ?>
                <?php else : ?>
                    <aside class="widget">
                        <h4 class="widget-title">Footer Bottom</h4>
                        <a class="demo-widgets" href="<?php echo esc_url( admin_url( 'widgets.php' ) ); ?>"><?php echo esc_html__( 'Click here to add your widgets', 'noo-chilli' ); ?></a>
                    </aside>
                <?php endif; ?>
            </div>
        </div>

    <?php endif; ?>
    <?php if( isset($copyright) && !empty($copyright) ): ?>
        <div class="container">
            <p class="copyright">
                <?php echo noo_html_content_filter($copyright); ?>
            </p>
        </div>
    <?php endif; ?>
</footer>
</div>
<?php wp_footer(); ?>
</body>
</html>
