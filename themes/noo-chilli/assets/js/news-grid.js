function noo_news_masonry(){
    "use strict";
    var $container = jQuery('.noo-news-wrap');
    $container.imagesLoaded(function(){
        $container.isotope({
            itemSelector : '.news-grid-item',
            transitionDuration : '0.8s',
            masonry : {
                'gutter' : 0
            }
        });
    });
}
jQuery(document).ready(function(){
    noo_news_masonry();
});
jQuery(window).load(function(){
    noo_news_masonry();
});
jQuery(document).on('noo-layout-changed', function() {
    noo_news_masonry();
});