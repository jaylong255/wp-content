/*!
 * Simple jQuery Equal Heights
 *
 * Copyright (c) 2013 Matt Banks
 * Dual licensed under the MIT and GPL licenses.
 * Uses the same license as jQuery, see:
 * http://docs.jquery.com/License
 *
 * @version 1.5.1
 */
(function($) {
    "use strict";
    $.fn.equalHeights = function() {
        var maxHeight = 0,
            $this = $(this);

        $this.each( function() {
            var height = $(this).innerHeight();

            if ( height > maxHeight ) { maxHeight = height; }
        });

        return $this.css('height', maxHeight);
    };

    // auto-initialize plugin
    $('[data-equal]').each(function(){
        var $this = $(this),
            target = $this.data('equal');
        // $this.find(target).equalHeights();
        // Using imagesLoaded to fix the height issues.
        imagesLoaded($(this),function(){
            $this.find(target).equalHeights();
        });
    });

})(jQuery);

/*!
 * NOO Site Script.
 *
 * Javascript used in NOO-Framework
 * This file contains base script used on the frontend of NOO theme.
 *
 * @package    NOO Framework
 * @subpackage NOO Site
 * @version    1.0.0
 * @author     Kan Nguyen <khanhnq@nootheme.com>
 * @copyright  Copyright (c) 2014, NooTheme
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       http://nootheme.com
 */
// =============================================================================

;(function($){
    "use strict";
    var nooGetViewport = function() {
        var e = window, a = 'inner';
        if (!('innerWidth' in window )) {
            a = 'client';
            e = document.documentElement || document.body;
        }
        return { width : e[ a+'Width' ] , height : e[ a+'Height' ] };
    };
    var nooGetURLParameters = function(url) {
        var result = {};
        var searchIndex = url.indexOf("?");
        if (searchIndex == -1 ) return result;
        var sPageURL = url.substring(searchIndex +1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++)
        {
            var sParameterName = sURLVariables[i].split('=');
            result[sParameterName[0]] = sParameterName[1];
        }
        return result;
    };


    $.fn.nooLoadmore = function(options,callback){
        var defaults = {
            contentSelector: null,
            contentWrapper:null,
            nextSelector: "div.navigation a:first",
            navSelector: "div.navigation",
            itemSelector: "div.post",
            dataType: 'html',
            finishedMsg: "<em>Congratulations, you've reached the end of the internet.</em>",
            loading:{
                speed:'fast',
                start: undefined
            },
            state: {
                isDuringAjax: false,
                isInvalidPage: false,
                isDestroyed: false,
                isDone: false, // For when it goes all the way through the archive.
                isPaused: false,
                isBeyondMaxPage: false,
                currPage: 1
            }
        };
        var options = $.extend(defaults, options);

        return this.each(function(){
            var self = this;
            var $this = $(this),
                wrapper = $this.find('.loadmore-wrap'),
                action = $this.find('.loadmore-action'),
                btn = action.find(".btn-loadmore"),
                loading = action.find('.loadmore-loading');

            options.contentWrapper = options.contentWrapper || wrapper;



            var _determinepath = function(path){
                if (path.match(/^(.*?)\b2\b(.*?$)/)) {
                    path = path.match(/^(.*?)\b2\b(.*?$)/).slice(1);
                } else if (path.match(/^(.*?)2(.*?$)/)) {
                    if (path.match(/^(.*?page=)2(\/.*|$)/)) {
                        path = path.match(/^(.*?page=)2(\/.*|$)/).slice(1);
                        return path;
                    }
                    path = path.match(/^(.*?)2(.*?$)/).slice(1);

                } else {
                    if (path.match(/^(.*?page=)1(\/.*|$)/)) {
                        path = path.match(/^(.*?page=)1(\/.*|$)/).slice(1);
                        return path;
                    } else {
                        options.state.isInvalidPage = true;
                    }
                }
                return path;
            }
            if(!$(options.nextSelector).length){
                return;
            }


            // callback loading
            options.callback = function(data, url) {
                if (callback) {
                    callback.call($(options.contentSelector)[0], data, options, url);
                }
            };

            options.loading.start = options.loading.start || function() {
                btn.hide();
                $(options.navSelector).hide();
                loading.show(options.loading.speed, $.proxy(function() {
                    loadAjax(options);
                }, self));
            };

            var loadAjax = function(options){
                var path = $(options.nextSelector).attr('href');
                path = _determinepath(path);

                var callback=options.callback,
                    desturl,frag,box,children,data;

                options.state.currPage++;
                // Manually control maximum page
                if ( options.maxPage !== undefined && options.state.currPage > options.maxPage ){
                    options.state.isBeyondMaxPage = true;
                    return;
                }
                desturl = path.join(options.state.currPage);
                box = $('<div/>');
                box.load(desturl + ' ' + options.itemSelector,undefined,function(responseText){
                    children = box.children();
                    if (children.length === 0) {
                        //loading.hide();
                        btn.hide();
                        action.append('<div style="margin-top:5px;">' + options.finishedMsg + '</div>').animate({ opacity: 1 }, 2000, function () {
                            action.fadeOut(options.loading.speed);
                        });
                        return ;
                    }
                    frag = document.createDocumentFragment();
                    while (box[0].firstChild) {
                        frag.appendChild(box[0].firstChild);
                    }
                    $(options.contentWrapper)[0].appendChild(frag);
                    data = children.get();
                    loading.hide();
                    btn.show(options.loading.speed);
                    options.callback(data);

                });
            }


            btn.on('click',function(e){
                e.stopPropagation();
                e.preventDefault();
                options.loading.start.call($(options.contentWrapper)[0],options);
            });
        });
    };



    var nooInit = function() {





        //Init masonry isotope
        $('.masonry').each(function(){
            var self = $(this);
            var $container = $(this).find('.masonry-container');
            var $filter = $(this).find('.masonry-filters a');
            $container.isotope({
                itemSelector : '.masonry-item',
                transitionDuration : '0.8s',
                masonry : {
                    'gutter' : 0
                }
            });

            imagesLoaded(self,function(){
                $container.isotope('layout');
            });

            $filter.click(function(e){
                e.stopPropagation();
                e.preventDefault();

                var $this = jQuery(this);
                // don't proceed if already selected
                if ($this.hasClass('selected')) {
                    return false;
                }
                self.find('.masonry-result h3').text($this.text());
                var filters = $this.closest('ul');
                filters.find('.selected').removeClass('selected');
                $this.addClass('selected');

                var options = {
                        layoutMode : 'masonry',
                        transitionDuration : '0.8s',
                        'masonry' : {
                            'gutter' : 0
                        }
                    },
                    key = filters.attr('data-option-key'),
                    value = $this.attr('data-option-value');

                value = value === 'false' ? false : value;
                options[key] = value;

                $container.isotope(options);

            });
        });



        if($('.masonry').length){
            $('.masonry').each(function(){
                var $this = $(this);
                $this.find('div.pagination').hide();
                $this.find('.loadmore-loading').hide();
                $this.nooLoadmore({
                    navSelector  : $this.find('div.pagination'),
                    nextSelector : $this.find('div.pagination a.next'),
                    itemSelector : '.loadmore-item',
                    contentWrapper: $this.find('.masonry-container'),
                    loading:{
                        speed:1,
                        start: undefined
                    },
                    finishedMsg  : ''
                },function(newElements){
                    var masonrycontainer = $this.find('.masonry-container');
                    $this.find('.masonry-container').isotope('appended', $(newElements));
                    masonrycontainer.isotope('layout');
                    masonrycontainer.isotope( 'on', 'layoutComplete', function() {
                        $(".posts-loop-title h3 span").html($('.masonry-item:visible').length);
                    });
                    $(window).unbind('.infscr');

                    imagesLoaded(masonrycontainer,function(){
                        masonrycontainer.isotope('layout');
                    });
                });
            });
        }



    };
    $( document ).ready( function () {
        nooInit();

    });

})(jQuery);

