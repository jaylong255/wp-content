jQuery(window).load(function(){

    "use strict";

    if ( jQuery('.parallax').length ) {
        jQuery('.parallax').each(function(){
            jQuery(this).parallax("30%", 0.1);
        });
    }
});
jQuery(document).ready(function(){

    "use strict";

    var $check_admin =  jQuery('#wpadminbar');
    if ( $check_admin.length > 0 ){
        jQuery('.noo-header').addClass('noo-adminbar');
    }

    // Scroll Menu Onpage
    jQuery('#noo-nav').onePageNav({
        currentClass: 'current-menu-item'
    });

    function navbar_scroll() {
        var $check_static_bar =  jQuery('.noo-static-top');
        var $check_onepage =  jQuery('.noo-onepage');

        if ( $check_static_bar.length == 0 && $check_onepage.length == 0 ) {
            var height_scroll = 0;
            // Check topbar, if isset topbar
            var $check_topbar =  jQuery('.top_bar');
            if ( $check_topbar.length > 0 ){
                height_scroll = 40;
            }

            if ( jQuery('.noo-menu-absolute').length > 0 ) {
                jQuery('.rest-nav').css('height', 0);
            } else {
                jQuery('.rest-nav').css( 'height', jQuery('.noo-box-header').height()+'px' );
            }
            
            var $_scrollTop = jQuery(window).scrollTop();

            if( $_scrollTop > height_scroll ){
                jQuery('.noo-box-header').addClass('noo-fix-top');
                jQuery('.rest-nav').show();
            }else{
                jQuery('.noo-box-header').removeClass('noo-fix-top');
                jQuery('.rest-nav').hide();
            }
        }

        if ( $check_onepage.length > 0 ) {
            if(jQuery('.wpb_revslider_element').length > 0){
                var $_revo = jQuery('.wpb_revslider_element').height();
                var _top   = jQuery(window).scrollTop();
                if( _top >= $_revo ){
                    jQuery('.noo-onepage').addClass('noo-onepage-eff');
                }else{
                    jQuery('.noo-onepage').removeClass('noo-onepage-eff');
                }
            }
        }

    }
    // scroll height
    navbar_scroll();
    jQuery(window).scroll(function(){
        navbar_scroll();
    });

    //Go to top
    jQuery(window).scroll(function () {
        if (jQuery(this).scrollTop() > 500) {
            jQuery('.go-to-top').addClass('on');
        }
        else {
            jQuery('.go-to-top').removeClass('on');
        }
    });
    jQuery('body').on( 'click', '.go-to-top', function () {
        jQuery("html, body").animate({
            scrollTop: 0
        }, 800);
        return false;
    });
    jQuery('.reloadpage').live('click',function(){
       location.reload();

    });
    if(  jQuery('.noo-images-slider').length > 0){
        jQuery('.noo-images-slider').owlCarousel({
            items : 3,
            itemsDesktop : [1199,3],
            itemsDesktopSmall : [979,3],
            itemsTablet: [768, 2],
            slideSpeed:500,
            paginationSpeed:1000,
            rewindSpeed:true,
            autoHeight: false,
            addClassActive: true,
            autoPlay: true,
            loop:true,
            pagination: false
        });
    }

    jQuery('.tz-icon-search').click(function(){
       jQuery('.top-search').toggleClass('top-search-eff');
    });
    if( jQuery('.single-gallery').length > 0){
        jQuery('.single-gallery').owlCarousel({
            items : 1,
            itemsDesktop : [1199,1],
            itemsDesktopSmall : [979,1],
            itemsTablet: [768, 1],
            slideSpeed:500,
            paginationSpeed:1000,
            rewindSpeed:true,
            autoHeight: false,
            addClassActive: true,
            autoPlay: true,
            loop:true,
            pagination: true
        });
    }
});