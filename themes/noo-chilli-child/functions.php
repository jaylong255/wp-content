<?php
/**
 * Theme functions for NOO Chilli Child Theme.
 *
 * @package    NOO Chilli Child Theme
 * @version    1.0.0
 * @author     Kan Nguyen <khanhnq@nootheme.com>
 * @copyright  Copyright (c) 2014, NooTheme
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       http://nootheme.com
 */

// If you want to override function file,
// you should copy function file to the same folder ( like /framework/admin/ ) on child theme, then use similar require_one 
// statement like this code.