<?php
// [Noo Testimonial]
// ============================
if( !function_exists('noo_shortcode_testimonial') ){
    function noo_shortcode_testimonial($atts){
        extract(shortcode_atts(array(
            'style'             =>  1,
            'border_color'      =>  '#f5f5f5',
            'categories'        =>  '',
            'autoplay'          =>  'true',
            'orderby'           =>  'latest',
            'posts_per_page'    =>  '10',
        ),$atts));
        ob_start();
        
        $style_class = '';
        if( $style == 2 ){
            $style_class = ' noo-testimonial-circle';
        }        
        $style_border = "border-color:" . $border_color;

        wp_enqueue_script( 'owl.carousel' );
        
            $order = 'DESC';
            switch ($orderby) {
                case 'latest':
                    $orderby = 'date';
                    break;
                case 'oldest':
                    $orderby = 'date';
                    $order = 'ASC';
                    break;
                case 'alphabet':
                    $orderby = 'title';
                    $order = 'ASC';
                    break;
                case 'ralphabet':
                    $orderby = 'title';
                    break;
                default:
                    $orderby = 'date';
                    break;
            }
            $args = array(
                'post_type' =>  'testimonial',
                'orderby'           =>   $orderby,
                'order'             =>   $order,
                'posts_per_page'    =>   $posts_per_page,
            );
            if( $categories != 'all' && $categories != ''  ){
                $args['tax_query'][]  = array(
                    'taxonomy'  =>  'testimonial_category',
                    'field'     =>  'id',
                    'terms'     =>   array($categories)
                );
            }
            $query = new WP_Query( $args );

        ?>
        <div class="the_say">
            <ul class="noo_testimonial <?php echo esc_attr($style_class); ?>">
                <?php if( $query->have_posts() ): ?>
                <?php  while( $query->have_posts() ): $query->the_post();
                        $name     = get_post_meta(get_the_ID(),'_noo_wp_post_name', true);
                        $position = get_post_meta(get_the_ID(),'_noo_wp_post_position', true);
                        $image_id = get_post_meta(get_the_ID(),'_noo_wp_post_image', true);
                ?>
                        <li>
                            <?php if( $style == 2 ) : ?>
                                <?php if( isset($image_id) && $image_id != '' ): ?><span class="avatar" style="<?php echo esc_attr($style_border); ?>"><?php echo wp_get_attachment_image(esc_attr($image_id),'noo-thumbnail-square'); ?></span><?php endif; ?>
                                <?php the_content(); ?>
                            <?php else : ?>
                                <?php the_content(); ?>
                                <?php if( isset($image_id) && $image_id != '' ): ?><span class="avatar" style="<?php echo esc_attr($style_border); ?>"><?php echo wp_get_attachment_image(esc_attr($image_id),'noo-thumbnail-square'); ?></span><?php endif; ?>
                            <?php endif; ?>                            
                            <?php if( isset($name) && $name != '' ): ?><h6 class="name"><?php echo esc_html($name); ?></h6><?php endif; ?>
                            <?php if( isset($position) && $position != '' ): ?><span class="position"><?php echo esc_html($position); ?></span><?php endif; ?>
                        </li>
                <?php endwhile; ?>
                <?php endif; wp_reset_postdata(); ?>
            </ul>
        </div>
        <script>
            jQuery(document).ready(function(){
                jQuery('.noo_testimonial').each(function(){
                    jQuery(this).owlCarousel({
                        items : 1,
                        itemsDesktop : [1199,1],
                        itemsDesktopSmall : [979,1],
                        itemsTablet: [768, 1],
                        slideSpeed:500,
                        paginationSpeed:800,
                        rewindSpeed:1000,
                        autoHeight: false,
                        addClassActive: true,
                        autoPlay: <?php echo esc_attr($autoplay); ?>, 
                        loop:true,
                        pagination: true
                    });
                });
            });
        </script>
        <?php

        $testimonial = ob_get_contents();
        ob_end_clean();
        return $testimonial;
    }
    add_shortcode('noo_testimonial','noo_shortcode_testimonial');
}
?>