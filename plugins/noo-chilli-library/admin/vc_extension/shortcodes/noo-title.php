<?php
// [Noo Title]
// ============================
if( !function_exists('noo_shortcode_title') ) :
    function noo_shortcode_title($attrs){
        extract(shortcode_atts(array(
            'title'       =>  '',
            'sub_title'   =>  '',
            'title_size'  =>  'large',
            'description' =>  '',
            'white_style' => false
        ),$attrs));
        ob_start();
        $class_med = ( $title_size == 'medium' ) ? 'noo-title-medium' : '';
        $class_white = ( $white_style ) ? 'invert' : '';
        ?>
            <div class="noo-title-wrap <?php echo esc_attr( $class_white ); ?>">
                <div class="noo-title-block">
                <?php if( isset($title) && !empty($title) ): ?>
                    <h3 class="noo-title header-title <?php echo esc_attr( $class_med ); ?>"><?php echo esc_html($title) ?></h3>
                <?php endif; ?>
                <?php if( isset($sub_title) && $sub_title != '' ): ?>
                    <p class="noo-sub-title"><?php echo esc_html($sub_title); ?></p>
                <?php endif; ?>
                </div>
                <?php if( isset($description) && $description != '' ): ?>
                    <p class="noo-ds"><?php echo esc_html($description); ?></p>
                <?php endif; ?>
            </div> <!-- /.noo-title-wrap -->
        <?php
        $title = ob_get_contents();
        ob_end_clean();
        return $title;
    }
    add_shortcode('noo_title','noo_shortcode_title');
endif;

?>