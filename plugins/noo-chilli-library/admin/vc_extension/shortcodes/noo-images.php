<?php
// [Noo Images]
// ============================
if( !function_exists('noo_shortcode_images') ) :
    function noo_shortcode_images($attrs){
        extract(shortcode_atts(array(
            'logo'           =>  '',
            'style'          =>  '1',
            'background_img' =>  '',
            'slide_img'      =>  ''
        ), $attrs));
        ob_start();
        // Mix images
        if ( $style == 2 )
        {
            $image_id = array();
            if( isset($slide_img) && !empty($slide_img) ){
                $image_id = explode(',',$slide_img);
            }
            if ( $image_id ) {
                $count_images_id = count($image_id);
                $i = 0;
                while ( count($image_id) < 8) {
                    $image_id[] = $image_id[$i];
                    $i++;
                    if ($i >= $count_images_id )
                        $i = 0;
                }
                $arr2 = array();
                foreach ($image_id as $key => $value) {
                    $arr1[] = $value;
                    if ( $key % 2 != 0) {
                        $arr2[] = $arr1;
                        $arr1 = array();    
                    }
                }
                $image_id = $arr2;
            }
        }
    ?>
        <div class="noo-images" <?php if( isset($background_img) && $background_img != '' && $style == 1 ): ?>style="background-image: url('<?php echo esc_url(wp_get_attachment_url($background_img)); ?>')"<?php endif; ?>>
            <?php if( $style == 2 && is_array($image_id) ): ?>
            <ul>
                <?php foreach($image_id as $k => $id): if ($k < 4) : ?>
                <?php
                    $img_1 = wp_get_attachment_image_src( esc_attr($id[0]), 'large' );
                    $img_1 = $img_1[0];

                    $img_2 = wp_get_attachment_image_src( esc_attr($id[1]), 'large' );
                    $img_2 = $img_2[0];
                ?>
                <li>
                    <div class="front" style="background-image: url('<?php echo esc_url( $img_1 ); ?>')"></div>
                    <div class="back" style="background-image: url('<?php echo esc_url( $img_2 ); ?>')"></div>
                </li>
                <?php endif; endforeach; ?>
            </ul>
            <?php endif; ?>
            <div class="logo-images" style="background-image: url('<?php echo wp_get_attachment_url(esc_attr($logo)); ?>')"></div>
        </div> <!-- /.noo-images -->
        <?php if( $style == 2 && is_array($image_id) ): ?>
        <script>
            var myTimeOut  = null;
            var myInterval = null;

            jQuery(document).ready(function($){

                // Clear time
                clearInterval(myInterval);
                clearTimeout(myTimeOut);
                
                $('.noo-images li').each(function(index){
                    // Define variable
                    var time_flip = 7000;
                    var obj = $(this);
                    if ( index == 0 || index == 3 ) 
                        var time = 0;
                    else
                        var time = 4000;
                    // Set time
                    myTimeOut = setTimeout(function(){
                        myInterval = setInterval(function(){ 
                            if ( ! obj.hasClass('flip') )
                                obj.addClass('flip');
                            else
                                obj.removeClass('flip');   
                        }, time_flip);
                    }, time);
                })
            });
        </script>
        <?php endif; ?>
    <?php
        $book = ob_get_contents();;
        ob_end_clean();
        return $book;
    }
    add_shortcode('noo_images','noo_shortcode_images');
endif;
?>