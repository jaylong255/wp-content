<?php
// [Noo Menu Grid]
// ============================
if( !function_exists('noo_shortcode_menu_grid') ):
    function noo_shortcode_menu_grid($attrs){
        extract(shortcode_atts(array(
            'style'          =>  'medium', //small
            'hide_filter'    =>  false,
            'show_icon'      =>  true,
            'icon'           =>  '',
            'cat'            =>  '',
            'limit'          =>  8,
            'orderby'        =>  'latest'
        ),$attrs));
        ob_start();

        $order = 'DESC';
        switch ($orderby) {
            case 'latest':
                $orderby = 'date';
                break;
            case 'oldest':
                $orderby = 'date';
                $order = 'ASC';
                break;
            case 'alphabet':
                $orderby = 'title';
                $order = 'ASC';
                break;
            case 'ralphabet':
                $orderby = 'title';
                break;
            default:
                $orderby = 'date';
                break;
        }
        $args = array(
            'post_type'      =>  'food_menu',
            'posts_per_page' =>  $limit,
            'orderby'        =>  $orderby,
            'order'          =>  $order,
        );
        if ( isset($cat) && !empty($cat) ) {
            $categories = explode(',', $cat);
            $args['tax_query'][]  = array(
                'taxonomy' =>  'menu_sections',
                'field'    =>  'term_id',
                'terms'    =>   $categories
            );
        }
        $query = new WP_Query( $args );

        
        wp_enqueue_script('vendor-imagesloaded');
        wp_enqueue_script('isotope');
        wp_enqueue_script('portfolio');
        // test
        $class_style = ( $style == 'small' ) ? 'style1' : 'style2';
        ?>
            <div class="menu-grid">
                <?php
                $args_cat = array(
                    'type'                     => 'food_menu',
                    'child_of'                 => 0,
                    'parent'                   => '',
                    'orderby'                  => 'name',
                    'order'                    => 'DESC',
                    'hide_empty'               => 1,
                    'hierarchical'             => 1,
                    'exclude'                  => '',
                    'include'                  => '',
                    'number'                   => '',
                    'taxonomy'                 => 'menu_sections',
                    'pad_counts'               => false
                );

                if( !$hide_filter ) :
                $category = get_categories($args_cat);
                    if( isset($category) && !empty($category) ):
                ?>
                    <div class="masonry-filters">
                        <ul data-option-key="filter">
                            <li>
                                <a data-option-value="*" href="#all" class="selected"><?php echo esc_html__('All', 'noo-chilli' ); ?></a>
                            </li>
                            <?php foreach( $category as $cat ): ?>
                                <li>
                                    <a data-option-value=".<?php echo esc_attr($cat->slug); ?>" href="#<?php echo esc_url($cat->name); ?>"><?php echo esc_html($cat->name); ?></a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <?php endif; ?>
                <?php endif; ?>

                <?php if( $show_icon && $icon != '' ): ?>
                <img class="food-icon" src="<?php echo wp_get_attachment_url(esc_attr($icon)); ?>" alt="food">
                <?php endif; ?>

                <div class="noo-menu-wrap">
                <?php
                if( $query->have_posts() ):
                    while( $query->have_posts() ):
                        $query->the_post();
                        $term_cat = get_the_terms(get_the_ID(), 'menu_sections');
                        $class_term = ' ';
                        if( isset($term_cat) && !empty($term_cat) ){
                            foreach($term_cat as $term):
                                $class_term .= ' '.$term->slug;
                            endforeach;
                        }
                        $price = noo_get_post_meta(get_the_ID(),'_noo_wp_food_price');
                        $attr  = noo_get_post_meta(get_the_ID(),'_noo_wp_food_attributes');
                ?>
                <div class="menu-grid-item col-md-4 col-sm-12<?php echo esc_attr($class_term); ?>">
                    <div class="menu-item menu-item-<?php echo esc_attr($class_style); ?>">
                        <div class="menu-grid-thum">
                            <?php the_post_thumbnail(); ?>
                            <span class="price"><?php echo esc_attr($price); ?></span>
                            <span class="line-left"></span>
                            <span class="line-right"></span>
                        </div>
                        <div class="menu-grid-content">
                            <h6><?php the_title(); ?></h6>
                            <?php the_content(); ?>
                            <span class="attr">
                                <?php echo esc_attr($attr); ?>
                            </span>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
            </div>
            <?php
                endif; wp_reset_postdata();
            ?>
        </div>
        <?php
        $grid = ob_get_contents();
        ob_end_clean();
        return $grid;
    }
    add_shortcode('noo_menu_grid','noo_shortcode_menu_grid');
endif;

?>