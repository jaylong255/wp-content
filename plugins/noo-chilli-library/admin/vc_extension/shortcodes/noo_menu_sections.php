<?php
// [Noo Menu Section]
// ============================
if( !function_exists('noo_shortcode_all_menu') ){
    function noo_shortcode_all_menu($attrs){
        extract(shortcode_atts(array(
            'cat'              =>  '',
            'limit'            =>  '',
            'background_color' =>  'white',
        ),$attrs));

        $style_bg = ( $background_color ) ? 'style="background-color: ' . $background_color . '"' : '';

        ob_start();
        ?>
            <ul class="menu-sections" <?php echo noo_html_content_filter( $style_bg ); ?>>
                <?php
                    if( $cat == '' || $cat == 'all' ):
                        $args_cat = array(
                            'taxonomy' => 'menu_sections',
                        );
                        $category = get_categories($args_cat);
                        
                        foreach( $category as $catt ){
                            $new_arr_cate[] = $catt->term_id;
                        }
                        $cat = @implode(',', $new_arr_cate);
                    endif;
                    
                    if( isset($cat) && !empty($cat) ):
                        $categories = explode(',',$cat);
                        foreach( $categories as $cat_id ):
                           $term =  get_term_by('id', esc_attr($cat_id), 'menu_sections');
                            $image_term = noo_get_term_meta($cat_id,'heading_image');
                ?>
                <li>
                    <div class="term-thumbnail">
                        <?php echo wp_get_attachment_image(esc_attr($image_term),'large'); ?>
                    </div>
                    <h3 class="cat-title"><span class="header-title"><?php echo esc_html($term->name);?></span></h3>
                    <?php
                        $args = array(
                            'post_type'         =>  'food_menu',
                            'orderby'           =>  'date',
                            'order'             =>  'desc',
                            'tax_query'         =>  array(
                                array(
                                    'taxonomy'      =>  'menu_sections',
                                    'field'         =>  'term_id',
                                    'terms'         =>  $cat_id
                                )
                            )
                        );
                        if ( !empty($limit) ) 
                            $args['posts_per_page'] = $limit;
                        
                        $query = new WP_Query($args);
                        if( $query->have_posts() ):
                            while( $query->have_posts() ):
                                $query->the_post();
                                $attr  = noo_get_post_meta(get_the_ID(),'_noo_wp_food_attributes');
                                $price = noo_get_post_meta(get_the_ID(),'_noo_wp_food_price');
                    ?>
                    <div class="menu-content">
                        <h6><?php the_title(); ?></h6>
                        <div class="menu-attributes">
                            <span class="left pull-left">
                                <span class="attr" <?php echo noo_html_content_filter( $style_bg ); ?>><?php echo esc_html($attr); ?></span>
                            </span>
                            <span class="pull-right price" <?php echo noo_html_content_filter( $style_bg ); ?>>
                                <?php echo esc_html($price); ?>
                            </span>
                        </div>
                    </div>
                    <?php endwhile; ?>
                    <?php endif; wp_reset_postdata(); ?>
                </li>
                    <?php endforeach; ?>
                <?php endif; ?>
            </ul>
        <?php
        $menu = ob_get_contents();
        ob_end_clean();
        return $menu;
    }
    add_shortcode('noo_menu_sections','noo_shortcode_all_menu');
}

?>