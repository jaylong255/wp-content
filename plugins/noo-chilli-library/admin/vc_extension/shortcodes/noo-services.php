<?php
// [Noo Services]
// ============================
if( !function_exists('noo_shortcode_services') ){
    function noo_shortcode_services($attrs){
        extract(shortcode_atts(array(
            'bg_color'       =>  'white',
            'icon_style'     =>  1,
            'icon'           =>  '',
            'background_img' =>  '',
            'title'          =>  ''
        ),$attrs));
        ob_start();

        $style_bg = 'style="';
        if( isset($background_img) && $background_img != '' && $icon_style == 1 ) {
            $style_bg .= 'background-image: url(' . esc_url( wp_get_attachment_url($background_img) ) . ');';
        }
        $style_bg .= '"';
        ?>
        <div class="noo-services-square">
            <div class="services-square-icon" style="background-color: <?php echo esc_attr( $bg_color ); ?>">
                <span class="bg-icon"></span>
                <span class="icon" <?php echo $style_bg; ?>>
                <?php if ( $icon_style == 2 ) : ?>
                    <i class="<?php echo esc_html($icon); ?>"></i>
                <?php endif; ?>
                </span>
                <h3><?php echo esc_html($title) ?></h3>
            </div>
        </div>
        <?php
        $services = ob_get_contents();
        ob_end_clean();
        return $services;

    }
    add_shortcode('noo_services','noo_shortcode_services');
}

?>