<?php
// [Noo Menu Section Slider]
// ============================
if( !function_exists('noo_shortcode_menu_sections_slider') ){

    function noo_shortcode_menu_sections_slider($attrs){
        extract(shortcode_atts(array(
            'cat'              =>  '',
            'limit'            =>  6,
            'slider'           =>  false,
            'limit_slider'     =>  8,
            'autoplay'         =>  'false',
            'excerpt_length'   => 80,
            'background_color' =>  'white',
        ),$attrs));
        ob_start();
        
        $style_bg = ( $background_color ) ? 'style="background-color: ' . $background_color . '"' : '';
        
        wp_enqueue_script( 'owl.carousel' );
        ?>
            <div class="noo-menu-with-slider" <?php echo noo_html_content_filter( $style_bg ); ?>>
            <?php

                if( $cat == '' || $cat == 'all' ):
                    $args_cat = array(
                        'taxonomy' => 'menu_sections',
                    );
                    $category = get_categories($args_cat);
                    
                    foreach( $category as $catt ){
                        $new_arr_cate[] = $catt->term_id;
                    }
                    $cat = @implode(',', $new_arr_cate);
                endif;

                if( isset($cat) && !empty($cat) ):
                    $categories = explode(',',$cat);
                    foreach( $categories as $cat_id ):
                       $term =  get_term_by('id', esc_attr($cat_id), 'menu_sections');
            ?>
                <div class="container">
                    <h2 class="cat-title"><?php echo esc_html($term->name);?></h2>
                    <div class="row row-menu">
                    <?php
                        $args = array(
                            'post_type'         =>  'food_menu',
                            'orderby'           =>  'date',
                            'order'             =>  'desc',
                            'posts_per_page'    =>  $limit,
                            'tax_query'         =>  array(
                                array(
                                    'taxonomy'      =>  'menu_sections',
                                    'field'         =>  'term_id',
                                    'terms'         =>  $cat_id
                                )
                            )
                        );
                        $query = new WP_Query($args);
                        if( $query->have_posts() ):
                            while( $query->have_posts() ):
                                $query->the_post();
                                $attr  = noo_get_post_meta(get_the_ID(),'_noo_wp_food_attributes');
                                $price = noo_get_post_meta(get_the_ID(),'_noo_wp_food_price');
                    ?>
                            <div class="menu-item col-md-4 col-sm-6">
                                <div class="menu-content">
                                    <h3><?php the_title(); ?></h3>
                                    <div class="menu-attributes">
                                        <span class="left pull-left">
                                            <span class="attr" <?php echo noo_html_content_filter( $style_bg ); ?>><?php echo esc_html($attr); ?></span>
                                        </span>
                                        <span class="pull-right price" <?php echo noo_html_content_filter( $style_bg ); ?>>
                                            <?php echo esc_html($price); ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    <?php endif; wp_reset_postdata(); ?>
                    </div> <!-- /.row -->
                </div> <!-- /.container -->
                
                <div class="noo-gallery-slider">
                <?php if ( $slider ) : ?>
                    <?php
                        $args_slider = array(
                            'post_type'         =>  'food_menu',
                            'orderby'           =>  'date',
                            'order'             =>  'desc',
                            'posts_per_page'    =>  $limit_slider,
                            'tax_query'         =>  array(
                                array(
                                    'taxonomy'      =>  'menu_sections',
                                    'field'         =>  'term_id',
                                    'terms'         =>  $cat_id
                                )
                            )
                        );
                        $r = new WP_Query($args_slider);
                    ?>
                    <?php if ( $r->have_posts() ) : ?>
                        <?php while ( $r->have_posts() ) : $r->the_post(); ?>
                            <?php $price = noo_get_post_meta(get_the_ID(),'_noo_wp_food_price'); ?>
                            <div class="noo-item-slider">
                                <?php if ( has_post_thumbnail() )
                                        the_post_thumbnail('noo-thumbnail-square'); ?>
                                <div class="info-overlay">
                                    <h4><?php the_title(); ?></h4>
                                    <p><?php echo wp_trim_words(get_the_content(), $excerpt_length, '...'); ?></p>
                                    <span class="view-price"><?php echo esc_html($price); ?></span>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    <?php endif; ?>
                <?php endif; ?>
                </div> <!-- /.noo-gallery-slider -->
                <?php endforeach; ?>
            <?php endif; ?>
            </div> <!-- /.noo-menu-with-slider -->
        <script>
            jQuery(document).ready(function(){
                jQuery('.noo-gallery-slider').each(function(){
                    jQuery(this).owlCarousel({
                        items : 4,
                        itemsDesktop : [1199,3],
                        itemsDesktopSmall : [979,2],
                        itemsTablet: [768, 2],
                        itemsMobile: [479, 1],
                        slideSpeed:500,
                        paginationSpeed:1000,
                        rewindSpeed:1000,
                        autoHeight: false,
                        addClassActive: true,
                        autoPlay: <?php echo esc_attr($autoplay); ?>,
                        loop:true,
                        pagination: false
                    });
                });
            });
        </script>
        <?php
        $menu = ob_get_contents();
        ob_end_clean();
        return $menu;
    }
    add_shortcode('noo_menu_sections_slider','noo_shortcode_menu_sections_slider');

}

?>