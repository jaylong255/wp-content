<?php
// [Noo Gallery]
// ============================
if( !function_exists('noo_shortcode_gallery') ){
    function noo_shortcode_gallery($attrs){
        extract(shortcode_atts(array(
            'title'          =>  '',
            'button_name'    =>  esc_html__('View more', 'noo-chilli' ),
            'button_link'    =>  '',
            'cat'            =>  '',
            'limit'          =>  8,
            'autoplay'       =>  'true',
            'style'          =>  'full',
            'orderby'        =>  'date',
            'order'          =>  'DESC',
            'excerpt_length' => 15,
            'white_style'    => ''
        ),$attrs));

        $args = array(
            'post_type'      =>  'gallery',
            'orderby'        =>  'id',
            'order'          =>  'asc',
            'posts_per_page' =>  $limit,
        );
        
        if ( isset($cat) && !empty($cat) ) {
            $categories = explode(',', $cat);

            $args['tax_query'][]  = array(
                'taxonomy' =>  'gallery_categories',
                'field'    =>  'term_id',
                'terms'    =>   $categories
            );
        }
        $r = new WP_Query( $args );
        ob_start();
        
        wp_enqueue_script( 'owl.carousel' );
        wp_enqueue_script( 'vendor-nivo-lightbox-js' );

        $class_white = ( $white_style ) ? 'noo-menu-today-dark' : '';

        ?>
            <div class="noo-menu-today-wrap noo-gallery-gothic <?php echo esc_attr( $class_white ); ?>">
                <div class="container">
                    <div class="noo-menu-today only-top">
                        <div class="menu-today-title">
                            <h3 class="noo-title header-title"><span><?php echo esc_html($title) ?></span></h3>
                        </div>
                        <div class="menu-today-content"></div>
                    </div>
                </div>
            </div>
            
            <div class="noo-gallery-slider">
            <?php if ( $r->have_posts() ) : ?>
                <?php while ( $r->have_posts() ) : $r->the_post(); ?>
                    <?php $href = wp_get_attachment_url( get_post_thumbnail_id( esc_attr( get_the_ID() ) ) ); ?>
                    <div class="noo-item-slider">
                        <?php if ( has_post_thumbnail() )
                                the_post_thumbnail('noo-thumbnail-square'); ?>
                        <?php if ( $style == 'full' ) : ?>
                        <div class="info-overlay">
                            <h4><?php the_title(); ?></h4>
                            <p><?php echo wp_trim_words(get_the_content(), $excerpt_length, '...'); ?></p>
                            <a class="view-detail noo-tngallery" data-lightbox-gallery="gallery1" href="<?php echo esc_url($href); ?>">
                                <span><i class="fa fa-search"></i></span>
                            </a>
                        </div>    
                        <?php endif;  ?>
                        <?php if ( $style == 'text_only' ) : ?>
                        <a class="info-overlay noo-tngallery" data-lightbox-gallery="gallery1" href="<?php echo esc_url($href); ?>">
                            <h4><?php the_title(); ?></h4>
                            <p><?php echo wp_trim_words(get_the_content(), $excerpt_length, '...'); ?></p>
                        </a>
                        <?php endif; ?>
                        <?php if ( $style == 'icon_only' ) : ?>
                        <a class="info-overlay noo-tngallery" data-lightbox-gallery="gallery1" href="<?php echo esc_url($href); ?>">
                           <span class="view-detail icon_only">
                               <span><i class="fa fa-search"></i></span>
                           </span>
                        </a>    
                        <?php endif;  ?>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
            </div>

            <div class="noo-menu-today-wrap noo-gallery-gothic <?php echo esc_attr( $class_white ); ?>">
                <div class="container">
                    <div class="noo-menu-today only-bottom">
                        <div class="menu-today-content">
                            <div class="btn-view-all"><a href="<?php echo esc_url($button_link) ?>"><span><?php echo esc_html($button_name); ?></span></a></div>
                        </div> <!-- /.menu-today-content -->
                    </div>
                </div>
            </div>
        
        <script>
            jQuery(document).ready(function(){
                jQuery('.noo-tngallery').nivoLightbox({
                    effect: 'slideUp'
                });
                jQuery('.noo-gallery-slider').each(function(){
                    jQuery(this).owlCarousel({
                        items : 4,
                        itemsDesktop : [1199,3],
                        itemsDesktopSmall : [979,2],
                        itemsTablet: [768, 2],
                        itemsMobile: [479, 1],
                        slideSpeed: 500,
                        paginationSpeed:1000,
                        rewindSpeed: 1000,
                        autoHeight: false,
                        addClassActive: true,
                        // autoPlay: <?php echo esc_attr($autoplay); ?>,
                        autoPlay: false,
                        loop:true,
                        pagination: false
                    });
                });
            });
        </script>
        <?php
        $menu = ob_get_contents();
        ob_end_clean();
        return $menu;
    }
    add_shortcode('noo_gallery','noo_shortcode_gallery');
}

?>