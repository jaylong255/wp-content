<?php
// [Noo Team]
// ============================
if( !function_exists('noo_shortcode_team') ){
    function noo_shortcode_team($attrs){
        extract(shortcode_atts(array(
            'categories'   =>  '',
            'limit'        =>  '4',
            'columns'      =>  '1',
            'orderby'      =>  'latest',
            'layout_style' =>  'default'
        ),$attrs));
        ob_start();
        $order = 'DESC';
        switch ($orderby) {
            case 'latest':
                $orderby = 'date';
                break;

            case 'oldest':
                $orderby = 'date';
                $order = 'ASC';
                break;

            case 'alphabet':
                $orderby = 'title';
                $order = 'ASC';
                break;

            case 'ralphabet':
                $orderby = 'title';
                break;

            default:
                $orderby = 'date';
                break;
        }
        ?>
            <div class="noo_team">
                <div class="container">
                    <div class="row">
                        <?php
                        $args = array(
                            'post_type'         =>  'team_member',
                            'posts_per_page'     =>  $limit
                        );
                        if( $categories != 'all' && $categories != '' ){
                            $args['tax_query'][]  = array(
                                'taxonomy'  =>  'team_category',
                                'field'     =>  'id',
                                'terms'     =>   array($categories)
                            );
                        }
                        $query = new WP_Query($args);
                        if( $query->have_posts() ):
                            while( $query->have_posts() ):
                                $query->the_post();
                                $image_id = get_post_meta(get_the_ID(),'_noo_wp_team_image', true);
                                $name     = get_post_meta(get_the_ID(),'_noo_wp_team_name', true);
                                $position = get_post_meta(get_the_ID(),'_noo_wp_team_position', true);

                                $facebook = get_post_meta(get_the_ID(),'_noo_wp_team_facebook', true);
                                $twitter  = get_post_meta(get_the_ID(),'_noo_wp_team_twitter', true);
                                $google   = get_post_meta(get_the_ID(),'_noo_wp_team_google', true);
                                $linkedin = get_post_meta(get_the_ID(),'_noo_wp_team_linkedin', true);

                                $flickr     = get_post_meta(get_the_ID(),'_noo_wp_team_flickr', true);
                                $pinterest  = get_post_meta(get_the_ID(),'_noo_wp_team_pinterest', true);
                                $instagram  = get_post_meta(get_the_ID(),'_noo_wp_team_instagram', true);
                                $tumblr     = get_post_meta(get_the_ID(),'_noo_wp_team_tumblr', true);
                                ?>
                                <div class="col-md-<?php echo absint((12 / esc_attr($columns))) ?> col-sm-6">
                                    <div class="noo_team_item <?php echo esc_attr( $layout_style ); ?>">
                                        <div class="team_thumbnail">
                                            <?php echo wp_get_attachment_image(esc_attr($image_id),'noo-thumbnail-square'); ?>
                                            
                                        </div>
                                        <h4 class="team_name"><?php echo esc_html($name); ?></h4>
                                        <span class="team_position"><?php echo esc_html($position); ?></span>
                                        <div class="team_social">
                                            <?php if( isset($facebook) && $facebook !='' ): ?>
                                                <a href="<?php echo esc_url($facebook) ?>" class="fa fa-facebook"></a>
                                            <?php endif; ?>
                                            <?php if( isset($twitter) && $twitter !='' ): ?>
                                                <a href="<?php echo esc_url($twitter) ?>" class="fa fa-twitter"></a>
                                            <?php endif; ?>
                                            <?php if( isset($google) && $google !='' ): ?>
                                                <a href="<?php echo esc_url($google) ?>" class="fa fa-google-plus"></a>
                                            <?php endif; ?>
                                            <?php if( isset($linkedin) && $linkedin !='' ): ?>
                                                <a href="<?php echo esc_url($linkedin) ?>" class="fa fa-linkedin"></a>
                                            <?php endif; ?>

                                            <?php if( isset($flickr) && $flickr !='' ): ?>
                                                <a href="<?php echo esc_url($flickr) ?>" class="fa fa-flickr"></a>
                                            <?php endif; ?>
                                            <?php if( isset($pinterest) && $pinterest !='' ): ?>
                                                <a href="<?php echo esc_url($pinterest) ?>" class="fa fa-pinterest"></a>
                                            <?php endif; ?>
                                            <?php if( isset($instagram) && $instagram !='' ): ?>
                                                <a href="<?php echo esc_url($instagram) ?>" class="fa fa-instagram"></a>
                                            <?php endif; ?>
                                            <?php if( isset($tumblr) && $tumblr !='' ): ?>
                                                <a href="<?php echo esc_url($tumblr) ?>" class="fa fa-tumblr"></a>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            endwhile;
                        endif;
                        wp_reset_postdata();
                        ?>
                    </div>
                </div>
            </div>
        <?php
        $team = ob_get_contents();
        ob_end_clean();
        return $team;
    }
    add_shortcode('noo_team','noo_shortcode_team');
}

?>