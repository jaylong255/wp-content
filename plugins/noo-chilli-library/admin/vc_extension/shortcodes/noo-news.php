<?php
// [Noo News]
// ============================
if( !function_exists('noo_shortcode_news') ) :
    function noo_shortcode_news($atts){
        extract(shortcode_atts(array(
            'style'             =>  'masonry',
            'type_query'        =>  'cate',
            'categories'        =>  '',
            'tags'              =>  '',
            'include'           =>  '',
            'orderby'           =>  'latest',
            'limit'             =>  3,
            'limit_excerpt'     =>  20,
            'button_name'       =>  'View more news',
            'button_link'       =>  '#'
        ),$atts));
        wp_enqueue_script('vendor-imagesloaded');
        wp_enqueue_script('isotope');
        wp_enqueue_script('news-grid');
        ob_start();

        $order = 'DESC';
        switch ($orderby) {
            case 'latest':
                $orderby = 'date';
                break;
            case 'oldest':
                $orderby = 'date';
                $order = 'ASC';
                break;
            case 'alphabet':
                $orderby = 'title';
                $order = 'ASC';
                break;
            case 'ralphabet':
                $orderby = 'title';
                break;
            default:
                $orderby = 'date';
                break;
        }
        $args = array(
            'orderby'           =>   $orderby,
            'order'             =>   $order,
            'posts_per_page'    =>   $limit
        );
        if($type_query == 'cate'){
            $args['cat']   =  $categories ;
        }
        if($type_query == 'tag'){
            if($tags != 'all'):
                $tag_id = explode (',' , $tags);
                $args['tag__in'] = $tag_id;
            endif;
        }
        if($type_query == 'post_id'){
            $posts_var = '';
            if ( isset($include) && !empty($include) ){
                $posts_var = explode (',' , $include);
            }
            $args['post__in'] = $posts_var;
        }
        $query = new WP_Query( $args );

        echo '<div class="noo_news_event">';
        
        if( $query->have_posts() ):
            if ( $style == 'masonry' ) :
                echo '<div class="noo-news-wrap">';
                while( $query->have_posts() ): $query->the_post();    
                ?>
                    <div class="news-grid-item col-md-4 col-sm-12">
                        <div class="news-thumb">
                            <?php if ( has_post_thumbnail() ) : ?>
                                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('large'); ?></a>
                            <?php endif; ?>
                        </div>
                        <div class="news-content">
                            <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                            <span class="news-author-cm"><?php echo esc_html__( 'Written by ', 'noo-chilli' ).'<span class="author-highlight">'.get_the_author().'</span>'.' on '.get_the_date(); ?></span>
                            <?php
                            $excerpt = get_the_content();
                            $excerpt = strip_shortcodes($excerpt);
                            echo '<p class="news-ds">' . wp_trim_words($excerpt,esc_attr($limit_excerpt)) . '</p>';
                            ?>
                            <a class="readmore" href="<?php the_permalink() ?>"><?php echo esc_html__('Read More', 'noo-chilli' ); ?></a>
                        </div>
                    </div>
                <?php
                endwhile;
                echo '</div>';
            else :
                echo '<div><ul class="noo_news">';
                while( $query->have_posts() ): $query->the_post();
                    $url = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'noo-thumbnail-square');
                ?>
                    <li>
                        <div class="news-thumbnail">
                            <div class="new-bk" style="background-image: url('<?php echo esc_url($url[0]); ?>')"></div>
                        </div>
                        <div class="news-content">
                            <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                            <?php
                            $excerpt = get_the_content();
                            $excerpt = strip_shortcodes($excerpt);
                            echo '<p class="news-ds">' . wp_trim_words($excerpt,esc_attr($limit_excerpt)) . '</p>';
                            ?>
                            <a class="readmore" href="<?php the_permalink() ?>"><?php echo esc_html__('Read More', 'noo-chilli' ); ?></a>
                        </div>
                    </li>
                <?php
                endwhile;
                echo '</ul></div>';
            endif;
            // Show button view more
            if ( $button_name != '' ) :
            ?>
                <div class="btn-view-all"><a href="<?php echo esc_url($button_link) ?>"><span><?php echo esc_html($button_name); ?></span></a></div>
            <?php
            endif;
        endif; wp_reset_postdata();
        echo '</div>';
        ?>

        <?php
        $news = ob_get_contents();
        ob_end_clean();
        return $news;
    }
    add_shortcode('noo_news','noo_shortcode_news');

endif;

?>