<?php
// [Noo OpenTable Reservation]
// ============================
if( !function_exists('noo_shortcode_opentable_reservation') ){
    function noo_shortcode_opentable_reservation($attrs){
        extract(shortcode_atts(array(
            'style'        =>  2,
            'opentable_id' =>  '',
            'title'        => 'Make a Reservation',
            'domain_ext'   => 'com',
            'date_format'  => 'MM/DD/YYYY', //this can be overwritten by the user
        ),$attrs));
        ob_start();

        $class_font = ( $style == 2 ) ? 'gray' : '';

        wp_enqueue_script('jquery-ui-custom');
        wp_enqueue_script('jquery.ui.timepicker');
        ?>
        <div class="noo-customform <?php echo esc_attr( $class_font ); ?>">
            <h2 class="book-title"><?php echo esc_html( $title ); ?></h2>
            <div class="book-form container">
                <form action="http://www.opentable.<?php echo esc_attr( $domain_ext );?>/restaurant-search.aspx" target="_blank" method="get">
                    <div class="noo-form">
                        <div class="noo-form-content">
                            <div class="book-form-field">
                                <span class="wpcf7-form-control-wrap reservation-date">
                                    <input type="text" name="startDate" value="" size="40" class="wpcf7-form-control wpcf7-text" id="noo_datepicker" placeholder="<?php echo esc_html__('Which Date?', 'noo-chilli' ); ?>">
                                </span>
                            </div>
                            <div class="book-form-field">
                                <span class="wpcf7-form-control-wrap reservation-time">
                                    <input type="text" name="ResTime" value="19:00" size="40" class="wpcf7-form-control wpcf7-text" id="noo_timepicker" placeholder="<?php echo esc_html__('Which Time?', 'noo-chilli' ); ?>">
                                </span>
                            </div>
                            <div class="book-form-field">
                                <span class="wpcf7-form-control-wrap reservation-guests">
                                    <select id="party-otreservations" name="partySize">
                                        <option value="1"><?php esc_html_e('1 Person', 'noo-chilli' ); ?></option>
                                        <option value="2" selected="selected"><?php esc_html_e('2 People', 'noo-chilli' ); ?></option>
                                        <option value="3"><?php esc_html_e('3 People', 'noo-chilli' ); ?></option>
                                        <option value="4"><?php esc_html_e('4 People', 'noo-chilli' ); ?></option>
                                        <option value="5"><?php esc_html_e('5 People', 'noo-chilli' ); ?></option>
                                        <option value="6"><?php esc_html_e('6 People', 'noo-chilli' ); ?></option>
                                        <option value="7"><?php esc_html_e('7 People', 'noo-chilli' ); ?></option>
                                        <option value="8"><?php esc_html_e('8 People', 'noo-chilli' ); ?></option>
                                        <option value="9"><?php esc_html_e('9 People', 'noo-chilli' ); ?></option>
                                        <option value="10"><?php esc_html_e('10 People', 'noo-chilli' ); ?></option>
                                    </select>
                                </span>
                            </div>
                            <div class="book-form-field book-form-submit book-form-opentable"><input type="submit" value="<?php echo esc_html__('Find a Table', 'noo-chilli' ); ?>" class="wpcf7-form-control wpcf7-submit"></div>
                            <input type="hidden" name="RestaurantID" class="RestaurantID" value="<?php echo esc_attr( $opentable_id ); ?>">
                            <input type="hidden" name="rid" class="rid" value="<?php echo esc_attr( $opentable_id ); ?>">
                            <input type="hidden" name="GeoID" class="GeoID" value="15">
                            <input type="hidden" name="txtDateFormat" class="txtDateFormat" value="<?php echo ! empty( $date_format ) ? esc_attr( $date_format ) : "MM/DD/YYYY"; ?>">
                            <input type="hidden" name="RestaurantReferralID" class="RestaurantReferralID" value="<?php echo esc_attr( $opentable_id ); ?>">
                        </div>
                    </div>        
                </form>
            </div>
        </div>
        <?php $start_of_week = (int)get_option('start_of_week'); ?>
        <script>
            jQuery(document).ready(function(){
                jQuery('#noo_timepicker').timepicker( {
                    showAnim: 'blind'
                } );
                jQuery( "#noo_datepicker" ).datepicker({
                    minDate: 0,
                    firstDay: <?php echo esc_attr( $start_of_week ); ?>
                }).datepicker("setDate", new Date());
            });
        </script>
        <?php
        $form = ob_get_contents();
        ob_end_clean();
        return $form;

    }
    add_shortcode('noo_opentable_reservation','noo_shortcode_opentable_reservation');
}
?>