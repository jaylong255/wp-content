<?php
// [Noo Gallery Grid]
// ============================
if( !function_exists('noo_shortcode_gallery_grid') ){
    function noo_shortcode_gallery_grid($attrs){
        extract(shortcode_atts(array(
            'style'          =>  'full',
            'hide_filter'    =>  false,
            'cat'            =>  '',
            'limit'          =>  8,
            'orderby'        =>  'date',
            'order'          =>  'DESC',
            'excerpt_length' => 15
        ),$attrs));
        ob_start();

        $order = 'DESC';
        switch ($orderby) {
            case 'latest':
                $orderby = 'date';
                break;

            case 'oldest':
                $orderby = 'date';
                $order = 'ASC';
                break;

            case 'alphabet':
                $orderby = 'title';
                $order = 'ASC';
                break;

            case 'ralphabet':
                $orderby = 'title';
                break;

            default:
                $orderby = 'date';
                break;
        }

        if( is_front_page() || is_home()) :
            $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : ( ( get_query_var( 'page' ) ) ? get_query_var( 'page' ) : 1 );
        else :
            $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
        endif;

        $args = array(
            'post_type'           =>  'gallery',
            'orderby'             =>  $orderby,
            'order'               =>  $order,
            'paged'               => $paged,
            'ignore_sticky_posts' => 1,
            'posts_per_page'      =>  $limit,
        );

        if ( isset($cat) && !empty($cat) ) {
            $categories = explode(',', $cat);
            $args['tax_query'][]  = array(
                'taxonomy' =>  'gallery_categories',
                'field'    =>  'term_id',
                'terms'    =>   $categories
            );
        }
        $r = new WP_Query( $args );
        
        wp_enqueue_script('vendor-imagesloaded');
        wp_enqueue_script('isotope');
        wp_enqueue_script('gallery-grid');
        wp_enqueue_script( 'vendor-nivo-lightbox-js' );
        
        ?>
        <div class="masonry">
            <?php
                if( !$hide_filter ) :
                    $args_cat = array(
                        'type'                     => 'gallery',
                        'child_of'                 => 0,
                        'parent'                   => '',
                        'orderby'                  => 'name',
                        'order'                    => 'DESC',
                        'hide_empty'               => 1,
                        'hierarchical'             => 1,
                        'exclude'                  => '',
                        'include'                  => '',
                        'number'                   => '',
                        'taxonomy'                 => 'gallery_categories',
                        'pad_counts'               => false,
                        'terms'                    => $categories
                    );
                    $category = get_categories($args_cat);
                    if( isset($category) && !empty($category) ):
            ?>
                    <div class="masonry-filters filter-gallery">
                        <ul data-option-key="filter">
                            <li>
                                <a data-option-value="*" href="#all" class="selected"><?php echo esc_html__('All', 'noo-chilli' ); ?></a>
                            </li>
                            <?php foreach( $category as $cat ): ?>
                                <li>
                                    <a data-option-value=".<?php echo esc_attr($cat->slug); ?>" href="#<?php echo esc_attr($cat->name); ?>"><?php echo esc_html($cat->name); ?></a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                <?php endif; ?>
            <?php endif; ?>

            <div class="noo-gallery-grid">
                <div class="row masonry-container">
                <?php if ( $r->have_posts() ) : ?>
                    <?php while ( $r->have_posts() ) : $r->the_post(); ?>
                        <?php
                        $href = wp_get_attachment_url( get_post_thumbnail_id( esc_attr( get_the_ID() ) ) );
                        $term_cat = get_the_terms(get_the_ID(), 'gallery_categories');
                        $class_term = '';
                        if( isset($term_cat) && !empty($term_cat) ){
                            foreach($term_cat as $term):
                                $class_term .= ' '.$term->slug;
                            endforeach;
                        }
                        ?>
                        <div class="col-md-3 col-sm-4 col-xs-6 noo-item-slider loadmore-item masonry-item <?php echo esc_attr($class_term); ?>">
                                <?php if ( has_post_thumbnail() )
                                    the_post_thumbnail('noo-thumbnail-square'); ?>
                            <?php if ( $style == 'full' ) : ?>
                            <div class="info-overlay">
                                <h4><?php the_title(); ?></h4>
                                <p><?php echo wp_trim_words(get_the_content(), $excerpt_length, '...'); ?></p>
                                <a class="view-detail noo-tngallery" data-lightbox-gallery="gallery1" href="<?php echo esc_url($href); ?>">
                                    <span><i class="fa fa-search"></i></span>
                                </a>
                            </div>    
                            <?php endif;  ?>
                            <?php if ( $style == 'text_only' ) : ?>
                            <a class="info-overlay noo-tngallery" data-lightbox-gallery="gallery1" href="<?php echo esc_url($href); ?>">
                                <h4><?php the_title(); ?></h4>
                                <p><?php echo wp_trim_words(get_the_content(), $excerpt_length, '...'); ?></p>
                            </a>
                            <?php endif; ?>
                            <?php if ( $style == 'icon_only' ) : ?>
                            <a class="info-overlay noo-tngallery" data-lightbox-gallery="gallery1" href="<?php echo esc_url($href); ?>">
                               <span class="view-detail icon_only"><i class="fa fa-search"></i></span>
                            </a>    
                            <?php endif;  ?>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
                </div> <!-- /.row -->
            </div>

            <?php if(1 < $r->max_num_pages):?>
                <div class="loadmore-action btn-view-all btn-gallery-loadmore">
                    <a href="#" class="btn-loadmore btn-gallery-loadmore" title="<?php esc_html_e('Load More', 'noo-chilli' )?>"><span><?php esc_html_e('Load More', 'noo-chilli' )?></span></a>
                    <div class="noo-loader loadmore-loading"><span></span><span></span><span></span><span></span><span></span></div>
                </div>
                <?php echo  noo_custom_paging_nav($r->max_num_pages);  ?>
            <?php endif;?>
        </div>

        <script>
            jQuery(document).ready(function(){
                jQuery('.noo-tngallery').nivoLightbox({
                    effect: 'slideUp'
                });
            });
        </script>
        <?php
        $menu = ob_get_contents();
        ob_end_clean();
        return $menu;

    }
    add_shortcode('noo_gallery_grid','noo_shortcode_gallery_grid');
}

?>