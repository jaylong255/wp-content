<?php
// [Noo Menu]
// ============================
if( !function_exists('noo_shortcode_food_menu') ) :
    function noo_shortcode_food_menu($attrs){
        extract(shortcode_atts(array(
            'menu_title'       =>  '',
            'cat'              =>  '',
            'limit'            =>  4,
            'column'           =>  3,
            'button_name'      =>  'View more',
            'button_link'      =>  '#',
            'background_color' =>  'white',
            'show_img_bottom'  =>  false,
            'img_bottom'       =>  '',
            'img_bottom_size'  =>  '260',
        ),$attrs));
        ob_start();
        
        $style_bg = ( $background_color ) ? 'style="background-color: ' . $background_color . '"' : '';
        ?>
            <div class="noo-menu-today-wrap" <?php echo noo_html_content_filter( $style_bg ); ?>>
                <div class="container">
                    <div class="noo-menu-today">
                        <div class="menu-today-title">
                            <h3 class="noo-title header-title" <?php echo noo_html_content_filter( $style_bg ); ?>><span><?php echo esc_html($menu_title) ?></span></h3>
                        </div>
                        <div class="menu-today-content">
                            <div class="row">
                            <?php
                                if( $cat == '' || $cat == 'all' ):
                                    $args_cat = array(
                                        'taxonomy' => 'menu_sections',
                                    );
                                    $category = get_categories($args_cat);
                                    
                                    foreach( $category as $catt ){
                                        $new_arr_cate[] = $catt->term_id;
                                    }
                                    $cat = @implode(',', $new_arr_cate);
                                endif;

                                if( isset($cat) && !empty($cat) ):
                                    $categories = explode(',',$cat);
                                    switch ( $column ) {
                                        case 2  : $class = 'col-md-6 col-sm-12';  break;
                                        case 1  : $class = 'col-md-12'; break;
                                        default : $class = 'col-md-4 col-sm-12'; break;
                                    }
                                    foreach( $categories as $k => $cat_id ):
                                        $term =  get_term_by('id', esc_attr($cat_id), 'menu_sections');
                            ?>
                                <div class="col-menu-today <?php echo $class; ?>">
                                    <h4 class="cat-title"><?php echo esc_html($term->name);?></h4>
                                    <?php
                                    $args = array(
                                        'post_type'         =>  'food_menu',
                                        'orderby'           =>  'date',
                                        'order'             =>  'desc',
                                        'posts_per_page'    =>  $limit,
                                        'tax_query'         =>  array(
                                            array(
                                                'taxonomy'      =>  'menu_sections',
                                                'field'         =>  'term_id',
                                                'terms'         =>  array($cat_id)
                                            )
                                        )
                                    );
                                    $query = new WP_Query($args);
                                    if( $query->have_posts() ):
                                        while( $query->have_posts() ):
                                            $query->the_post();
                                            $attr  = noo_get_post_meta(get_the_ID(),'_noo_wp_food_attributes');
                                            $price = noo_get_post_meta(get_the_ID(),'_noo_wp_food_price');
                                    ?>
                                        <div class="menu-content">
                                            <h5><?php the_title(); ?></h5>
                                            <div class="menu-attributes">
                                                <span class="left pull-left">
                                                    <span class="attr" <?php echo noo_html_content_filter( $style_bg ); ?>><?php echo esc_html($attr); ?></span>
                                                </span>
                                                <span class="pull-right price" <?php echo noo_html_content_filter( $style_bg ); ?>>
                                                    <?php echo esc_html($price); ?>
                                                </span>
                                            </div>
                                        </div>
                                        <?php endwhile; ?>
                                    <?php endif; wp_reset_postdata(); ?>
                                </div> <!-- /.col-menu-today -->
                                <?php endforeach; ?>
                            <?php endif; ?>
                            </div> <!-- /.row -->
                            <div class="btn-view-all"><a href="<?php echo esc_url($button_link) ?>"><span><?php echo esc_html($button_name); ?></span></a></div>
                        </div> <!-- /.menu-today-content -->
                    </div>
                    <?php
                        $style_bottom = 'style="';
                        if( isset($img_bottom) && $img_bottom != '' && $show_img_bottom ) {
                            $style_bottom .= 'background-image: url(' . esc_url( wp_get_attachment_url($img_bottom) ) . ');';
                            $style_bottom .= ' height: ' . esc_attr( $img_bottom_size ) . 'px';
                        }
                        $style_bottom .= '"';
                    ?>
                    <div class="menu-bottom-img" <?php echo $style_bottom; ?>></div>
                </div>
            </div>
        <?php
        $menu = ob_get_contents();
        ob_end_clean();
        return $menu;
    }
    add_shortcode('noo_menu','noo_shortcode_food_menu');

endif;

?>