<?php
// [Noo Reservation]
// ============================
if( !function_exists('noo_shortcode_reservation') ){
    function noo_shortcode_reservation($attrs){
        extract(shortcode_atts(array(
            'title'         =>  '',
            'description'   =>  '',
            'info'          =>  ''
        ),$attrs));
        ob_start();
        ?>
        <div class="noo-reservation">
            <h3><?php echo esc_html($title); ?></h3>
            <p><?php echo esc_html($description); ?></p>
            <span class="info"><?php echo esc_html($info); ?></span>
        </div>
        <?php
        $reservation = ob_get_contents();
        ob_end_clean();
        return $reservation;
    }
    add_shortcode('noo_reservation','noo_shortcode_reservation');
}
?>