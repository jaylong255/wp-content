<?php
// [Noo Form 7]
// ============================
if( !function_exists('noo_shortcode_form') ){
    function noo_shortcode_form($attrs){
        extract(shortcode_atts(array(
            'style'       =>  2,
            'title'       =>  '',
            'description' =>  '',
            'custom_form' =>  ''
        ),$attrs));
        ob_start();

        $class_font = ( $style == 2 ) ? 'gray' : '';
        
        wp_enqueue_script('jquery-ui-custom');
        wp_enqueue_script('jquery.ui.timepicker');
        ?>
        <div class="noo-customform <?php echo esc_attr( $class_font ); ?>">
            <?php if ( !empty($title) ) : ?>
            <h2 class="book-title"><?php echo esc_html( $title ); ?></h2>
            <?php endif; ?>
            <div class="book-form container">
                <?php echo do_shortcode('[contact-form-7 id="'.esc_attr($custom_form).'"]'); ?>
            </div>
            <?php if ( $description != '' ) : ?>
            <p class="opentime"><?php printf( '<span>%1$s</span>%2$s', esc_html__( 'Opentime: ', 'noo-chilli' ), esc_html($description) ); ?></p>
            <?php endif; ?>
        </div>
        <?php $start_of_week = (int)get_option('start_of_week'); ?>
        <script>
            jQuery(document).ready(function(){
                jQuery('#noo_timepicker').timepicker( {
                    showAnim: 'blind'
                } );
                jQuery( "#noo_datepicker" ).datepicker({
                    minDate: 0,
                    firstDay: <?php echo esc_attr( $start_of_week ); ?>
                }).datepicker("setDate", new Date());
            });
        </script>
        <?php
        $form = ob_get_contents();
        ob_end_clean();
        return $form;

    }
    add_shortcode('noo_form','noo_shortcode_form');
}
?>