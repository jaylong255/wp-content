<?php
// [Noo Events]
// ============================
if( !function_exists('noo_shortcode_events') ) :
    function noo_shortcode_events($atts){
        extract(shortcode_atts(array(
            'cat'                 =>  '',
            'style'               =>  'grid',
            'columns'             =>  '1',
            'orderby'             =>  'latest',
            'show_date'           =>  'yes',
            'show_excerpt'        =>  'yes',
            'orderby'             =>  'latest',
            'limit'               =>  3,
            'limit_excerpt'       =>  20
        ),$atts));
        wp_enqueue_script('vendor-imagesloaded');
        wp_enqueue_script('isotope');
        wp_enqueue_script('news-grid');
        ob_start();

        $order = 'DESC';
        switch ($orderby) {
            case 'latest':
                $orderby = 'date';
                break;
            case 'oldest':
                $orderby = 'date';
                $order = 'ASC';
                break;
            case 'alphabet':
                $orderby = 'title';
                $order = 'ASC';
                break;
            case 'ralphabet':
                $orderby = 'title';
                break;
            default:
                $orderby = 'date';
                break;
        }
        $args = array(
            'post_type'         =>  'tribe_events',
            'orderby'           =>  $orderby,
            'order'             =>  $order,
            'posts_per_page'    =>  $limit,
        );
        $cat_id = '';
        if( isset($cat) && !empty($cat) && $cat != 'all'){
            $cat_id = explode(',',$cat);
            $args['tax_query'][]  =    array(
                'taxonomy'  =>  'tribe_events_cat',
                'field'     =>  'term_id',
                'terms'     =>   $cat_id
            );
        }
        $query = new WP_Query( $args );

        echo '<div class="noo_news_event grid-event">';
        
        if( $query->have_posts() ):
            echo '<div class="noo-news-wrap">';
            while( $query->have_posts() ): $query->the_post();    
            ?>
                <div class="news-grid-item col-md-<?php echo absint((12 / esc_attr($columns))) ?> col-sm-12">
                    <a href="<?php the_permalink(); ?>" class="news-thumb">
                        <?php if ( has_post_thumbnail() )
                                the_post_thumbnail('large'); ?>
                    </a>
                    <div class="news-content">
                        <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                        <?php if($show_date == 'yes'):?>
                        <span class="news-author-cm">
                            <?php  echo tribe_events_event_schedule_details( get_the_ID(), '', '' ); ?>
                        </span>
                        <?php endif; ?>
                        <?php if($show_excerpt == 'yes'):?>
                            <?php
                                $excerpt = get_the_content();
                                $excerpt = strip_shortcodes($excerpt);
                                echo '<p class="news-ds">' . wp_trim_words($excerpt,esc_attr($limit_excerpt)) . '</p>';
                            ?>
                        <?php endif; ?>
                    </div>
                </div>
            <?php
            endwhile;
            echo '</div>';
        endif; wp_reset_postdata();
        echo '</div>';
        ?>

        <?php
        $news = ob_get_contents();
        ob_end_clean();
        return $news;
    }
    add_shortcode('noo_events','noo_shortcode_events');

endif;

?>