<?php
if ( class_exists('WPBakeryVisualComposerAbstract') ):
    function nootheme_includevisual(){
        require_once PLUGIN_SERVER_PATH . '/admin/vc_extension/map/new_params.php';
        require_once PLUGIN_SERVER_PATH . '/admin/vc_extension/map/map.php';
        // VC Templates
        $vc_templates_dir = PLUGIN_SERVER_PATH . '/admin/vc_extension/shortcodes/vc_templates/';
        vc_set_shortcodes_templates_dir($vc_templates_dir);

        // require file
        require_once PLUGIN_SERVER_PATH . '/admin/vc_extension/shortcodes/noo-images.php';
        require_once PLUGIN_SERVER_PATH . '/admin/vc_extension/shortcodes/noo-title.php';
        require_once PLUGIN_SERVER_PATH . '/admin/vc_extension/shortcodes/noo-food-menu.php';
        require_once PLUGIN_SERVER_PATH . '/admin/vc_extension/shortcodes/noo-news.php';
        require_once PLUGIN_SERVER_PATH . '/admin/vc_extension/shortcodes/noo-testimonial.php'; 
        require_once PLUGIN_SERVER_PATH . '/admin/vc_extension/shortcodes/noo-services.php';
        require_once PLUGIN_SERVER_PATH . '/admin/vc_extension/shortcodes/noo-team.php';
        require_once PLUGIN_SERVER_PATH . '/admin/vc_extension/shortcodes/noo-form7.php';
        require_once PLUGIN_SERVER_PATH . '/admin/vc_extension/shortcodes/noo-opentable-reservation.php';
        require_once PLUGIN_SERVER_PATH . '/admin/vc_extension/shortcodes/noo-reservation.php';
        require_once PLUGIN_SERVER_PATH . '/admin/vc_extension/shortcodes/noo_menu_sections.php';
        require_once PLUGIN_SERVER_PATH . '/admin/vc_extension/shortcodes/noo_menu_sections_slider.php';
        require_once PLUGIN_SERVER_PATH . '/admin/vc_extension/shortcodes/noo-menu-grid.php';
        require_once PLUGIN_SERVER_PATH . '/admin/vc_extension/shortcodes/noo-gallery.php';
        require_once PLUGIN_SERVER_PATH . '/admin/vc_extension/shortcodes/noo-gallery-grid.php';
        require_once PLUGIN_SERVER_PATH . '/admin/vc_extension/shortcodes/noo-events.php';
    }
    add_action('init', 'nootheme_includevisual', 20);
endif;

