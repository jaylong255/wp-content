<?php
/**
 * NOO Visual Composer Add-ons
 *
 * Customize Visual Composer to suite NOO Framework
 *
 * @package    NOO Framework
 * @subpackage NOO Visual Composer Add-ons
 * @version    1.0.0
 * @author     Kan Nguyen <khanhnq@nootheme.com>
 * @copyright  Copyright (c) 2014, NooTheme
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       http://nootheme.com
 */
$name = esc_html__('Chilli', 'noo-chilli' );


// custom [row]
vc_add_param('vc_row', array(
        "type"        =>  "checkbox",
        "admin_label" =>  true,
        "heading"     =>  "Using Container",
        "param_name"  =>  "container_width",
        "description" =>  esc_html__('If checked container will be set to width 1170px for content.', 'noo-chilli' ),
        'weight'      => 1,
        'value'       => array( esc_html__( 'Yes', 'noo-chilli' ) => 'yes' )
    )
);
//[noo_slider]
add_filter( 'vc_autocomplete_noo_slider_include_callback',
    'vc_include_field_search', 10, 1 ); // Get suggestion(find). Must return an array
add_filter( 'vc_autocomplete_noo_slider_include_render',
    'vc_include_field_render', 10, 1 ); // Render exact product. Must return an array (label,value)

if ( is_plugin_active( 'contact-form-7/wp-contact-form-7.php' ) ) {
    $noo_cf7 = get_posts('post_type="wpcf7_contact_form"');

    $noo_contact_forms = array();
    if ($noo_cf7) {
        foreach ($noo_cf7 as $cform) {
            $noo_contact_forms[$cform->post_title] = $cform->ID;
        }
    } else {
        $noo_contact_forms[esc_html__('No contact forms found', 'js_composer')] = 0;
    }

    vc_map(array(
        'name' => esc_html__('Noo Custom Form 7', 'noo-chilli' ),
        'base' => 'noo_form',
        'icon' => 'noo_icon_form',
        'category' => $name,
        'params' => array(
            array(
                'param_name'  => 'style',
                'heading'     => esc_html__( 'Choose Form Style', 'noo-chilli' ),
                'description' => 'Title color, description color and background Input color will change depend on Form Style',
                'type'        => 'dropdown',
                'holder'      => 'div',
                'value'       =>  array(
                    esc_html__( 'Gray', 'noo-chilli' )   =>  2,
                    esc_html__( 'White', 'noo-chilli' ) =>  1
                )
            ),
            array(
                'param_name'    => 'title',
                'heading'       => esc_html__('Title', 'noo-chilli' ),
                'description'   => '',
                'type'          => 'textfield',
                'value'         => '',
                'horder'        => 'div',
                'admin_label'   => true
            ),
            array(
                'type'          => 'dropdown',
                'heading'       => esc_html__('Select contact form', 'js_composer'),
                'param_name'    => 'custom_form',
                'value'         => $noo_contact_forms,
                'description'   => esc_html__('Choose previously created contact form from the drop down list.', 'js_composer')
            ),
            array(
                'type'       => 'textarea',
                'holder'     => 'div',
                'heading'    => esc_html__( 'Open time', 'noo-chilli' ),
                'param_name' => 'description',
                'value'      => ''
            ),
        )
    ));
}

// noo OpenTable Reservations
vc_map(
    array(
        'name'     => esc_html__('Noo OpenTable Reservations', 'noo-chilli' ),
        'base'     => 'noo_opentable_reservation',
        'icon'     => 'noo_icon_form',
        'category' => $name,
        'params'   => array(
            array(
                'param_name'  => 'style',
                'heading'     => esc_html__( 'Choose Form Style', 'noo-chilli' ),
                'description' => 'Title color, description color and background Input color will change depend on Form Style',
                'type'        => 'dropdown',
                'holder'      => 'div',
                'value'       =>  array(
                    esc_html__( 'Gray', 'noo-chilli' )   =>  2,
                    esc_html__( 'White', 'noo-chilli' ) =>  1
                )
            ),
            array(
                'param_name'  => 'opentable_id',
                'heading'     => esc_html__('OpenTable Restaurant ID', 'noo-chilli' ),
                'description' => '',
                'type'        => 'textfield',
                'horder'      => 'div',
                'admin_label' => true,
            ),
            array(
                'param_name'  => 'title',
                'heading'     => esc_html__('Title', 'noo-chilli' ),
                'description' => '',
                'type'        => 'textfield',
                'value'       => '',
                'horder'      => 'div',
                'admin_label' => true
            ),
            array(
                'param_name'  => 'domain_ext',
                'heading'     => esc_html__( 'Country', 'noo-chilli' ),
                'description' => '',
                'type'        => 'dropdown',
                'holder'      => 'div',
                'value'       =>  array(
                    esc_html__( 'Global / U.S.', 'noo-chilli' ) =>  '',
                    esc_html__( 'Germany', 'noo-chilli' )        =>  'de',
                    esc_html__( 'United Kingdom', 'noo-chilli' ) =>  'co.uk',
                    esc_html__( 'Japan', 'noo-chilli' )          =>  'jp',
                    esc_html__( 'Mexico', 'noo-chilli' )         =>  'com.mx',
                )
            ),
        )
    )
);

// noo Images
vc_map(
    array(
        'name' => esc_html__('Noo Images', 'noo-chilli' ),
        'base' => 'noo_images',
        'icon' => 'noo_icon_slider',
        'category' => $name,
        'params' => array(
            array(
                'param_name' => 'logo',
                'heading' => esc_html__('Choose Logo', 'noo-chilli' ),
                'description' => 'Logo at center Images',
                'type' => 'attach_image',
                'horder' => 'div',
                'admin_label' => true,
            ),
            array(
                'param_name' => 'style',
                'heading' => esc_html__('Choose Style', 'noo-chilli' ),
                'description' => '',
                'type' => 'dropdown',
                'holder' => 'div',
                'value' => array(
                    'Simple Image'   => 1,
                    'Slider Images'   => 2
                )
            ),
            array(
                'param_name' => 'background_img',
                'heading' => esc_html__('Choose Image', 'noo-chilli' ),
                'description' => '',
                'type' => 'attach_image',
                'horder' => 'div',
                'admin_label' => true,
                'dependency' => array(
                    'element' => 'style',
                    'value' => array('1')
                ),
            ),
            array(
                'param_name'  => 'slide_img',
                'heading'     => esc_html__('Choose Images', 'noo-chilli' ),
                'description' => 'Maximum is 8 images for place',
                'type'        => 'attach_images',
                'horder'      => 'div',
                'admin_label' => true,
                'dependency'  => array(
                    'element' => 'style',
                    'value'   => array('2')
                ),
            )
        )
    )
);

// noo_title
vc_map(array(
    'name' => esc_html__('Noo Title', 'noo-chilli' ),
    'base' => 'noo_title',
    'icon' => 'noo_icon_title',
    'category' => $name,
    'params' => array(
        array(
            'param_name'    => 'title',
            'heading'       => esc_html__('Title', 'noo-chilli' ),
            'description'   => '',
            'type'          => 'textfield',
            'value'         => '',
            'horder'        => 'div',
            'admin_label'   => true
        ),
        array(
            'param_name'  => 'title_size',
            'heading'     => esc_html__( 'Title Size', 'noo-chilli' ),
            'description' => '',
            'type'        => 'dropdown',
            'holder'      => 'div',
            'value'       => array(
                esc_html__( 'Large', 'noo-chilli' ) => 'large',
                esc_html__( 'Medium', 'noo-chilli' ) => 'medium'
            )
        ),
        array(
            'param_name'    => 'sub_title',
            'heading'       => esc_html__('Sub Title', 'noo-chilli' ),
            'description'   => '',
            'type'          => 'textfield',
            'value'         => '',
            'horder'        => 'div',
            'admin_label'   => true
        ),
        array(
            'type' => 'textarea',
            'holder' => 'div',
            'heading' => esc_html__( 'Description', 'noo-chilli' ),
            'param_name' => 'description',
            'value' => ''
        ),
        array(
            'param_name'  => 'white_style',
            'heading'     => esc_html__( 'Use White Style', 'noo-chilli' ),
            'description' => 'Use white style font on darker background',
            'type'        => 'checkbox',
            'value'       => array( '' => 'true' )
        ),
    )
));

// noo_menu
vc_map(array(
    'name' => esc_html__('Noo Our Menu', 'noo-chilli' ),
    'base' => 'noo_menu',
    'icon' => 'noo_icon_menu',
    'category' => $name,
    'params' => array(
        array(
            'type' => 'textfield',
            'holder' => 'div',
            'heading' => esc_html__( 'Menu Title', 'noo-chilli' ),
            'param_name' => 'menu_title',
            'value' => ''
        ),
        array(
            'param_name'    => 'cat',
            'heading'       => esc_html__('Choose Menu Sections', 'noo-chilli' ),
            'description'   => '',
            'type'          => 'menu_sections',
            'value'         => '',
            'horder'        => 'div',
            'admin_label'   => false
        ),
        array(
            'param_name'  => 'column',
            'heading'     => esc_html__( 'Choose Menu Columns', 'noo-chilli' ),
            'description' => '',
            'type'        => 'dropdown',
            'holder'      => 'div',
            'value'       => array(
                esc_html__( 'Three', 'noo-chilli' ) => '3',
                esc_html__( 'Two', 'noo-chilli' )   => '2',
                esc_html__( 'One', 'noo-chilli' )   => '1'
            )
        ),
        array(
            'param_name'  => 'limit',
            'heading'     => esc_html__( 'Limit menu item', 'noo-chilli' ),
            'description' => esc_html__('This is option limit number menu item in one category', 'noo-chilli' ),
            'type'        => 'textfield',
            'holder'      => 'div',
            'value'       => ''
        ),
        array(
            'param_name' => 'button_name',
            'type'       => 'textfield',
            'holder'     => 'div',
            'heading'    => esc_html__( 'Button Name', 'noo-chilli' ),
            'group'      => esc_html__( 'Navigation & Design options', 'noo-chilli' ),
            'value'      => ''
        ),
        array(
            'param_name' => 'button_link',
            'type'       => 'textfield',
            'holder'     => 'div',
            'heading'    => esc_html__( 'Button Link', 'noo-chilli' ),
            'group'      => esc_html__( 'Navigation & Design options', 'noo-chilli' ),
            'value'      => ''
        ),
        array(
            'param_name'  => 'background_color',
            'heading'     => esc_html__( 'Background Color', 'noo-chilli' ),
            'group'       => esc_html__( 'Navigation & Design options', 'noo-chilli' ),
            'description' => '',
            'type'        => 'colorpicker',
            'holder'      => 'div',
            'value'       =>  ''
        ),
        array(
            'param_name'  => 'show_img_bottom',
            'heading'     => esc_html__( 'Show Images Bottom', 'noo-chilli' ),
            'group'       => esc_html__( 'Navigation & Design options', 'noo-chilli' ),
            'description' => '',
            'type'        => 'checkbox',
            'value'       => array( '' => 'true' )
        ),
        array(
            'param_name'  => 'img_bottom',
            'heading'     => esc_html__('Choose Image', 'noo-chilli' ),
            'group'       => esc_html__( 'Navigation & Design options', 'noo-chilli' ),
            'description' => '',
            'type'        => 'attach_image',
            'horder'      => 'div',
            'dependency'  => array( 'element' => 'show_img_bottom', 'value' => array( 'true' ) )
        ),
        array( 
            'param_name'  => 'img_bottom_size', 
            'heading'     => esc_html__( 'Size (px)', 'noo-chilli' ), 
            'description' => esc_html__( 'Enter in the size of your Image Bottom.', 'noo-chilli' ), 
            'group'       => esc_html__( 'Navigation & Design options', 'noo-chilli' ),
            'type'        => 'textfield',
            'holder'      => 'div', 
            'value'       => '260',
            'dependency'  => array( 'element' => 'show_img_bottom', 'value' => array( 'true' ) )
        ),
    )
));


//[noo_news]
add_filter( 'vc_autocomplete_noo_news_include_callback',
    'vc_include_field_search', 10, 1 ); // Get suggestion(find). Must return an array
add_filter( 'vc_autocomplete_noo_news_include_render',
    'vc_include_field_render', 10, 1 ); // Render exact product. Must return an array (label,value)
vc_map(array(
    'name'      =>  esc_html__('Noo News', 'noo-chilli' ),
    'base'      =>  'noo_news',
    'icon'      =>  'noo_icon_news',
    'category'  =>  $name,
    'params'    =>  array(
        array(
            'param_name'  => 'style',
            'heading'     => esc_html__( 'Choose Style', 'noo-chilli' ),
            'description' => '',
            'type'        => 'dropdown',
            'holder'      => 'div',
            'value'       =>  array(
                esc_html__( 'Masonry Style', 'noo-chilli' ) =>  'masonry',
                esc_html__( 'Blog Style', 'noo-chilli' )   =>  'blog'
            )
        ),
        array(
            'param_name'    =>  'type_query',
            'heading'       =>  esc_html__('Data Source', 'noo-chilli' ),
            'description'   =>  esc_html__('Select content type', 'noo-chilli' ),
            'type'          =>  'dropdown',
            'admin_label'   => true,
            'holder'        =>  'div',
            'value'         =>  array(
                'Category'      =>  'cate',
                'Tags'          =>  'tag',
                'Posts'         =>  'post_id'
            )
        ),
        array(
            'param_name'    => 'categories',
            'heading'       => esc_html__( 'Categories', 'noo-chilli' ),
            'description'   => esc_html__('Select categories.', 'noo-chilli' ),
            'type'          => 'post_categories',
            'admin_label'   => true,
            'holder'        => 'div',
            'dependency'    => array(
                'element'   => 'type_query',
                'value'     => array( 'cate' )
            ),
        ),
        array(
            'param_name'    => 'tags',
            'heading'       => esc_html__( 'Tags', 'noo-chilli' ),
            'description'   => esc_html__('Select Tags.', 'noo-chilli' ),
            'type'          => 'post_tags',
            'admin_label'   => true,
            'holder'        => 'div',
            'dependency'    => array(
                'element'   => 'type_query',
                'value'     => array( 'tag' )
            ),
        ),
        array(
            'type'        => 'autocomplete',
            'heading'     => esc_html__( 'Include only', 'noo-chilli' ),
            'param_name'  => 'include',
            'description' => esc_html__( 'Add posts, pages, etc. by title.', 'noo-chilli' ),
            'admin_label'   => true,
            'holder'        => 'div',
            'settings' => array(
                'multiple' => true,
                'sortable' => true,
                'groups'   => true,
            ),
            'dependency'    => array(
                'element'   => 'type_query',
                'value'     => array( 'post_id' )
            ),
        ),
        array(
            'param_name'  => 'orderby',
            'heading'     => esc_html__( 'Order By', 'noo-chilli' ),
            'description' => '',
            'type'        => 'dropdown',
            'holder'      => 'div',
            'value'       => array(
                esc_html__( 'Recent First', 'noo-chilli' )            => 'latest',
                esc_html__( 'Older First', 'noo-chilli' )             => 'oldest',
                esc_html__( 'Title Alphabet', 'noo-chilli' )          => 'alphabet',
                esc_html__( 'Title Reversed Alphabet', 'noo-chilli' ) => 'ralphabet'
            )
        ),
        array(
            'param_name'    => 'limit',
            'heading'       => esc_html__( 'Limited Item', 'noo-chilli' ),
            'description'   => esc_html__('The maximun number of post will be display.', 'noo-chilli' ),
            'type'          => 'textfield',
            'admin_label'   => true,
            'holder'        => 'div',
            'value'         =>  3
        ),
        array(
            'param_name'   => 'limit_excerpt',
            'heading'      => esc_html__( 'Excerpt Length', 'noo-chilli' ),
            'description'  => '',
            'type'         => 'textfield',
            'holder'       => 'div',
            'value'        =>  20
        ),
        array(
            'param_name' => 'button_name',
            'type'       => 'textfield',
            'holder'     => 'div',
            'heading'    => esc_html__( 'Button Name', 'noo-chilli' ),
            'group'      => esc_html__( 'Navigation options', 'noo-chilli' ),
            'value'      => ''
        ),
        array(
            'param_name' => 'button_link',
            'type'       => 'textfield',
            'holder'     => 'div',
            'heading'    => esc_html__( 'Button Link', 'noo-chilli' ),
            'group'      => esc_html__( 'Navigation options', 'noo-chilli' ),
            'value'      => ''
        ),
    )
));

//[noo_testimonial]
$cat = array();
$cat['All'] = 'all';
if(class_exists('Noo_Chilli_Library')):
    $categories = get_categories( array( 'taxonomy'  => 'testimonial_category' ) );
    if ( isset( $categories ) && !empty( $categories ) ):
        foreach( $categories as $cate ):
            $cat[ $cate->name ] = $cate -> term_id;
        endforeach;
    endif;
endif;
vc_map(array(
    'name'      =>  esc_html__('Noo Testimonial', 'noo-chilli' ),
    'base'      =>  'noo_testimonial',
    'icon'      =>  'noo_icon_testimonial',
    'category'  =>   $name,
    'params'    =>  array(
        array(
            'param_name'  => 'style',
            'heading'     => esc_html__( 'Choose Style', 'noo-chilli' ),
            'description' => '',
            'type'        => 'dropdown',
            'holder'      => 'div',
            'value'       =>  array(
                esc_html__( 'Square Images', 'noo-chilli' ) =>  1,
                esc_html__( 'Circle Images', 'noo-chilli' )   =>  2
            )
        ),
        array(
            'param_name'  => 'border_color',
            'heading'     => esc_html__( 'Border Color Images', 'noo-chilli' ),
            'description' => '',
            'type'        => 'colorpicker',
            'holder'      => 'div',
            'value'       =>  ''
        ),
        array(
            'param_name'  => 'categories',
            'heading'     => esc_html__( 'Categories', 'noo-chilli' ),
            'description' => '',
            'type'        => 'dropdown',
            'holder'      => 'div',
            'value'       =>  $cat
        ),

        array(
            'param_name'  => 'orderby',
            'heading'     => esc_html__( 'Order By', 'noo-chilli' ),
            'description' => '',
            'type'        => 'dropdown',
            'holder'      => 'div',
            'value'       => array(
                esc_html__( 'Recent First', 'noo-chilli' ) => 'latest',
                esc_html__( 'Older First', 'noo-chilli' ) => 'oldest',
                esc_html__( 'Title Alphabet', 'noo-chilli' ) => 'alphabet',
                esc_html__( 'Title Reversed Alphabet', 'noo-chilli' ) => 'ralphabet' )
        ),
        array(
            'param_name'  => 'autoplay',
            'heading'     => esc_html__( 'Auto Play', 'noo-chilli' ),
            'description' => '',
            'type'        => 'dropdown',
            'holder'      => 'div',
            'value'       =>  array(
                'Yes'    =>  'true',
                'No'   =>  'false'
            )
        ),
        array(
            'param_name'  => 'posts_per_page',
            'heading'     => esc_html__( 'Limited Testimonial', 'noo-chilli' ),
            'description' => '',
            'type'        => 'textfield',
            'holder'      => 'div',
            'value'       => 10
        ),
    )
));

//noo_services
vc_map(array(
    'name'      =>  esc_html__('Noo Services', 'noo-chilli' ),
    'base'      =>  'noo_services',
    'icon'      =>  'noo_icon_services',
    'category'  =>   $name,
    'params'    =>  array(
        array(
            'param_name'  => 'icon_style',
            'heading'     => esc_html__( 'Choose Icon Style', 'noo-chilli' ),
            'description' => '',
            'type'        => 'dropdown',
            'holder'      => 'div',
            'value'       =>  array(
                esc_html__( 'Select Image', 'noo-chilli' ) =>  1,
                esc_html__( 'FontAwesome', 'noo-chilli' )   =>  2
            )
        ),
        array(
            'param_name'  => 'background_img',
            'heading'     => esc_html__('Choose Image', 'noo-chilli' ),
            'description' => '',
            'type'        => 'attach_image',
            'horder'      => 'div',
            'dependency'  => array(
                'element' => 'icon_style',
                'value'   => array('1')
            ),
        ),
        array(
            'param_name'    =>  'icon',
            'heading'       =>  esc_html__('Choose icon', 'noo-chilli' ),
            'description'   =>  '',
            'type'          =>  'iconpicker',
            'holder'        =>  'div',
            'dependency'  => array(
                'element' => 'icon_style',
                'value'   => array('2')
            ),
        ),
        array(
            'param_name'  => 'title',
            'heading'     => esc_html__( 'Title', 'noo-chilli' ),
            'description' => '',
            'type'        => 'textfield',
            'holder'      => 'div',
            'value'       => ''
        ),
        array(
            'param_name'  => 'bg_color',
            'heading'     => esc_html__( 'Background Color', 'noo-chilli' ),
            'description' => '',
            'type'        => 'colorpicker',
            'holder'      => 'div',
            'value'       =>  ''
        )
    )
));

//noo_team
$cat = array();
$cat['All'] = 'all';
if( taxonomy_exists('team_category') ):
    $categories = get_categories( array( 'taxonomy'  => 'team_category' ) );
    if ( isset( $categories ) && !empty( $categories ) ):
        foreach( $categories as $cate ):
            $cat[ $cate->name ] = $cate -> term_id;
        endforeach;
    endif;
endif;
vc_map(array(
    'name'      =>  esc_html__('Noo Team', 'noo-chilli' ),
    'base'      =>  'noo_team',
    'icon'      =>  'noo_icon_team',
    'category'  =>   $name,
    'params'    =>  array(
        array(
            'param_name'  => 'categories',
            'heading'     => esc_html__( 'Categories', 'noo-chilli' ),
            'description' => '',
            'type'        => 'dropdown',
            'holder'      => 'div',
            'value'       =>  $cat
        ),

        array(
            'param_name'  => 'orderby',
            'heading'     => esc_html__( 'Order By', 'noo-chilli' ),
            'description' => '',
            'type'        => 'dropdown',
            'holder'      => 'div',
            'value'       => array(
                esc_html__( 'Recent First', 'noo-chilli' ) => 'latest',
                esc_html__( 'Older First', 'noo-chilli' ) => 'oldest',
                esc_html__( 'Title Alphabet', 'noo-chilli' ) => 'alphabet',
                esc_html__( 'Title Reversed Alphabet', 'noo-chilli' ) => 'ralphabet' )
        ),
        array(
            'param_name'  => 'limit',
            'heading'     => esc_html__( 'Limited Team Member', 'noo-chilli' ),
            'description' => '',
            'type'        => 'textfield',
            'holder'      => 'div',
            'value'       => 4
        ),
        array(
            'param_name'  => 'columns',
            'heading'     => esc_html__( 'Columns', 'noo-chilli' ),
            'description' => '',
            'type'        => 'dropdown',
            'holder'      => 'div',
            'value'       => array(
                esc_html__( '1', 'noo-chilli' ) => '1',
                esc_html__( '2', 'noo-chilli' ) => '2',
                esc_html__( '3', 'noo-chilli' ) => '3',
                esc_html__( '4', 'noo-chilli' ) => '4'
            ),
        ),
        array( 
            'param_name' => 'layout_style', 
            'heading'    => esc_html__( 'Layout Style', 'noo-chilli' ), 
            'type'       => 'dropdown', 
            'holder'     => 'div',
            'value'      => array( 
                esc_html__( 'Default', 'noo-chilli' )      => 'default', 
                esc_html__( 'Info Overlay', 'noo-chilli' ) => 'info_overlay'
            ),
        ),
    )
));

// noo_reservation
vc_map(array(
    'name'      =>  esc_html__('Noo Reservation', 'noo-chilli' ),
    'base'      =>  'noo_reservation',
    'icon'      =>  'noo_icon_reservation',
    'category'  =>   $name,
    'params'    =>  array(
        array(
            'param_name'  => 'title',
            'heading'     => esc_html__( 'Title', 'noo-chilli' ),
            'description' => '',
            'type'        => 'textfield',
            'holder'      => 'div',
            'value'       => ''
        ),
        array(
            'type' => 'textarea',
            'holder' => 'div',
            'heading' => esc_html__( 'Description', 'noo-chilli' ),
            'param_name' => 'description',
            'value' => ''
        ),
        array(
            'param_name'  => 'info',
            'heading'     => esc_html__( 'Info', 'noo-chilli' ),
            'description' => '',
            'type'        => 'textfield',
            'holder'      => 'div',
            'value'       => ''
        )
    )
));

// noo menu Sections
vc_map(array(
    'name' => esc_html__('Noo Menu Sections', 'noo-chilli' ),
    'base' => 'noo_menu_sections',
    'icon' => 'noo_icon_menus',
    'category' => $name,
    'params' => array(
        array(
            'param_name'    => 'cat',
            'heading'       => esc_html__('Choose Menu Sections', 'noo-chilli' ),
            'description'   => '',
            'type'          => 'menu_sections',
            'value'         => '',
            'horder'        => 'div',
            'admin_label'   => false
        ),
        array(
            'param_name'  => 'limit',
            'heading'     => esc_html__( 'Limit menu item', 'noo-chilli' ),
            'description' => esc_html__('This is option limit number menu item in one category. Leave blank so no limit.', 'noo-chilli' ),
            'type'        => 'textfield',
            'holder'      => 'div',
            'value'       => ''
        ),
        array(
            'param_name'  => 'background_color',
            'heading'     => esc_html__( 'Background Color', 'noo-chilli' ),
            'description' => '',
            'type'        => 'colorpicker',
            'holder'      => 'div',
            'value'       =>  ''
        ),
    )
));

// noo menu Sections
vc_map(array(
    'name' => esc_html__('Noo Menu Sections with Slider', 'noo-chilli' ),
    'base' => 'noo_menu_sections_slider',
    'icon' => 'noo_icon_menus',
    'category' => $name,
    'params' => array(
        array(
            'param_name'    => 'cat',
            'heading'       => esc_html__('Choose Menu Sections', 'noo-chilli' ),
            'description'   => '',
            'type'          => 'menu_sections',
            'value'         => '',
            'horder'        => 'div',
            'admin_label'   => false
        ),
        array(
            'param_name'  => 'limit',
            'heading'     => esc_html__( 'Limit menu item', 'noo-chilli' ),
            'description' => esc_html__('This is option limit number menu item in one category', 'noo-chilli' ),
            'type'        => 'textfield',
            'holder'      => 'div',
            'value'       => ''
        ),
        array(
            'param_name'  => 'slider',
            'heading'     => esc_html__( 'Show Slider Menu', 'noo-chilli' ),
            'description' => '',
            'type'        => 'checkbox',
            'value'       => array( '' => 'true' ),
            'admin_label' => true
        ),
        array(
            'param_name'  => 'limit_slider',
            'heading'     => esc_html__( 'Limit Thumbnail Slider', 'noo-chilli' ),
            'description' => esc_html__('This is option limit number thumbnail item in one category with slider', 'noo-chilli' ),
            'type'        => 'textfield',
            'holder'      => 'div',
            'value'       => '',
            'dependency'    => array( 'element' => 'slider', 'value' => array( 'true' ) ),
        ),
        array(
            'param_name'  => 'autoplay',
            'heading'     => esc_html__( 'Auto Play', 'noo-chilli' ),
            'group'       => esc_html__( 'Navigation & Design options', 'noo-chilli' ),
            'description' => '',
            'type'        => 'dropdown',
            'holder'      => 'div',
            'value'       =>  array(
                'Yes' =>  'true',
                'No'  =>  'false'
            ),
            'dependency'    => array( 'element' => 'slider', 'value' => array( 'true' ) ),
        ),
        array(
            'param_name'  => 'background_color',
            'heading'     => esc_html__( 'Background Color', 'noo-chilli' ),
            'group'       => esc_html__( 'Navigation & Design options', 'noo-chilli' ),
            'description' => '',
            'type'        => 'colorpicker',
            'holder'      => 'div',
            'value'       =>  ''
        ),
    )
));

// menu grid
vc_map(array(
    'name' => esc_html__('Noo Menu Grid', 'noo-chilli' ),
    'base' => 'noo_menu_grid',
    'icon' => 'noo_icon_menus',
    'category' => $name,
    'params' => array(
        array(
            'param_name'    => 'cat',
            'heading'       => esc_html__('Choose Menu Sections', 'noo-chilli' ),
            'description'   => '',
            'type'          => 'menu_sections',
            'value'         => '',
            'horder'        => 'div',
            'admin_label'   => false
        ),
        array(
            'param_name'  => 'limit',
            'heading'     => esc_html__( 'Limit menu item', 'noo-chilli' ),
            'description' => esc_html__('This is option limit number menu item in one category', 'noo-chilli' ),
            'type'        => 'textfield',
            'holder'      => 'div',
            'value'       => ''
        ),
        array(
            'param_name'  => 'orderby',
            'heading'     => esc_html__( 'Order By', 'noo-chilli' ),
            'description' => '',
            'type'        => 'dropdown',
            'holder'      => 'div',
            'value'       => array(
                esc_html__( 'Recent First', 'noo-chilli' ) => 'latest',
                esc_html__( 'Older First', 'noo-chilli' ) => 'oldest',
                esc_html__( 'Title Alphabet', 'noo-chilli' ) => 'alphabet',
                esc_html__( 'Title Reversed Alphabet', 'noo-chilli' ) => 'ralphabet' )
        ),
        array(
            'param_name'  => 'hide_filter',
            'heading'     => esc_html__( 'Hide Filter', 'noo-chilli' ),
            'description' => '',
            'type'        => 'checkbox',
            'value'       => array( '' => 'true' )
        ),
        array(
            'param_name'  => 'style',
            'heading'     => esc_html__( 'Choose Item Style', 'noo-chilli' ),
            'description' => '',
            'type'        => 'dropdown',
            'holder'      => 'div',
            'value'       =>  array(
                esc_html__( 'Medium Item', 'noo-chilli' ) =>  'medium',
                esc_html__( 'Small Item', 'noo-chilli' )  =>  'small'
            )
        ),
        array(
            'param_name'  => 'show_icon',
            'heading'     => esc_html__( 'Show Icon', 'noo-chilli' ),
            'group'       => esc_html__( 'Icon options', 'noo-chilli' ),
            'description' => '',
            'type'        => 'checkbox',
            'value'       => array( '' => 'true' )
        ),
        array(
            'param_name'  => 'icon',
            'heading'     => esc_html__('Choose Image', 'noo-chilli' ),
            'group'       => esc_html__( 'Icon options', 'noo-chilli' ),
            'description' => '',
            'type'        => 'attach_image',
            'horder'      => 'div',
            'admin_label' => true,
            'dependency'  => array(
                'element' => 'show_icon',
                'value'   => array('true')
            ),
        ),
    )
));

// gallery
vc_map(array(
    'name' => esc_html__('Noo Gallery', 'noo-chilli' ),
    'base' => 'noo_gallery',
    'icon' => 'noo_icon_slider',
    'category' => $name,
    'params' => array(
        array(
            'param_name' => 'title',
            'type' => 'textfield',
            'holder' => 'div',
            'heading' => esc_html__( 'Title', 'noo-chilli' ),
            'value' => ''
        ),
        array(
            'param_name'    => 'cat',
            'heading'       => esc_html__('Choose Gallery Categories', 'noo-chilli' ),
            'description'   => '',
            'type'          => 'gallery_categories',
            'value'         => '',
            'horder'        => 'div',
            'admin_label'   => false
        ),
        array(
            'param_name'  => 'orderby',
            'heading'     => esc_html__( 'Order By', 'noo-chilli' ),
            'description' => '',
            'type'        => 'dropdown',
            'holder'      => 'div',
            'value'       => array(
                esc_html__( 'Recent First', 'noo-chilli' ) => 'latest',
                esc_html__( 'Older First', 'noo-chilli' ) => 'oldest',
                esc_html__( 'Title Alphabet', 'noo-chilli' ) => 'alphabet',
                esc_html__( 'Title Reversed Alphabet', 'noo-chilli' ) => 'ralphabet' )
        ),
        array(
            'param_name'  => 'limit',
            'heading'     => esc_html__( 'Limit gallery item', 'noo-chilli' ),
            'description' => esc_html__('This is option limit number gallery item in one category', 'noo-chilli' ),
            'type'        => 'textfield',
            'holder'      => 'div',
            'value'       => ''
        ),
        array(
            'param_name'  => 'style',
            'heading'     => esc_html__( 'Choose Hover Style', 'noo-chilli' ),
            'description' => '',
            'type'        => 'dropdown',
            'holder'      => 'div',
            'value'       =>  array(
                esc_html__( 'Show Full Information', 'noo-chilli' )    =>  'full',
                esc_html__( 'Show Only Title & Description', 'noo-chilli' ) =>  'text_only',
                esc_html__( 'Show Only Icon', 'noo-chilli' )            =>  'icon_only'
            )
        ),
        array(
            'param_name'   => 'excerpt_length',
            'heading'      => esc_html__( 'Excerpt Length', 'noo-chilli' ),
            'description'  => '',
            'type'         => 'textfield',
            'holder'       => 'div',
            'value'        =>  20
        ),
        array(
            'param_name'  => 'white_style',
            'heading'     => esc_html__( 'Use White Style', 'noo-chilli' ),
            'group'       => esc_html__( 'Navigation & Design options', 'noo-chilli' ),
            'description' => 'Use white style font on darker background',
            'type'        => 'checkbox',
            'value'       => array( '' => 'true' )
        ),
        array(
            'param_name'  => 'autoplay',
            'heading'     => esc_html__( 'Auto Play', 'noo-chilli' ),
            'group'       => esc_html__( 'Navigation & Design options', 'noo-chilli' ),
            'description' => '',
            'type'        => 'dropdown',
            'holder'      => 'div',
            'value'       =>  array(
                'Yes' =>  'true',
                'No'  =>  'false'
            )
        ),
        array(
            'param_name' => 'button_name',
            'type'       => 'textfield',
            'holder'     => 'div',
            'heading'    => esc_html__( 'Button Name', 'noo-chilli' ),
            'group'      => esc_html__( 'Navigation & Design options', 'noo-chilli' ),
            'value'      => ''
        ),
        array(
            'param_name' => 'button_link',
            'type'       => 'textfield',
            'holder'     => 'div',
            'heading'    => esc_html__( 'Button Link', 'noo-chilli' ),
            'group'      => esc_html__( 'Navigation & Design options', 'noo-chilli' ),
            'value'      => ''
        ),

    )
));

// gallery grid
vc_map(array(
    'name' => esc_html__('Noo Gallery Grid', 'noo-chilli' ),
    'base' => 'noo_gallery_grid',
    'icon' => 'noo_icon_slider',
    'category' => $name,
    'params' => array(
        array(
            'param_name'    => 'cat',
            'heading'       => esc_html__('Choose Gallery Categories', 'noo-chilli' ),
            'description'   => '',
            'type'          => 'gallery_categories',
            'value'         => '',
            'horder'        => 'div',
            'admin_label'   => false
        ),
        array(
            'param_name'  => 'orderby',
            'heading'     => esc_html__( 'Order By', 'noo-chilli' ),
            'description' => '',
            'type'        => 'dropdown',
            'holder'      => 'div',
            'value'       => array(
                esc_html__( 'Recent First', 'noo-chilli' ) => 'latest',
                esc_html__( 'Older First', 'noo-chilli' ) => 'oldest',
                esc_html__( 'Title Alphabet', 'noo-chilli' ) => 'alphabet',
                esc_html__( 'Title Reversed Alphabet', 'noo-chilli' ) => 'ralphabet' )
        ),
        array(
            'param_name'  => 'limit',
            'heading'     => esc_html__( 'Limit gallery item', 'noo-chilli' ),
            'description' => esc_html__('This is option limit number gallery item in one category', 'noo-chilli' ),
            'type'        => 'textfield',
            'holder'      => 'div',
            'value'       => ''
        ),
        array(
            'param_name'  => 'hide_filter',
            'heading'     => esc_html__( 'Hide Filter', 'noo-chilli' ),
            'description' => '',
            'type'        => 'checkbox',
            'value'       => array( '' => 'true' )
        ),
        array(
            'param_name'  => 'style',
            'heading'     => esc_html__( 'Choose Hover Style', 'noo-chilli' ),
            'description' => '',
            'type'        => 'dropdown',
            'holder'      => 'div',
            'value'       =>  array(
                esc_html__( 'Show Full Information', 'noo-chilli' )    =>  'full',
                esc_html__( 'Show Only Title & Description', 'noo-chilli' ) =>  'text_only',
                esc_html__( 'Show Only Icon', 'noo-chilli' )            =>  'icon_only'
            )
        ),
        array(
            'param_name'   => 'excerpt_length',
            'heading'      => esc_html__( 'Excerpt Length', 'noo-chilli' ),
            'description'  => '',
            'type'         => 'textfield',
            'holder'       => 'div',
            'value'        =>  20
        )
    )
));

// Event

vc_map(array(
    'base'        => 'noo_events',
    'name'        => esc_html__( 'Noo Events', 'noo-chilli' ),
    'class'       => 'noo-icon-events',
    'icon'        => 'noo_icon_news',
    'category'    => $name,
    'description' => '',
    'params'      => array(
        array(
            'param_name'    => 'cat',
            'heading'       => esc_html__('Categories Events', 'noo-chilli'),
            'description'   => '',
            'type'          => 'categories_events',
            'value'         => '',
            'horder'        => 'div',
            'admin_label'   => true
        ),
        array(
            'param_name'  => 'columns',
            'heading'     => esc_html__( 'Columns', 'noo-chilli' ),
            'description' => '',
            'type'        => 'dropdown',
            'holder'      => 'div',
            'value'       => array(
                esc_html__( '1', 'noo-chilli' ) => '1',
                esc_html__( '2', 'noo-chilli' ) => '2',
                esc_html__( '3', 'noo-chilli' ) => '3',
                esc_html__( '4', 'noo-chilli' ) => '4' )
        ),
        array(
            'param_name'  => 'orderby',
            'heading'     => esc_html__( 'Order By', 'noo-chilli' ),
            'description' => '',
            'type'        => 'dropdown',
            'holder'      => 'div',
            'value'       => array(
                esc_html__( 'Recent First', 'noo-chilli' ) => 'latest',
                esc_html__( 'Older First', 'noo-chilli' ) => 'oldest',
                esc_html__( 'Title Alphabet', 'noo-chilli' ) => 'alphabet',
                esc_html__( 'Title Reversed Alphabet', 'noo-chilli' ) => 'ralphabet' )
        ),
        array(
            'param_name'  => 'show_date',
            'heading'     => esc_html__( 'Show Date', 'noo-chilli' ),
            'description' => '',
            'type'        => 'dropdown',
            'holder'      => 'div',
            'value'       => array(
                esc_html__( 'Yes', 'noo-chilli' ) => 'yes',
                esc_html__( 'No', 'noo-chilli' ) => 'no')
        ),
        array(
            'param_name'  => 'show_excerpt',
            'heading'     => esc_html__( 'Show Excerpt', 'noo-chilli' ),
            'description' => '',
            'type'        => 'dropdown',
            'holder'      => 'div',
            'value'       => array(
                esc_html__( 'Yes', 'noo-chilli' ) => 'yes',
                esc_html__( 'No', 'noo-chilli' ) => 'no'),
        ),
        array(
            'param_name'   => 'limit_excerpt',
            'heading'      => esc_html__( 'Excerpt Length', 'noo-chilli' ),
            'description'  => '',
            'type'         => 'textfield',
            'holder'       => 'div',
            'value'        =>  20,
            'dependency' => array( 'element' => 'show_excerpt', 'value' => array( 'yes' ) )
        ),
        array(
            'param_name'    => 'limit',
            'heading'       => esc_html__( 'Limited', 'noo-chilli' ),
            'description'   => esc_html__('The maximum number of events.', 'noo-chilli' ),
            'type'          => 'textfield',
            'admin_label'   => true,
            'holder'        => 'div',
            'value'         =>  10
        )
    )
));