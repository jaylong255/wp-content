<?php
/**
 * Register NOO Gallery.
 * This file register Item and Category for NOO gallery.
 *
 * @package    NOO Framework
 * @subpackage NOO gallery
 * @version    1.0.0
 * @author     Kan Nguyen <khanhnq@nootheme.com>
 * @copyright  Copyright (c) 2014, NooTheme
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       http://nootheme.com
 */

if ( ! function_exists('noo_init_gallery')) :
    function noo_init_gallery() {

        // Text for NOO gallery.
        $gallery_labels = array(
            'name' => esc_html__('Gallery', 'noo-chilli') ,
            'singular_name' => esc_html__('Gallery', 'noo-chilli') ,
            'menu_name' => esc_html__('Gallery', 'noo-chilli') ,
            'add_new' => esc_html__('Add New', 'noo-chilli') ,
            'add_new_item' => esc_html__('Add New gallery Item', 'noo-chilli') ,
            'edit_item' => esc_html__('Edit gallery Item', 'noo-chilli') ,
            'new_item' => esc_html__('Add New gallery Item', 'noo-chilli') ,
            'view_item' => esc_html__('View gallery', 'noo-chilli') ,
            'search_items' => esc_html__('Search gallery', 'noo-chilli') ,
            'not_found' => esc_html__('No gallery items found', 'noo-chilli') ,
            'not_found_in_trash' => esc_html__('No gallery items found in trash', 'noo-chilli') ,
            'parent_item_colon' => ''
        );

        $admin_icon = PLUGIN_PATH . '/assets/images/noo20x20.png';
        if ( floatval( get_bloginfo( 'version' ) ) >= 3.8 ) {
            $admin_icon = 'dashicons-images-alt2';
        }

        $gallery_slug = 'noo-gallery';

        // Options
        $gallery_args = array(
            'labels' => $gallery_labels,
            'public' => false,
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'menu_position' => 5,
            'menu_icon' => $admin_icon,
            'capability_type' => 'post',
            'hierarchical' => false,
            'supports' => array(
                'title',
                'editor',
//                'excerpt',
                'thumbnail',
                // 'comments',
                //'custom-fields',
                'revisions'
            ) ,
            'has_archive' => true,
            'rewrite' => array(
                'slug' => $gallery_slug,
                'with_front' => false
            )
        );

        register_post_type('gallery', $gallery_args);

        // Register a taxonomy for Project Categories.
        $category_labels = array(
            'name' => esc_html__('Gallery Categories', 'noo-chilli') ,
            'singular_name' => esc_html__('Gallery Category', 'noo-chilli') ,
            'menu_name' => esc_html__('Gallery Categories', 'noo-chilli') ,
            'all_items' => esc_html__('All Gallery Categories', 'noo-chilli') ,
            'edit_item' => esc_html__('Edit Gallery Category', 'noo-chilli') ,
            'view_item' => esc_html__('View Gallery Category', 'noo-chilli') ,
            'update_item' => esc_html__('Update Gallery Category', 'noo-chilli') ,
            'add_new_item' => esc_html__('Add New Gallery Category', 'noo-chilli') ,
            'new_item_name' => esc_html__('New Gallery Category Name', 'noo-chilli') ,
            'parent_item' => esc_html__('Parent Gallery Category', 'noo-chilli') ,
            'parent_item_colon' => esc_html__('Parent Gallery Category:', 'noo-chilli') ,
            'search_items' => esc_html__('Search Gallery Categories', 'noo-chilli') ,
            'popular_items' => esc_html__('Popular Gallery Categories', 'noo-chilli') ,
            'separate_items_with_commas' => esc_html__('Separate Gallery Categories with commas', 'noo-chilli') ,
            'add_or_remove_items' => esc_html__('Add or remove Gallery Categories', 'noo-chilli') ,
            'choose_from_most_used' => esc_html__('Choose from the most used Gallery Categories', 'noo-chilli') ,
            'not_found' => esc_html__('No Gallery Categories found', 'noo-chilli') ,
        );

        $category_args = array(
            'labels' => $category_labels,
            'public' => false,
            'show_ui' => true,
            'show_in_nav_menus' => false,
            'show_tagcloud' => false,
            'show_admin_column' => true,
            'hierarchical' => true,
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'gallery_categories',
                'with_front' => false
            ) ,
        );

        register_taxonomy('gallery_categories', array(
            'gallery'
        ) , $category_args);

    }
endif;

add_action('init', 'noo_init_gallery');




