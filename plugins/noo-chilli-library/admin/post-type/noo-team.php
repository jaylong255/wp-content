<?php
/**
 * Register NOO Portfolio.
 * This file register Item and Category for NOO Portfolio.
 *
 * @package    NOO Framework
 * @subpackage NOO Portfolio
 * @version    1.0.0
 * @author     Kan Nguyen <khanhnq@nootheme.com>
 * @copyright  Copyright (c) 2014, NooTheme
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       http://nootheme.com
 */

if ( ! function_exists('noo_init_team')) :
	function noo_init_team() {

		// Text for NOO team.
		$team_labels = array(
			'name' => esc_html__('Team Member', 'noo-chilli') ,
			'singular_name' => esc_html__('Team Member', 'noo-chilli') ,
			'menu_name' => esc_html__('Team Member', 'noo-chilli') ,
			'add_new' => esc_html__('Add New', 'noo-chilli') ,
			'add_new_item' => esc_html__('Add New Team Member Item', 'noo-chilli') ,
			'edit_item' => esc_html__('Edit Team Member Item', 'noo-chilli') ,
			'new_item' => esc_html__('Add New Team Member Item', 'noo-chilli') ,
			'view_item' => esc_html__('View Team Member', 'noo-chilli') ,
			'search_items' => esc_html__('Search Team Member', 'noo-chilli') ,
			'not_found' => esc_html__('No Team Member items found', 'noo-chilli') ,
			'not_found_in_trash' => esc_html__('No Team Member items found in trash', 'noo-chilli') ,
			'parent_item_colon' => ''
		);

		$admin_icon = PLUGIN_PATH . '/assets/images/noo20x20.png';
        if ( floatval( get_bloginfo( 'version' ) ) >= 3.8 ) {
            $admin_icon = 'dashicons-groups';
        }

		$team_slug = 'noo-team';

		// Options
		$team_args = array(
			'menu_icon' => $admin_icon,
			'labels' => $team_labels,
			'public' => false,
			'publicly_queryable' => true,
            'show_in_nav_menus' => false,
			'show_ui' => true,
			'show_in_menu' => true,
			 'menu_position' => 5,
			'capability_type' => 'post',
			'hierarchical' => false,
			'supports' => array(
				'title',
				'revisions'
			) ,
			'has_archive' => true,
			'rewrite' => array(
				'slug' => $team_slug,
				'with_front' => false
			)
		);
		
		register_post_type('team_member', $team_args);

		// Register a taxonomy for Project Categories.
		$category_labels = array(
			'name' => esc_html__('Team Categories', 'noo-chilli') ,
			'singular_name' => esc_html__('Team Category', 'noo-chilli') ,
			'menu_name' => esc_html__('Team Categories', 'noo-chilli') ,
			'all_items' => esc_html__('All Team Categories', 'noo-chilli') ,
			'edit_item' => esc_html__('Edit Team Category', 'noo-chilli') ,
			'view_item' => esc_html__('View Team Category', 'noo-chilli') ,
			'update_item' => esc_html__('Update Team Category', 'noo-chilli') ,
			'add_new_item' => esc_html__('Add New Team Category', 'noo-chilli') ,
			'new_item_name' => esc_html__('New Team Category Name', 'noo-chilli') ,
			'parent_item' => esc_html__('Parent Team Category', 'noo-chilli') ,
			'parent_item_colon' => esc_html__('Parent Team Category:', 'noo-chilli') ,
			'search_items' => esc_html__('Search Team Categories', 'noo-chilli') ,
			'popular_items' => esc_html__('Popular Team Categories', 'noo-chilli') ,
			'separate_items_with_commas' => esc_html__('Separate Team Categories with commas', 'noo-chilli') ,
			'add_or_remove_items' => esc_html__('Add or remove Team Categories', 'noo-chilli') ,
			'choose_from_most_used' => esc_html__('Choose from the most used Team Categories', 'noo-chilli') ,
			'not_found' => esc_html__('No Team Categories found', 'noo-chilli') ,
		);
		
		$category_args = array(
			'labels' => $category_labels,
			'public' => false,
			'show_ui' => true,
			'show_in_nav_menus' => false,
			'show_tagcloud' => false,
			'show_admin_column' => false,
			'hierarchical' => true,
			'query_var' => true,
			'rewrite' => array(
				'slug' => 'team_category',
				'with_front' => false
			) ,
		);
		
		register_taxonomy('team_category', array(
			'team_member'
		) , $category_args);


	}
endif;

add_action('init', 'noo_init_team');




