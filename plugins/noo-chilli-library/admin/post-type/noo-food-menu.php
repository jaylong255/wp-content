<?php
/**
 * Register NOO Portfolio.
 * This file register Item and Category for NOO Portfolio.
 *
 * @package    NOO Framework
 * @subpackage NOO Portfolio
 * @version    1.0.0
 * @author     Kan Nguyen <khanhnq@nootheme.com>
 * @copyright  Copyright (c) 2014, NooTheme
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       http://nootheme.com
 */

if ( ! function_exists('noo_init_food_menu')) :
    function noo_init_food_menu() {

        // Text for NOO team.
        $team_labels = array(
            'name' => esc_html__('Menu items', 'noo-chilli') ,
            'singular_name' => esc_html__('Menu item', 'noo-chilli') ,
            'menu_name' => esc_html__('Food Menus', 'noo-chilli') ,
            'all_items'     => esc_html__( 'Menu Items', 'noo-chilli' ),
            'add_new' => esc_html__('Add One Item', 'noo-chilli') ,
            'add_new_item' => esc_html__('Add New Menu Item', 'noo-chilli') ,
            'edit_item' => esc_html__('Edit Menu Item', 'noo-chilli') ,
            'new_item' => esc_html__('Add New Menu Item', 'noo-chilli') ,
            'view_item' => esc_html__('View Menu item', 'noo-chilli') ,
            'search_items' => esc_html__('Search Menu item', 'noo-chilli') ,
            'not_found' => esc_html__('No Menu item items found', 'noo-chilli') ,
            'not_found_in_trash' => esc_html__('No Menu items found in trash', 'noo-chilli') ,
            'parent_item_colon' => ''
        );

        $admin_icon = PLUGIN_PATH . '/assets/images/icon.png';
        $team_slug = 'noo-food-menu';

        // Options
        $team_args = array(
            'labels' => $team_labels,
            'public' => false,
            'publicly_queryable' => true,
            'show_in_nav_menus' => false,
            'show_ui' => true,
            'show_in_menu' => true,
            'menu_icon' => $admin_icon,
            'menu_position' => 5,
            'capability_type' => 'post',
            'hierarchical' => false,
            'supports' => array(
                'title',
                 'editor',
                // 'excerpt',
                'thumbnail',
                // 'comments',
                // 'custom-fields',
                'revisions'
            ) ,
            'has_archive' => true,
            'rewrite' => array(
                'slug' => $team_slug,
                'with_front' => false
            )
        );

        register_post_type('food_menu', $team_args);

        // Register a taxonomy for Menu Sections
        $category_labels = array(
            'name' => esc_html__('Menu Sections', 'noo-chilli') ,
            'singular_name' => esc_html__('Menu Section', 'noo-chilli') ,
            'menu_name' => esc_html__('Menu Sections', 'noo-chilli') ,
            'all_items' => esc_html__('All Menu Sections', 'noo-chilli') ,
            'edit_item' => esc_html__('Edit Menu Sections', 'noo-chilli') ,
            'view_item' => esc_html__('View Menu Sections', 'noo-chilli') ,
            'update_item' => esc_html__('Update Menu Sections', 'noo-chilli') ,
            'add_new_item' => esc_html__('Add New Menu Sections', 'noo-chilli') ,
            'new_item_name' => esc_html__('New Menu Sections Name', 'noo-chilli') ,
            'parent_item' => esc_html__('Parent Menu Sections', 'noo-chilli') ,
            'parent_item_colon' => esc_html__('Parent Menu Sections:', 'noo-chilli') ,
            'search_items' => esc_html__('Search Menu Sections', 'noo-chilli') ,
            'popular_items' => esc_html__('Popular Menu Sections', 'noo-chilli') ,
            'separate_items_with_commas' => esc_html__('Separate Menu Sections with commas', 'noo-chilli') ,
            'add_or_remove_items' => esc_html__('Add or remove Menu Sections', 'noo-chilli') ,
            'choose_from_most_used' => esc_html__('Choose from the most used Menu Sections', 'noo-chilli') ,
            'not_found' => esc_html__('No Menu Sections found', 'noo-chilli') ,
        );

        $category_args = array(
            'labels' => $category_labels,
            'public' => false,
            'show_ui' => true,
            'show_in_nav_menus' => false,
            'show_tagcloud' => false,
            'show_admin_column' => true,
            'hierarchical' => true,
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'menu_sections',
                'with_front' => false
            ) ,
        );

        register_taxonomy('menu_sections', array(
            'food_menu'
        ) , $category_args);


    }
endif;

add_action('init', 'noo_init_food_menu');

add_action('restrict_manage_posts','restrict_listings_by_menu_sections');
function restrict_listings_by_menu_sections() {
    global $typenow;
    global $wp_query;
    if( isset($_GET['menu_sections']) ){
        $select = $_GET['menu_sections'];
    }else{
        $select = 0;
    }
    if ($typenow=='food_menu') {
        $taxonomy = 'menu_sections';
        wp_dropdown_categories(array(
            'show_option_all' =>  esc_html__("Show All Menu Sections"),
            'taxonomy'        =>  $taxonomy,
            'name'            =>  'menu_sections',
            'orderby'         =>  'name',
            'selected'        =>  esc_attr($select),
            'hierarchical'    =>  true,
            'depth'           =>  3,
            'show_count'      =>  true, // Show # listings in parens
            'hide_empty'      =>  true, // Don't show businesses w/o listings
        ));
    }
}
function noo_convert_restrict($query) {
    global $pagenow;
    global $typenow;
    if ($pagenow=='edit.php' && $typenow=='food_menu') {

        $var = &$query->query_vars['menu_sections'];

        if ( isset($var) && $var != 0  ) {
            $term = get_term_by('id',$var,'menu_sections');
            $var = $term->slug;
        }

    }
}
add_filter('parse_query','noo_convert_restrict');





