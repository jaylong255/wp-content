<?php
/*
Plugin Name: Noo Chilli Library
Plugin URI: http://nootheme.com/
Description: This is plugin for NooTheme. This plugin allows you to create post types, taxonomies, support classes for sidebar, twitter...
Version: 1.4.0
Author: NooTheme
Author URI: http://nootheme.com/
License: GPLv2 or later
*/

/**
 * This is the Noo Chilli Library loader class.
 *
 * @package   Noo_Chilli_Library
 * @author    nootheme (http:://nootheme.com)
 * @copyright Copyright (c) 2014, NooTheme
 */

if ( !class_exists('Noo_Chilli_Library') ):

    class Noo_Chilli_Library{

        /*
         * This method loads other methods of the class.
         */
        public function __construct(){
            /* load languages */
            $this->load_languages();

            /*load all nootheme*/
            $this->load_nootheme();

            /*auto update version*/
            $this->load_check_version();
        }

        /*
         * Load the languages before everything else.
         */
        private function load_languages(){
            add_action( 'plugins_loaded', array( $this, 'load_textdomain' ) );
        }

        /*
         * Load the text domain.
         */
        public function load_textdomain(){

            load_plugin_textdomain( 'noo-chilli', false, plugin_dir_url( __FILE__ ) . '/languages' );
        }

        /*
         * Load Nootheme on the 'after_setup_theme' action. Then filters will
         */
        public function load_nootheme(){

            $this->constants();

            $this->admin_includes();
        }

        /*
         * Load Nootheme on the 'after_setup_theme' action. Then filters will
         */
        public function load_check_version(){

            if( !class_exists('Noo_Check_Version_Child') ) {
                require_once( PLUGIN_SERVER_PATH.'/admin/noo-check-version-child.php' );
            }

            $check_version = new Noo_Check_Version_Child(
                'noo-chilli-library',
                'Noo Chilli Library',
                'noo-chilli',
                'http://update.nootheme.com/api/license-manager/v1',
                'plugin',
                __FILE__
            );
        }

        /**
         * Constants
         */
        private function constants(){

            if( !defined('PLUGIN_PATH') ) define('PLUGIN_PATH', plugin_dir_url( __FILE__ ));

            if( !defined('PLUGIN_SERVER_PATH') ) define('PLUGIN_SERVER_PATH',dirname( __FILE__ ) );
        }

        /*
         * Require file
         */
        private function  admin_includes(){
            require_once PLUGIN_SERVER_PATH.'/admin/post-type/function-init.php';
            require_once PLUGIN_SERVER_PATH.'/admin/vc_extension/vc_init.php';
        }


    }
    $oj_nooplugin = new Noo_Chilli_Library();

endif;

require_once dirname( __FILE__ ) . '/smk-sidebar-generator/smk-sidebar-generator.php';
require_once dirname( __FILE__ ) . '/MCAPI.class.php';
require_once dirname( __FILE__ ) . '/twitteroauth.php';
require_once dirname( __FILE__ ) . '/admin/import-demo/noo-setup-install.php';
?>